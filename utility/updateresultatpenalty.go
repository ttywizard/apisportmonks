package utility

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"os"
)

func UpdateResultatOfPenalty() {

	query := `SELECT DISTINCT(match_id) AS match_id FROM matchcenter_event WHERE type LIKE 'Pen Shootout%'`
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("updateResultatOfPenalty", err)
	}

	for rows.Next() {
		var matchId int

		err = rows.Scan(&matchId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("updateResultatOfPenalty", err)
			os.Exit(1)
		}

		updateQuery := `UPDATE matchcenter_event SET result = res.score 
                        FROM (
                                  SELECT *,
                                         CASE
                                             WHEN num = 1 AND type = 'Pen Shootout Goal' THEN '1-0'
                                             WHEN num = 1 AND type = 'Pen Shootout Miss' THEN '0-0'
                                             WHEN num % 2 != 0 THEN (COALESCE(res, 0))::TEXT || '-' || (COALESCE(LAG(res, 1) OVER (), 0))::TEXT
                                             WHEN num % 2 = 0 THEN (COALESCE(LAG(res, 1) OVER (), 0))::TEXT || '-' || (COALESCE(res, 0))::TEXT
                                             END AS score
                                  FROM (
                                           SELECT *,
                                                  SUM(CASE
                                                          WHEN prev_status ISNULL AND type = 'Pen Shootout Goal' THEN 1
                                                          WHEN prev_status NOTNULL AND type = 'Pen Shootout Goal' THEN 1
                                                          ELSE 0
                                                      END)
                                                  OVER (PARTITION BY team_id ORDER BY time ASC, id ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS res
                                           FROM (
                                                    SELECT ROW_NUMBER() OVER (PARTITION BY match_id ORDER BY time ASC, id ASC) AS num,
                                                           LAG(type, 2) OVER ()                                  AS prev_status,
                                                           match_id,
                                                           player_id,
                                                           team_id,
                                                           type,
                                                           result,
                                                           time,
                                                           id
                                                    FROM matchcenter_event AS mc_e
                                                    WHERE type LIKE 'Pen Shootout%'
                                                      AND match_id = $1
                                                    ORDER BY time, id ASC
                                                ) AS it
                                           ORDER BY time ASC, id ASC
                                       ) AS itog
                              ) AS res WHERE res.id = matchcenter_event.id`

		_, err := db.Db.Exec(updateQuery, matchId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("updateResultatOfPenalty", err)
		}

	}
}
