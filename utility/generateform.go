package utility

import (
	"errors"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"os"
	"strconv"
	"strings"
)

//UpdateStandingsLeagueForm Обновление последних матчей для общей таблицы типа League
// -task updateTableForm -typeForm all
// -task updateTableForm -typeForm all -seasonIsCurrent true

func UpdateStandingsLeagueForm(seasonId int, leagueId int, typeForm string, seasonCurrent bool) {
	var err error
	var setUpdate string
	var andWhere string
	var arrAndWhere []string

	switch typeForm {
	case "home":
		setUpdate = ` SET home_form = itog.form,
                  home_last_match_id = itog.arr_match_id `
		andWhere = " AND home_team_id = mct.team_id "

	case "away":
		setUpdate = ` SET away_form = itog.form,
                  away_last_match_id = itog.arr_match_id `
		andWhere = " AND away_team_id = mct.team_id "

	case "all":
		setUpdate = ` SET overall_form = itog.form,
                  overall_last_match_id = itog.arr_match_id `
		andWhere = " AND (home_team_id = mct.team_id OR away_team_id = mct.team_id) "

	default:
		err := errors.New("тип формы не определен, варианты (home|away|all)")
		db.ErrLog("UpdateStandingsLeagueForm", err)
		os.Exit(0)
	}

	// Обновление текущего сезона
	if seasonCurrent {
		arrAndWhere = append(arrAndWhere, "mc_s.current")
	}

	// Указан сезон
	if seasonId != 0 {
		arrAndWhere = append(arrAndWhere, "mct.season_id = "+strconv.Itoa(seasonId))
	}
	// Указана лига
	if leagueId != 0 {
		arrAndWhere = append(arrAndWhere, "mct.league_id = "+strconv.Itoa(leagueId))
	}

	var sectionWhere string
	if len(arrAndWhere) > 0 {
		sectionWhere += " WHERE "
		for _, w := range arrAndWhere {
			sectionWhere += w + " AND "
		}
	}

	query := `UPDATE matchcenter_standings_stage AS mctu` + setUpdate +
		` FROM (     SELECT mct.league_id,
                         mct.season_id,
                         mct.team_id,
                         arr_match_id,
                         it.form
                  FROM matchcenter_standings_stage AS mct LEFT JOIN LATERAL
                      (
                      SELECT mct.team_id     AS team_id
                            ,mct.league_id  AS league_id
                            ,mct.season_id  AS season_id
                            ,array_agg(id)   AS arr_match_id
                            ,array_agg(form) AS form
                      FROM (
                               SELECT mct.team_id AS team_id
                                    ,id
                                    ,form
                               FROM (SELECT mc_m.id
                                            ,match_date
                                            ,CASE
                                                 WHEN home_team_id = mct.team_id
                                                    THEN
                                                    CASE
                                                        WHEN home_team_id = mct.team_id AND ftr = 'A' THEN 'L'
                                                        WHEN home_team_id = mct.team_id AND ftr = 'D' THEN 'D'
                                                        WHEN home_team_id = mct.team_id AND ftr = 'H' THEN 'W'
                                                END
                                                WHEN away_team_id = mct.team_id
                                                    THEN
                                                    CASE
                                                        WHEN away_team_id = mct.team_id AND ftr = 'A' THEN 'W'
                                                        WHEN away_team_id = mct.team_id AND ftr = 'D' THEN 'D'
                                                        WHEN away_team_id = mct.team_id AND ftr = 'H' THEN 'L'
                                                END
                                            END AS form
                                      FROM matchcenter_match AS mc_m LEFT JOIN matchcenter_league AS mc_l ON (mc_l.id = mct.league_id)
                                           LEFT JOIN matchcenter_season AS mc_s ON (mc_m.season_id = mc_s.id AND mc_m.stage_id = mc_s.current_stage_id)
                                           LEFT JOIN matchcenter_stages AS mc_stages ON (mc_s.current_stage_id = mc_stages.id)
                                      WHERE mc_stages.has_standings
                                        AND mc_m.league_id = mct.league_id
                                        AND mc_m.season_id = mct.season_id` + andWhere +
		`AND match_date < NOW()
                                      ORDER BY match_date DESC
                                      LIMIT 6
                                   ) AS preform
                               ORDER BY match_date)
                          AS preitog
                      ) AS it ON(mct.league_id, mct.team_id, mct.season_id) = (it.league_id, it.team_id, it.season_id)
                      LEFT JOIN matchcenter_season AS mc_s ON (mc_s.id = mct.season_id) ` + strings.Trim(sectionWhere, "AND ") +
		` ) itog WHERE mctu.league_id = itog.league_id AND mctu.season_id = itog.season_id AND mctu.team_id = itog.team_id`
	//Выполняем запрос
	_, err = db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("UpdateStandingsLeagueForm", err)
	}
}
