package utility

import (
	logger2 "github.com/Pastir/logger"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"go.uber.org/zap"
)

func MappingPlayersDifferentProviders(leagueId int, seasonId int) {
	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetType("League")

	// Список сезонов лиг
	seasons := queries.GetSeasons(stats)

	for _, league := range seasons {
		for _, seasonData := range league {

			query := `UPDATE matchcenter_player SET uid = it.old_uid
                      FROM (
                               SELECT old_uid,
                                      curr_uid
                               FROM (
                                        SELECT mc_p.id                   AS old_player_id,
                                               mc_p.firstname            AS old_fisrname,
                                               mc_p.lastname             AS old_lastname,
                                               mc_line.number            AS number,
                                               mc_line.team_id           AS old_team_id,
                                               mc_p.uid                  AS old_uid,
                                               DATE(old_mc_m.match_date) AS old_match_date,
                                               mmtp.id                   AS team_id,
                                               mc_p.country_id           AS country_id,
                                               mc_line.position,
                                               mc_p.birthdate
                                        FROM old_matchcenter_league AS mc_l
                                                 LEFT JOIN old_matchcenter_season AS mc_s ON (mc_l.uid = mc_s.league_uid)
                                                 LEFT JOIN old_matchcenter_match AS old_mc_m
                                                           ON ((mc_l.id, mc_s.seasons) = (old_mc_m.league_id, old_mc_m.season_id))
                                                 LEFT JOIN old_matchcenter_lineups AS mc_line ON (mc_line.match_id = old_mc_m.id)
                                                 LEFT JOIN old_matchcenter_player AS mc_p ON (mc_line.player_id = mc_p.id)
                                                 LEFT JOIN matchcenter_map_team_providers AS mmtp
                                                           ON (mc_line.team_id = mmtp.provider_team_id AND mmtp.provider_id = 1)
                                        WHERE mc_l.uid = $1
                                          AND mc_s.uid = $2
                                          AND mmtp.provider_id = 1
                                    ) AS old
                                        LEFT JOIN (
                                   SELECT mc_p.id               AS curr_player_id,
                                          mc_p.firstname        AS curr_fisrname,
                                          mc_p.lastname         AS curr_lastname,
                                          mc_line.number,
                                          mc_line.team_id,
                                          mc_p.uid              AS curr_uid,
                                          DATE(mc_m.match_date) AS curr_match_date,
                                          mc_p.country_id           AS country_id,
                                          mc_line.position,
                                          mc_p.birthdate
                                   FROM matchcenter_league AS mc_l
                                            LEFT JOIN matchcenter_season AS mc_s ON (mc_l.uid = mc_s.league_uid)
                                            LEFT JOIN matchcenter_match AS mc_m ON ((mc_l.uid, mc_s.uid) = (mc_m.league_uid, mc_m.season_uid))
                                            LEFT JOIN matchcenter_lineups AS mc_line ON (mc_line.match_id = mc_m.id)
                                            LEFT JOIN matchcenter_player AS mc_p ON (mc_line.player_id = mc_p.id)
                                   WHERE mc_l.uid = $1
                                     AND mc_s.uid = $2
                               ) AS curr ON ((old.number, old.team_id, old_match_date,old.country_id, old.position, old.birthdate) = (curr.number, curr.team_id, curr_match_date,curr.country_id, curr.position, curr.birthdate))
                               WHERE old_uid NOTNULL
                                 AND curr_uid NOTNULL
                               GROUP BY 1, 2
                           ) AS it
                      WHERE it.curr_uid = matchcenter_player.uid`

			_, err := db.Db.Exec(query, seasonData.LeagueUid, seasonData.SeasonUid)
			if err, ok := err.(*pq.Error); ok {
				logger2.ErrorLog.Error("error",
					zap.String("UPDATE", "MappingPlayersDifferentProviders"),
					zap.String("errorName", err.Code.Name()),
					zap.Error(err),
					zap.String("League", seasonData.LeagueUid),
					zap.String("Season", seasonData.SeasonUid),
				)
			}
		}
	}
}
