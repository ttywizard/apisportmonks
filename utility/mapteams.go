package utility

import (
	logger2 "github.com/Pastir/logger"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"go.uber.org/zap"
)

func MapTeamProviders(leagueId int, seasonId int) {

	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetType("League")

	// Список сезонов лиг
	seasons := queries.GetSeasons(stats)

	for _, league := range seasons {
		for _, seasonData := range league {
			query := `UPDATE matchcenter_team SET uid = it.old_uid 
FROM (
         SELECT *
         FROM (
                  SELECT row_number()
                         over (PARTITION BY curr.name ORDER BY curr.name, strict_word_similarity(old.name, curr.name) DESC ) AS num,
                         curr.name                                                                                           AS curr_team_name,
                         old.name                                                                                            AS old_team_name,
                         strict_word_similarity(old.name, curr.name),
                         curr.team_uid                                                                                       AS curr_uid,
                         old.team_uid                                                                                        AS old_uid
                  FROM (
                           SELECT league_uid,
                                  season_uid,
                                  mc_t.uid AS team_uid,
                                  name
                           FROM matchcenter_teams_season AS mc_ts
                                    LEFT JOIN matchcenter_team AS mc_t ON (mc_ts.team_id = mc_t.id)
                           WHERE mc_ts.league_uid = $1
                             AND mc_ts.season_uid = $2
                       ) AS curr
                           LEFT JOIN (
                      SELECT league_uid,
                             season_uid,
                             mc_t.uid AS team_uid,
                             name
                      FROM old_matchcenter_teams_season AS mc_ts
                               LEFT JOIN old_matchcenter_team AS mc_t ON (mc_ts.team_id = mc_t.id)
                      WHERE mc_ts.league_uid = $1
                        AND mc_ts.season_uid = $2
                  ) AS old ON (strict_word_similarity(old.name, curr.name) > 0.1)
                  ORDER BY curr.team_uid, 3 DESC) AS pre_itog
         WHERE num = 1
     )AS it WHERE it.curr_uid = uid AND it.old_uid NOTNULL`

			_, err := db.Db.Exec(query, seasonData.LeagueUid, seasonData.SeasonUid)
			if err, ok := err.(*pq.Error); ok {
				logger2.ErrorLog.Error("errorDB",
					zap.String("SELECT ", "MapTeamProviders"),
					zap.String("errorName", err.Code.Name()),
					zap.String("LeagueUID", seasonData.LeagueUid),
					zap.String("SeasonUID", seasonData.SeasonUid),
					zap.Error(err),
				)
			}
		}
	}
}
