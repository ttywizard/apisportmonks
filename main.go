package main

import (
	logger2 "github.com/Pastir/logger"
	"gitlab.com/ttywizard/apisportmonks/bk"
	"gitlab.com/ttywizard/apisportmonks/config"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/endpoints"
	"gitlab.com/ttywizard/apisportmonks/utility"
	"go.uber.org/zap"
)

func main() {

	config.Config.ConfigFile()
	// access log
	access := logger2.ConfigLog{
		LogDir:     config.Config.Logs.LogDir,
		Filename:   "access.log",
		MaxSize:    config.Config.Logs.MaxFileSize,
		MaxBackups: config.Config.Logs.MaxFilesCount,
		MaxAge:     config.Config.Logs.MaxAge,
		Compress:   config.Config.Logs.Compress,
	}
	logger2.AccessLog = access.SetLog()

	// info log
	info := logger2.ConfigLog{
		LogDir:     config.Config.Logs.LogDir,
		Filename:   "info.log",
		MaxSize:    config.Config.Logs.MaxFileSize,
		MaxBackups: config.Config.Logs.MaxFilesCount,
		MaxAge:     config.Config.Logs.MaxAge,
		Compress:   config.Config.Logs.Compress,
	}
	logger2.InfoLog = info.SetLog()
	// error log
	errors := logger2.ConfigLog{
		LogDir:     config.Config.Logs.LogDir,
		Filename:   "error.log",
		MaxSize:    config.Config.Logs.MaxFileSize,
		MaxBackups: config.Config.Logs.MaxFilesCount,
		MaxAge:     config.Config.Logs.MaxAge,
		Compress:   config.Config.Logs.Compress,
	}
	logger2.ErrorLog = errors.SetLog()

	db.InitDb()
	db.ConnectDB()
	db.SetSettingsProvider()
	db.SetProviderEndpoints()
	queries.GetUuidListProviderCountryId()
	queries.GetCountryDataByISO()
	queries.GetListPitch()
	queries.GetStandingRules()
	queries.GetGroupsName()
	queries.GetEventsType()
	queries.GetWeatherUuid()
	queries.GetSeasonPointRules()
	queries.GetIdBookmakers()
	queries.GetIdMarkets()

	//Логируем время старта задачи
	logger2.InfoLog.Info("Start task: ",
		zap.String("Task ", config.Args.Task),
	)

	switch config.Args.Task {
	case "editLeagueCurrentSeason":
		endpoints.UpsertLeaguesCurrentSeason("leagues")
	case "editLeague":
		endpoints.UpsertLeagues("leagues")
	case "upsertTeamsSeasonsId":
		endpoints.UpsertTeamsDataSeasonId("teams", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason, config.Args.TypeHandlers)
	case "upsertFixtures":
		endpoints.UpsertFixturesDataSeasonId("seasons", config.Args.TypeHandlers, config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "upsertFixturesBetweenDate":
		endpoints.UpsertFixturesBetweenDate("fixtures", config.Args.TypeHandlers, config.Args.FromDate, config.Args.ToDate)
	case "upsertCountriesProvider":
		endpoints.UpsertRawCountries("countries", 1)
	case "upsertSeasons":
		endpoints.UpsertSeasons("seasons", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "upsertStageSeasonsId":
		endpoints.UpsertStagesSeasonId("stages", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "upsertRoundsSeasonsId":
		endpoints.UpsertRoundsOfSeason("rounds", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "upsertStandingOfSeasons":
		endpoints.UpsertStandingsOfSeason("standings", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "upsertGroupsName":
		endpoints.UpsertGroupsName("seasons", config.Args.LeagueId, config.Args.SeasonId, 1)
	case "upsertSidelined":
		endpoints.UpsertSidelined("teams", config.Args.LeagueId, config.Args.SeasonId, 1)
	case "mapTeams":
		utility.MapTeamProviders(config.Args.LeagueId, config.Args.SeasonId)
	case "mapPlayers":
		utility.MappingPlayersDifferentProviders(config.Args.LeagueId, config.Args.SeasonId)
	case "updateResultatPenalty":
		utility.UpdateResultatOfPenalty()
	case "upsertBookmakers":
		endpoints.UpsertBookmakers("bookmakers")
	case "upsertMarkets":
		endpoints.UpsertMarkets("markets")
	case "upsertOdds":
		endpoints.UpsertOddsFixturesRangeData("fixtures", config.Args.FromDate, config.Args.ToDate)
	case "upsertLast2Hours":
		endpoints.UpsertLast2Hours("fixtures")
	case "upsertLivescore":
		endpoints.UpsertLivescore("livescore")
	case "upsertAggregateScore":
		endpoints.UpsertAggregateScore("topscorers", config.Args.LeagueId, config.Args.SeasonId, config.Args.CurrentSeason)
	case "updateTableForm":
		utility.UpdateStandingsLeagueForm(config.Args.LeagueId, config.Args.SeasonId, config.Args.TypeForm, config.Args.CurrentSeason)
	case "findTrends":
		queries.GetListTrends()
		queries.GetMarkets()
		endpoints.Trends(config.Args.TypeTrends)
	/*case "findFacts":
	queries.GetListTrends()
	queries.GetMarkets()
	endpoints.Facts(config.Args.TypeFacts)*/
	case "coefficient":
		bk.RoutingBK(config.Args.NameBK)
	}
}
