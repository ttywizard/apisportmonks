package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
)

//UpsertGroupsName -task upsertGroupsName
func UpsertGroupsName(nameEndpoint string, leagueId int, seasonId int, page int) {

	// Получаем список сезонов для обработки
	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	listSeasons := queries.GetSeasonsHasStandings(stats)

	// Что включать в ответе
	var inc = make(types.Include)
	inc.SetSeasons()
	params := db.GetParam
	params.Add("include", "groups")

	// Обходим все сезоны
	for _, seasId := range listSeasons {
		// URL API
		url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seasId) + "?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			os.Exit(0)
		}

		var seasonData types.ResponseSeason
		err = json.Unmarshal(resp, &seasonData)
		if err != nil {
			db.ErrLog("UpsertSeasons", err)
			os.Exit(0)
		}

		handler.UpsertGroupsBySeason(nameEndpoint, &seasonData)
	}
}
