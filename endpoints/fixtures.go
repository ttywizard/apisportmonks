package endpoints

import (
	"encoding/json"
	"fmt"
	"gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"reflect"
	"strconv"
	"strings"
)

//UpsertFixturesDataSeasonId  -task upsertFixtures -fixtureHandlers fixtures -seasonIsCurrent true
func UpsertFixturesDataSeasonId(nameEndpoint string, handlerTypes string, leagueId int, seasonId int, seasonCurrent bool) {
	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)

	// Список сезонов лиг
	seasons := queries.GetSeasonsIdData(stats)

	// Что включать в ответе
	hT := []string{"fixtures.aggregate", "fixtures.stage", "groups", "fixtures.referee", "fixtures", "fixtures.sidelined", "fixtures.stats", "fixtures.lineup", "fixtures.bench", "fixtures.events", "fixtures.stats", "fixtures.firstAssistant", "fixtures.secondAssistant", "fixtures.fourthOfficial"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))

	for _, seas := range seasons {
		// URL API
		//url := "https://soccer.sportmonks.com/api/v2.0/seasons/5337?api_token=l9gjvrFitnrWjwTHFfGAxYiNp9vVsMmOikE5GAwyenaeNhxZ1gWqCGUQYqtc&include=fixtures.referee%2Cfixtures%2Cfixtures.sidelined%2Cfixtures.stats%2Cfixtures.lineup%2Cfixtures.events%2Cfixtures.fourthOfficial%2Cfixtures.stage%2Cgroups%2Cfixtures.bench%2Cfixtures.firstAssistant%2Cfixtures.secondAssistant&page="
		//url := "https://soccer.sportmonks.com/api/v2.0/seasons/5556?api_token=l9gjvrFitnrWjwTHFfGAxYiNp9vVsMmOikE5GAwyenaeNhxZ1gWqCGUQYqtc&include=fixtures.referee%2Cfixtures%2Cfixtures.sidelined%2Cfixtures.stats%2Cfixtures.lineup%2Cfixtures.events%2Cfixtures.fourthOfficial%2Cfixtures.stage%2Cgroups%2Cfixtures.bench%2Cfixtures.firstAssistant%2Cfixtures.secondAssistant&page="
		url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seas.SeasonId) + "?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertFixturesDataSeasonId", err)
			os.Exit(0)
		}

		var fixturesData types.ResponseSeason
		err = json.Unmarshal(resp, &fixturesData)
		if err != nil {
			db.ErrLog("UpsertFixturesDataSeasonId", err)
			os.Exit(0)
		}

		arrHandler := strings.Split(handlerTypes, ",")

		for _, handlerChoice := range arrHandler {
			switch strings.Trim(handlerChoice, " ") {
			case "fixtures":
				handler.HandlersFixturesSeason(nameEndpoint, &fixturesData, seas.LeagueUid, seas.SeasonUid)
			case "events":
				handler.HandlersUpsertEventsSeason(nameEndpoint, &fixturesData)
			case "lineups":
				handler.HandlersUpsertLineupsSeason(nameEndpoint, &fixturesData)
			case "statsPlayersFixture":
				handler.HandlersUpsertStatsPlayersFixturesSeasons(nameEndpoint, &fixturesData)
			case "statsFixtures":
				handler.HandlersUpsertFixturesStatsSeasons(nameEndpoint, &fixturesData)
			case "sidelined":
				handler.HandlersUpsertSidelined(nameEndpoint, &fixturesData, seasons)
			case "weather":
				handler.HandlersUpsertWeatherSeasons(nameEndpoint, &fixturesData)
			case "referees":
				handler.HandlersUpsertRefereesSeasons(nameEndpoint, &fixturesData)
			case "all":
				//-task upsertFixtures -fixtureHandlers all
				handler.HandlersUpsertRefereesSeasons(nameEndpoint, &fixturesData)
				handler.HandlersFixturesSeason(nameEndpoint, &fixturesData, seas.LeagueUid, seas.SeasonUid)
				handler.HandlersUpsertEventsSeason(nameEndpoint, &fixturesData)
				handler.HandlersUpsertLineupsSeason(nameEndpoint, &fixturesData)
				handler.HandlersUpsertStatsPlayersFixturesSeasons(nameEndpoint, &fixturesData)
				handler.HandlersUpsertFixturesStatsSeasons(nameEndpoint, &fixturesData)
				handler.HandlersUpsertSidelined(nameEndpoint, &fixturesData, seasons)
				handler.HandlersUpsertWeatherSeasons(nameEndpoint, &fixturesData)
			}
		}
	}
}

//UpsertOddsFixturesRangeData -task upsertOdds -fromDate 2021-07-21 -toDate 2021-07-23
func UpsertOddsFixturesRangeData(nameEndpoint string, startDate string, endDate string) {

	var paramMarket string
	var paramBookmaker string

	// типы коффициентов
	for k, _ := range queries.MarketsData {
		paramMarket += strconv.Itoa(k) + ","
	}

	// только активные букмекеры
	for k, _ := range queries.BookmakersData {
		paramBookmaker += strconv.Itoa(k) + ","
	}

	// Что включать в ответе
	hT := []string{"odds", "probability"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))
	params.Add("markets", strings.Trim(paramMarket, ","))
	params.Add("bookmakers", strings.Trim(paramBookmaker, ","))

	numPage := 1
	//Реализация пагинации
	for {
		params.Set("page", strconv.Itoa(numPage))
		url := db.Endpoints[nameEndpoint].Url + "between/" + startDate + "/" + endDate + "/?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			os.Exit(0)
		}

		var fixturesData types.ResponseFixtures
		err = json.Unmarshal(resp, &fixturesData)
		if err != nil {
			db.ErrLog("UpsertOddsFixturesRangeData", err)
			os.Exit(0)
		}

		//Хандлер
		handler.HandlersOdds(nameEndpoint, &fixturesData)
		handler.HandlersProbability(nameEndpoint, &fixturesData)

		//Пагинация
		var pageData handler.PaginationData
		pageData.Init()
		handler.PaginationHandler(&fixturesData.Meta.Pagination, &pageData)

		//Если текущая страница равна общему количеству страниц завершаем обработку
		if numPage > *fixturesData.Meta.Pagination.TotalPages || *fixturesData.Meta.Pagination.TotalPages == 1 {
			break
		}
		numPage++
	}
}

func UpsertLast2Hours(nameEndpoint string) {
	var paramMarket string
	var paramBookmaker string

	// типы коффициентов
	for k, _ := range queries.MarketsData {
		paramMarket += strconv.Itoa(k) + ","
	}

	// только активные букмекеры
	for k, _ := range queries.BookmakersData {
		paramBookmaker += strconv.Itoa(k) + ","
	}

	// Что включать в ответе
	hT := []string{"stage", "group", "referee"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))
	params.Add("markets", strings.Trim(paramMarket, ","))
	params.Add("bookmakers", strings.Trim(paramBookmaker, ","))

	url := db.Endpoints[nameEndpoint].Url + "updates/?" + params.Encode()
	fmt.Println(url)

}

//UpsertLivescore - обновляет данные события которые проходят в текущий момент, LIVE
// -task upsertLivescore
func UpsertLivescore(nameEndpoint string) {

	var paramMarket string
	var paramBookmaker string

	// типы коффициентов
	for k, _ := range queries.MarketsData {
		paramMarket += strconv.Itoa(k) + ","
	}

	// только активные букмекеры
	for k, _ := range queries.BookmakersData {
		paramBookmaker += strconv.Itoa(k) + ","
	}

	// Что включать в ответе
	hT := []string{"events", "referee", "lineup", "bench", "odds", "stats", "aggregate"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))
	params.Add("markets", strings.Trim(paramMarket, ","))
	params.Add("bookmakers", strings.Trim(paramBookmaker, ","))

	url := db.Endpoints[nameEndpoint].Url + "?" + params.Encode()

	numPage := 1
	//Реализация пагинации
	for {

		params.Set("page", strconv.Itoa(numPage))
		// URL API
		url = db.Endpoints[nameEndpoint].Url + "/now?" + params.Encode()
		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertLivescore", err)
			os.Exit(0)
		}

		var fixturesData types.ResponseFixtures
		err = json.Unmarshal(resp, &fixturesData)
		if err != nil {
			db.ErrLog("UpsertLivescore", err)
			os.Exit(0)
		}

		if len(fixturesData.Data) > 0 {
			handler.HandlersUpsertReferees(nameEndpoint, &fixturesData)
			handler.HandlersUpsertFixtures(nameEndpoint, &fixturesData)
			handler.HandlersUpsertEvents(nameEndpoint, &fixturesData)
			handler.HandlersUpsertLineups(nameEndpoint, &fixturesData)
			handler.HandlersUpsertStatsPlayersFixtures(nameEndpoint, &fixturesData)
			handler.HandlersUpsertFixturesStats(nameEndpoint, &fixturesData)
			handler.HandlersUpsertWeather(nameEndpoint, &fixturesData)
		}

		//Пагинация
		if reflect.ValueOf(fixturesData.Meta.Pagination.TotalPages).IsNil() {
			break
		} else {
			var pageData handler.PaginationData
			pageData.Init()
			handler.PaginationHandler(&fixturesData.Meta.Pagination, &pageData)

			//Если текущая страница равна общему количеству страниц завершаем обработку
			if numPage > *fixturesData.Meta.Pagination.TotalPages || *fixturesData.Meta.Pagination.TotalPages == 1 {
				break
			}
			numPage++
		}
	}
}

//UpsertFixturesBetweenDate -task upsertFixturesBetweenDate
// Обновляет данные fixtures за заданный период
func UpsertFixturesBetweenDate(nameEndpoint string, handlerTypes string, startDate string, endDate string) {
	var paramMarket string
	var paramBookmaker string

	// типы коффициентов
	for k, _ := range queries.MarketsData {
		paramMarket += strconv.Itoa(k) + ","
	}

	// только активные букмекеры
	for k, _ := range queries.BookmakersData {
		paramBookmaker += strconv.Itoa(k) + ","
	}

	// Что включать в ответе
	hT := []string{"events", "referee", "lineup", "bench", "odds", "stats", "aggregate"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))
	params.Add("markets", strings.Trim(paramMarket, ","))
	params.Add("bookmakers", strings.Trim(paramBookmaker, ","))

	var betweenDate string
	if startDate != "" && endDate != "" {
		betweenDate = "between/" + startDate + "/" + endDate
	} else if startDate != "" && endDate == "" {
		betweenDate = "date/" + startDate
	}

	numPage := 1
	//Реализация пагинации
	for {

		params.Set("page", strconv.Itoa(numPage))
		// URL API
		url := db.Endpoints[nameEndpoint].Url + betweenDate + "?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertFixturesBetweenDate", err)
			os.Exit(0)
		}

		var fixturesData types.ResponseFixtures
		err = json.Unmarshal(resp, &fixturesData)
		if err != nil {
			db.ErrLog("UpsertFixturesBetweenDate", err)
			os.Exit(0)
		}

		arrHandler := strings.Split(handlerTypes, ",")

		for _, handlerChoice := range arrHandler {
			switch strings.Trim(handlerChoice, " ") {
			case "fixtures":
				handler.HandlersUpsertFixtures(nameEndpoint, &fixturesData)
			case "events":
				handler.HandlersUpsertEvents(nameEndpoint, &fixturesData)
			case "lineups":
				handler.HandlersUpsertLineups(nameEndpoint, &fixturesData)
			case "statsPlayersFixture":
				handler.HandlersUpsertStatsPlayersFixtures(nameEndpoint, &fixturesData)
			case "statsFixtures":
				handler.HandlersUpsertFixturesStats(nameEndpoint, &fixturesData)
			case "weather":
				handler.HandlersUpsertWeather(nameEndpoint, &fixturesData)
			case "referees":
				handler.HandlersUpsertReferees(nameEndpoint, &fixturesData)
			case "all":
				//-task upsertFixtures -fixtureHandlers all
				handler.HandlersUpsertReferees(nameEndpoint, &fixturesData)
				handler.HandlersUpsertFixtures(nameEndpoint, &fixturesData)
				handler.HandlersUpsertEvents(nameEndpoint, &fixturesData)
				handler.HandlersUpsertLineups(nameEndpoint, &fixturesData)
				handler.HandlersUpsertStatsPlayersFixtures(nameEndpoint, &fixturesData)
				handler.HandlersUpsertFixturesStats(nameEndpoint, &fixturesData)
				handler.HandlersUpsertWeather(nameEndpoint, &fixturesData)

			}
		}

		//Пагинация
		var pageData handler.PaginationData
		pageData.Init()
		handler.PaginationHandler(&fixturesData.Meta.Pagination, &pageData)

		//Если текущая страница равна общему количеству страниц завершаем обработку
		if numPage > *fixturesData.Meta.Pagination.TotalPages || *fixturesData.Meta.Pagination.TotalPages == 1 {
			break
		}
		numPage++
	}
}
