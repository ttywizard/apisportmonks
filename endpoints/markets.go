package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
)

func UpsertMarkets(nameEndpoint string) {

	// GET Параметры
	params := db.GetParam

	// URL API
	url := db.Endpoints[nameEndpoint].Url + "?" + params.Encode()

	// Запрос к API
	resp, err := client.RequestToEndpoint(url)
	if err != nil {
		db.ErrLog("UpsertMarkets", err)
		os.Exit(0)
	}

	var s types.ResponseMarket
	err = json.Unmarshal(resp, &s)
	if err != nil {
		db.ErrLog("UpsertMarkets", err)
		os.Exit(0)
	}

	handler.HandlerMarkets(&s)

	return
}
