package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
)

func UpsertRawCountries(nameEndpoint string, page int) {

	// GET Параметры
	params := db.GetParam
	params.Set("page", strconv.Itoa(page))

	// URL API
	url := db.Endpoints[nameEndpoint].Url + "?" + params.Encode()

	// Запрос к API
	resp, err := client.RequestToEndpoint(url)
	if err != nil {
		db.ErrLog("UpsertRawCountries", err)
		os.Exit(0)
	}

	var s types.ResponseCountries
	err = json.Unmarshal(resp, &s)
	if err != nil {
		db.ErrLog("UpsertRawCountries", err)
		os.Exit(0)
	}

	handler.UpsertProviderCountry(&s)

	//Пагинация
	var paginationData handler.PaginationData
	handler.PaginationHandler(&s.Meta.Pagination, &paginationData)

	if *paginationData.Total > page {
		UpsertRawCountries(nameEndpoint, page+1)
	}

	return
}
