package endpoints

import (
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
)

//UpsertAggregateScore -task upsertAggregateScore -seasonIsCurrent true
func UpsertAggregateScore(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool) {

	handler.HandlersUpsertTopAssist(nameEndpoint)
	handler.HandlersUpsertTopGoals(nameEndpoint)
	handler.HandlersUpsertTopCards(nameEndpoint)
	handler.HandlersUpsertTopGoalkeeper(nameEndpoint)
	handler.HandlersUpsertTopPenalty(nameEndpoint)

}
