package endpoints

import (
	"gitlab.com/ttywizard/apisportmonks/api/trends"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
)

func Trends(typesTrend string) {
	var stats db.Statistics
	stats.SetCurrent(true)
	season := queries.GetSeasonsIdData(stats)

	//Обходим лиги и текущие сезоны
	for _, seasData := range season {
		listMatches := queries.GetMatchNextRound(seasData.LeagueId, seasData.SeasonId, "trend")

		//Обходим матчи следующего тура
		for _, val := range listMatches {
			trends.MatchIsVisible(val.MatchId)

			switch typesTrend {
			case "corners":
				trends.CornersTotalTrends(val, seasData.Season)
			case "yellowcards":
				trends.CardsTotalTrends(val, seasData.Season)
			case "handicap":
				trends.HandicapTotalTrends(val, seasData.Season)
			case "1x2":
				trends.X12TrendsH2H(val, seasData.Season)
			case "all":
				trends.CornersTotalTrends(val, seasData.Season)
				trends.CardsTotalTrends(val, seasData.Season)
				trends.HandicapTotalTrends(val, seasData.Season)
				trends.X12TrendsH2H(val, seasData.Season)
			}
		}
	}
}

/*//Facts -conf config.yaml -task Facts1X2 -typeFact btts
func Facts(typesFact string) {
	var stats db.Statistics
	stats.SetCurrent(true)
	season := queries.GetSeasonsIdData(stats)

	//Обходим лиги и текущие сезоны
	for _, seasData := range season {

		//fmt.Printf("%d =>  %d ", seasData.LeagueId, seasData.SeasonId)
		//Обходим матчи следующего тура, для которых есть коффы и нет сгенерированых фактов определенного типа
		switch typesFact {
		case "1X2":
			//listMatches := queries.GetMatchNextRoundAndMarket(1,  seasData.SeasonId, "fact")
			//for _, val := range listMatches {
			//	facts.Facts1X2(val.MatchId, val.LeagueId)
			//}
		case "btts":
			listMatches := queries.GetMatchNextRoundAndMarket(976105, seasData.SeasonId, "fact")
			for _, val := range listMatches {
				facts.FactsBttsH2H(val)
			}
		case "all": // Все факты
			listMatches := queries.GetMatchNextRoundAndMarket(1, seasData.SeasonId, "fact")
			//for _, val := range listMatches {
			//	facts.Facts1X2(val.MatchId, val.LeagueId)
			//}

			listMatches = queries.GetMatchNextRoundAndMarket(976105,seasData.SeasonId, "fact")
			for _, val := range listMatches {
				facts.FactsBttsH2H(val)
			}
		}

	}
}*/
