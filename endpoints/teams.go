package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
	"strings"
)

//UpsertTeamsDataSeasonId -task upsertTeamsSeasonsId -seasonIsCurrent true -typeHandler venue
func UpsertTeamsDataSeasonId(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool, typeHandler string) {

	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)

	// Список сезонов лиг
	seasons := queries.GetSeasons(stats)
	seasonIdData := queries.GetSeasonsIdData(stats)

	// Что включать в ответе
	var hT []string
	handlersChoice := strings.Split(typeHandler, ",")
	//{"squad.player", "coach", "venue"}
	for _, dt := range handlersChoice {
		switch dt {
		case "squad.player":
			hT = append(hT, "squad.player")
		case "coach":
			hT = append(hT, "coach")
		case "venue":
			hT = append(hT, "venue")
		}
	}

	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))
	params.Add("page", "1")

	for _, seas := range seasons {
		for _, seasData := range seas {
			// URL API
			numPage := 1
			//Реализация пагинации
			for {
				params.Set("page", strconv.Itoa(numPage))
				url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seasData.SeasonId) + "?" + params.Encode()
				// Запрос к API
				resp, err := client.RequestToEndpoint(url)
				if err != nil {
					db.ErrLog("UpsertTeamsDataSeasonId", err)
					os.Exit(0)
				}

				var teamData types.ResponseTeams
				err = json.Unmarshal(resp, &teamData)
				if err != nil {
					db.ErrLog("UpsertTeamsDataSeasonId", err)
					os.Exit(0)
				}

				for _, dt := range handlersChoice {
					switch dt {
					case "squad.player":
						handler.HandlersUpsertPlayerSquadTeams(nameEndpoint, &teamData)
						handler.UpsertSquadTeamSeason(nameEndpoint, &teamData, seasData.LeagueId, seasData.SeasonId)
					case "coach":
						handler.UpsertCoachSquadTeams(nameEndpoint, &teamData)
					case "venue":
						handler.UpsertTeamsVenues(nameEndpoint, &teamData)
					case "teams":
						handler.UpsertTeams(nameEndpoint, &teamData)
						handler.UpsertTeamsSeason(nameEndpoint, &teamData, seasData.LeagueId, seasData.SeasonId, &seasonIdData)
					case "all":
						handler.UpsertTeams(nameEndpoint, &teamData)
						handler.HandlersUpsertPlayerSquadTeams(nameEndpoint, &teamData)
						handler.UpsertSquadTeamSeason(nameEndpoint, &teamData, seasData.LeagueId, seasData.SeasonId)
						handler.UpsertCoachSquadTeams(nameEndpoint, &teamData)
						handler.UpsertTeamsVenues(nameEndpoint, &teamData)
						handler.UpsertTeamsSeason(nameEndpoint, &teamData, seasData.LeagueId, seasData.SeasonId, &seasonIdData)
					}
				}

				//Пагинация
				var pageData handler.PaginationData
				pageData.Init()
				handler.PaginationHandler(&teamData.Meta.Pagination, &pageData)

				//Если текущая страница равна общему количеству страниц завершаем обработку
				if numPage > *teamData.Meta.Pagination.TotalPages || *teamData.Meta.Pagination.TotalPages == 1 {
					break
				}
				numPage++
			}
		}
	}
}
