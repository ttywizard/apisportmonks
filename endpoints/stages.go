package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
)

//UpsertStagesSeasonId добаляет или обновляет данные этапов по указаным сезонам
//-task upsertStageSeasonsId -seasonIsCurrent true
func UpsertStagesSeasonId(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool) {
	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)

	// Список сезонов лиг
	seasons := queries.GetSeasonsIdData(stats)

	params := db.GetParam

	for _, seas := range seasons {
		// URL API
		url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seas.SeasonId) + "?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertStagesSeasonId", err)
			os.Exit(0)
		}

		var stageData types.ResponseStages
		err = json.Unmarshal(resp, &stageData)
		if err != nil {
			db.ErrLog("UpsertStagesSeasonId", err)
			os.Exit(0)
		}

		handler.UpsertStagesOfSeasons(nameEndpoint, &stageData)
	}
}
