package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
	"strings"
)

//UpsertSeasons  -task upsertSeasons
func UpsertSeasons(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool) {

	// Получаем список сезонов для обработки
	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)
	listLeagueSeasons := queries.GetSeasons(stats)

	// Что включать в ответе
	var inc = make(types.Include)
	inc.SetSeasons()
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))

	// Обходим все сезоны
	for _, seasons := range listLeagueSeasons {
		for _, seasId := range seasons {

			// URL API
			url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seasId.SeasonId) + "?" + params.Encode()

			// Запрос к API
			resp, err := client.RequestToEndpoint(url)
			if err != nil {
				os.Exit(0)
			}

			var seasonData types.ResponseSeason
			err = json.Unmarshal(resp, &seasonData)
			if err != nil {
				db.ErrLog("UpsertSeasons", err)
				os.Exit(0)
			}

			handler.HandlersUpsertRound(nameEndpoint, seasonData.Data.Rounds.Data)

		}
	}
}
