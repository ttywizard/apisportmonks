package endpoints

import (
	"github.com/valyala/fastjson"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"os"
	"strconv"
)

//UpsertStandingsOfSeason -task upsertStandingOfSeasons
func UpsertStandingsOfSeason(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool) {

	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)

	// Список сезонов с этапами(stage) и имеющих турнирные таблицы
	seasons := queries.GetStageSeasonsHasStandings(stats)
	params := db.GetParam

	//Перебираем все этапы с турнирными таблицами
	for _, seas := range seasons {
		// URL API

		url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seas) + "/?" + params.Encode()

		//url := "https://soccer.sportmonks.com/api/v2.0/standings/season/17381?api_token=l9gjvrFitnrWjwTHFfGAxYiNp9vVsMmOikE5GAwyenaeNhxZ1gWqCGUQYqtc"
		//Лига чемпионов
		//url := "https://soccer.sportmonks.com/api/v2.0/standings/season/17299?api_token=l9gjvrFitnrWjwTHFfGAxYiNp9vVsMmOikE5GAwyenaeNhxZ1gWqCGUQYqtc&include=stages"
		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertStandingsOfSeason", err)
			continue
		}

		var p fastjson.Parser
		standingsData, err := p.ParseBytes(resp)
		if err != nil {
			db.ErrLog("UpsertStandingsOfSeason", err)
			os.Exit(0)
		}

		//Обработка данных
		handler.UpsertStandingsSeasonId(nameEndpoint, standingsData)
	}
}
