package endpoints

import (
	"encoding/json"
	"github.com/valyala/fastjson"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
	"strings"
)

// UpsertLeagues функция подготавливает запрос к endpoint leagues и передает обработку данных
// -task editLeague
func UpsertLeagues(nameEndpoint string) {

	// GET Параметры
	params := db.GetParam
	params.Add("include", "seasons,country")

	numPage := 1
	//Реализация пагинации
	for {
		params.Set("page", strconv.Itoa(numPage))
		// URL API
		url := db.Endpoints[nameEndpoint].Url + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			os.Exit(0)
		}

		var s types.ResponseLeague
		err = json.Unmarshal(resp, &s)
		if err != nil {
			os.Exit(0)
		}

		handler.UpsertLeagues(&s)

		//Пагинация
		var pageData handler.PaginationData
		pageData.Init()
		handler.PaginationHandler(&s.Meta.Pagination, &pageData)

		//Если текущая страница равна общему количеству страниц завершаем обработку
		if numPage > *s.Meta.Pagination.TotalPages || *s.Meta.Pagination.TotalPages == 1 {
			break
		}
		numPage++
	}
}

// UpsertLeaguesCurrentSeason функция подготавливает запрос к endpoint leagues и передает обработку данных
// -task editLeagueCurrentSeason
func UpsertLeaguesCurrentSeason(nameEndpoint string) {
	leagueUID := queries.GetListLeaguesId()

	// Что включать в ответе
	hT := []string{"season", "country"}
	var inc = make(types.Include)
	inc.SetHandler(hT)
	params := db.GetParam
	params.Add("include", strings.Trim(inc.GetStringIncludes(), ","))

	numPage := 1
	//Реализация пагинации
	for {
		params.Set("page", strconv.Itoa(numPage))
		// URL API
		url := db.Endpoints[nameEndpoint].Url + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			db.ErrLog("UpsertLeaguesCurrentSeason", err)
			os.Exit(0)
		}

		var s fastjson.Parser
		leaguesData, err := s.ParseBytes(resp)

		if err != nil {

			db.ErrLog("UpsertLeaguesCurrentSeason", err)
			os.Exit(0)
		}

		handler.HandlersUpsertCurrentSeason(leaguesData, leagueUID)

		//Пагинация
		//Если текущая страница равна общему количеству страниц завершаем обработку
		if leaguesData.Exists("meta", "pagination") {
			pagination := leaguesData.Get("meta", "pagination")

			if numPage > pagination.GetInt("total_pages") || pagination.GetInt("total_pages") == 1 {
				break
			}
			numPage++
		}
	}
}
