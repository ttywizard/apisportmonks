package endpoints

import (
	"encoding/json"
	handler "gitlab.com/ttywizard/apisportmonks/api/handlers"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strconv"
)

// UpsertRoundsOfSeason - добавляет, обновляет раунды сезонов
// -task upsertRoundsSeasonsId -seasonIsCurrent true
// -task upsertRoundsSeasonsId -leagueId 2 -seasonId 1275
func UpsertRoundsOfSeason(nameEndpoint string, leagueId int, seasonId int, seasonCurrent bool) {

	var stats db.Statistics
	stats.SetLeague(leagueId)
	stats.SetSeason(seasonId)
	stats.SetCurrent(seasonCurrent)

	// Список сезонов лиг
	seasons := queries.GetSeasonsIdData(stats)

	params := db.GetParam

	for _, seas := range seasons {
		// URL API
		url := db.Endpoints[nameEndpoint].Url + strconv.Itoa(seas.SeasonId) + "?" + params.Encode()

		// Запрос к API
		resp, err := client.RequestToEndpoint(url)
		if err != nil {
			os.Exit(0)
		}

		var roundData types.ResponseRound
		err = json.Unmarshal(resp, &roundData)
		if err != nil {
			db.ErrLog("UpsertRoundsOfSeason", err)
			os.Exit(0)
		}

		handler.HandlersUpsertRound(nameEndpoint, roundData.Data)
	}
}
