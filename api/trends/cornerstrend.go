package trends

import (
	"fmt"
	"github.com/lib/pq"
	"github.com/valyala/fastjson"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/utility"
	"math"
	"math/rand"
	"strconv"
	"strings"
)

const CornersTotalMarket = "Corners Over Under"

func CornersTotalTrends(matchData db.ListMatches, season string) error {
	//Поиск трендов
	h2hHome, _ := CornersH2HTotalTrends(matchData, season, "h_h2h")
	h2hAway, _ := CornersH2HTotalTrends(matchData, season, "a_h2h")
	h2h, _ := CornersH2HTotalTrends(matchData, season, "h2h")

	//Структура для хранения одной из Home или Away команд
	var HomeAway []TrendData

	//Количество трендов по типам
	h2hHomeSize := len(h2hHome)
	h2hAwaySize := len(h2hAway)
	h2hSize := len(h2h)

	//Если найдены индивидуальные тоталы обоих команд, выбираем какой из них используем
	if h2hHomeSize > 0 && h2hAwaySize > 0 {
		if utility.Rand(2) == 1 {
			HomeAway = h2hHome
		} else {
			HomeAway = h2hAway
		}
	} else if h2hHomeSize > 0 && h2hAwaySize == 0 {
		HomeAway = h2hHome
	} else if h2hHomeSize == 0 && h2hAwaySize > 0 {
		HomeAway = h2hAway
	}

	// 1) Если индивидуальные тренды не найдены а тренды h2h найдены, генерим факт
	// 2) Если найдены тренды обоих групп, для тренда матча генерим тренд, а для индивидуального факт
	if len(HomeAway) == 0 && h2hSize > 0 {
		InsertTrendAndFacts(h2h[utility.Rand(h2hSize)], "trend")
	} else if len(HomeAway) > 0 && h2hSize > 0 {
		r := utility.Rand(2)
		//Рандомно решаем вопрос о том какой тренд уходит на факты
		var trendOrFact []string

		if r == 1 {
			trendOrFact = append(trendOrFact, "fact")
			trendOrFact = append(trendOrFact, "trend")
		} else {
			trendOrFact = append(trendOrFact, "trend")
			trendOrFact = append(trendOrFact, "fact")
		}

		InsertTrendAndFacts(HomeAway[utility.Rand(len(HomeAway))], trendOrFact[0])
		InsertTrendAndFacts(h2h[utility.Rand(h2hSize)], trendOrFact[1])
	}

	return nil
}

//InsertTrendAndFacts - функция генерит тренды или факты исходя из найденных тенденций
func InsertTrendAndFacts(data TrendData, typeTrend string) {

	data.WhatText = typeTrend
	floatTotal, _ := strconv.ParseFloat(data.Total, 10)

	//Text
	tJ, _ := fastjson.Parse(queries.ListTrendsText[data.TypeTrend])
	textJson := tJ.Get(typeTrend)

	//Массив текстов
	var textTrend []*fastjson.Value

	//Определяем минимальный и максимальный тотал
	maxMinTotal, _ := fastjson.Parse(data.MaxMin)
	max, _ := maxMinTotal.Get("0").StringBytes()
	//min, _ := maxMinTotal.Get("1").StringBytes()

	maxTotal, _ := strconv.ParseFloat(string(max), 64)
	//minTotal, _ := strconv.ParseFloat(string(min), 64)

	if floatTotal == maxTotal {
		if utility.Rand(2) == 1 {
			data.Label = "Under"
			floatTotal += 1.0
		} else {
			data.Label = "Over"
		}
	} else if floatTotal < maxTotal {
		data.Label = "Over"
	}

	//Окончательная строка текста тренда или факта
	var FinalText string
	var Location string

	switch data.Context {
	case "h2h":
		textTrend = textJson.GetArray(data.Label)

		if len(textTrend) == 0 {
			return
		}
		rnText := utility.Rand(len(textTrend))
		t, _ := textTrend[rnText].StringBytes()
		FinalText = string(t)

	case "a_h2h", "h_h2h":
		textTrend = textJson.GetArray("Ind" + data.Label)
		if len(textTrend) == 0 {
			return
		}
		rnText := utility.Rand(len(textTrend))
		t, _ := textTrend[rnText].StringBytes()
		FinalText = string(t)

		//Определяем текст места игры команды
		var l []*fastjson.Value
		if data.Context == "a_h2h" {
			l = textJson.GetArray("LocationAway")
		} else {
			l = textJson.GetArray("LocationHome")
		}

		rnText = utility.Rand(len(l))
		t, _ = l[rnText].StringBytes()
		Location = string(t)
	}

	queryInsert := `INSERT INTO matchcenter_trend(number_match, number_trend,market_id,label, total,percent, context, match_id,team_id, trend_text, xxhash64, types) VALUES `
	// Если количество тренда равно матчам пишем тольк количество матчей инача "количество трендов" из "количество матчей"
	var trend string
	if data.NumberMatch == data.NumberTrend {
		trend = strconv.Itoa(data.NumberMatch)
	} else {
		trend = strconv.Itoa(data.NumberTrend) + " из " + strconv.Itoa(data.NumberMatch)
	}

	word := strings.Replace(FinalText, "[HOME_AWAY]", data.HomeAway, 1)
	word = strings.Replace(word, "[TEAM]", data.TeamName, 1)
	word = strings.Replace(word, "[TREND]", trend, 1)

	ln, _ := fastjson.Parse(data.LeagueName)
	lnSize := ln.GetArray()
	rn := rand.Intn(len(lnSize))
	leagueName, _ := lnSize[rn].StringBytes()

	word = strings.Replace(word, "[LEAGUE]", string(leagueName), 1)
	word = strings.Replace(word, "[SEASON]", data.Season, 1)
	word = strings.Replace(word, "[TEAM1]", data.HomeTeamName, 1)
	word = strings.Replace(word, "[TEAM2]", data.AwayTeamName, 1)
	word = strings.Replace(word, "[CARDS]", "желтых карточек", 1)

	if math.Mod(floatTotal, 1.0) > 0 {
		word = strings.Replace(word, "[TOTAL]", strconv.FormatFloat(floatTotal, 'f', 1, 64), 1)
	} else {
		word = strings.Replace(word, "[TOTAL]", strconv.FormatFloat(floatTotal, 'f', 0, 64), 1)
	}
	word = strings.Replace(word, "[LOCATION]", Location, 1)

	var sectionValues strings.Builder
	sectionValues.WriteString("(")
	sectionValues.WriteString(strconv.Itoa(data.NumberMatch) + ", ")
	sectionValues.WriteString(strconv.Itoa(data.NumberTrend) + ", ")
	sectionValues.WriteString(strconv.Itoa(data.MarketId) + ", ")
	sectionValues.WriteString("'" + data.Label + "', ")
	sectionValues.WriteString("'" + strconv.FormatFloat(floatTotal, 'f', 1, 64) + "', ")
	sectionValues.WriteString(strconv.FormatFloat(data.Percent, 'f', 1, 64) + ", ")
	sectionValues.WriteString("'" + data.Context + "', ")
	sectionValues.WriteString(strconv.Itoa(data.MatchId) + ", ")
	sectionValues.WriteString(strconv.Itoa(data.TeamId) + ", ")
	sectionValues.WriteString("'" + word + "', ")

	var h utility.HashData
	h.Init()
	h.MarkertId = data.MarketId
	h.MatchId = data.MatchId
	h.TeamId = data.TeamId
	*h.Context = data.Context
	*h.WhatText = data.WhatText

	sectionValues.WriteString(strconv.FormatUint(utility.GenerateHash64(h), 10) + ", ")
	sectionValues.WriteString("'" + data.WhatText + "'), ")

	if sectionValues.String() != "" {
		queryInsertTrend := queryInsert + strings.Trim(sectionValues.String(), ", ") + " ON CONFLICT (xxhash64) DO NOTHING"

		//Выполняем запрос
		_, err := db.Db.Exec(queryInsertTrend)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("InsertTrendAndFacts", err)
		}
	}
}

//CornersH2HTotalTrends Home Corners Over/Under - market_id: 136830894, поиск трендов H2H для хозяев, гостей и игры
func CornersH2HTotalTrends(matchData db.ListMatches, season string, context string) ([]TrendData, error) {

	var aTr []TrendData

	query := fmt.Sprintf(`SELECT   number_match
                                        , number_trend
                                        , market_id
                                        , total
                                        , percent
                                        , context
                                        , match_id
                                        , team_id
                                        , team_name
                                        , home_team_name
                                        , away_team_name
                                        , league_name
                                        , jsonb_build_array(MAX(total) OVER(), MIN(total) OVER()) AS max_min_trend
                                 FROM search_statistic_h2h_total_trends(
                                          '{"limit": %d,
                                             "league_id": %d,
                                             "home_team_id": %d,
                                             "away_team_id": %d,
                                             "event": "corners",
                                             "context": "%s",
                                             "match_id": %d,
                                             "seasons": "%s",
                                             "market_id": %d,
                                             "number_match": 5
                                            }'::JSONB) 
                                 ORDER BY percent DESC`,
		VarTrend.Limit, matchData.LeagueId, matchData.HomeTeamId, matchData.AwayTeamId, context, matchData.MatchId, season, queries.ListMarket[CornersTotalMarket])

	//Выполняем запрос
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("CornersH2HTotalTrends", err)
		return aTr, err
	}

	//Разбираем ответ
	for rows.Next() {
		var tr TrendData
		err = rows.Scan(&tr.NumberMatch, &tr.NumberTrend, &tr.MarketId, &tr.Total, &tr.Percent, &tr.Context,
			&tr.MatchId, &tr.TeamId, &tr.TeamName, &tr.HomeTeamName, &tr.AwayTeamName, &tr.LeagueName, &tr.MaxMin)

		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("CornersH2HTotalTrends", err)
			return aTr, err
		}
		tr.TypeTrend = CornersTotalMarket
		aTr = append(aTr, tr)
	}

	return aTr, nil
}
