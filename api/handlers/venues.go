package handler

import (
	"fmt"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func UpsertTeamsVenues(nameEndpoint string, teams *types.ResponseTeams) {
	//(id, provider_id, name, surface, address, capacity, image, city_name, coordinates) VALUES `
	sectionValues := ""
	arrValues := make(map[int]string)

	for _, team := range teams.Data {
		if team.Venue.Data.Id == 3876 {
			fmt.Println(team.Venue.Data.Id)
		}
		sectionValues = "("
		sectionValues += db.CastInterfaceString(team.Venue.Data.Id)
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(team.Venue.Data.Name)
		sectionValues += db.CastInterfaceString(team.Venue.Data.Surface)
		sectionValues += db.CastInterfaceString(team.Venue.Data.Address)
		sectionValues += db.CastInterfaceString(team.Venue.Data.Capacity)
		sectionValues += db.CastInterfaceString(team.Venue.Data.ImagePath)
		sectionValues += db.CastInterfaceString(team.Venue.Data.City)
		sectionValues += strings.Trim(db.CastInterfaceString(team.Venue.Data.Coordinates), ", ") + "), "
		arrValues[team.Venue.Data.Id] = sectionValues
	}

	if len(arrValues) > 0 {
		sectionValues = ""
		for _, v := range arrValues {
			sectionValues += v
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_venue"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_venue"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertTeamsVenues", err)
		}
	}
}
