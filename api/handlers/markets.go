package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func HandlerMarkets(markets *types.ResponseMarket) {

	sectionValues := ""
	for _, market := range markets.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(market.Id)
		sectionValues += strings.Trim(db.CastInterfaceString(market.Name), ", ") + "), "
	}

	queryInsert := db.SectionInsert["matchcenter_market"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_market"]

	//Выполняем запрос
	_, err := db.Db.Exec(queryInsert)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlerMarkets", err)
	}
}
