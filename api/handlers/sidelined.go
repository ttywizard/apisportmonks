package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func HandlersUpsertSidelined(nameEndpoint string, season *types.ResponseSeason, listSeasonsData map[int]db.SeasonData) {
	for _, fixture := range season.Data.Fixtures.Data {
		//player_id, season_id, team_id, description, start_date, end_date
		for _, player := range fixture.SideLined.Data {
			sectionValues := "("
			sectionValues += db.CastInterfaceString(player.PlayerId)
			sectionValues += db.CastInterfaceString(listSeasonsData[player.SeasonId].SeasonUid)
			sectionValues += db.CastInterfaceString(player.TeamId)
			sectionValues += db.CastInterfaceString(player.Description)
			sectionValues += db.CastInterfaceString(player.StartDate)
			sectionValues += strings.Trim(db.CastInterfaceString(player.EndDate), ", ") + "), "

			queryInsert := db.SectionInsert["matchcenter_sidelined"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_sidelined"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertSidelined", err)
			}
		}
	}
}
