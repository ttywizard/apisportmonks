package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
	"strings"
)

// UpsertLeagues добавляет или обновляет данные по лигам
func UpsertLeagues(leaguesData *types.ResponseLeague) {

	// мапа стран name -> uid
	listCountryUUID, err := queries.GetProviderCountry()
	if err != nil {
		os.Exit(0)
	}

	for _, leId := range leaguesData.Data {

		sectionValues := db.CastInterfaceString(leId.Id)
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(leId.Name)
		if leId.IsCup {
			sectionValues += "'Cup', "
		} else {
			sectionValues += "'League', "
		}

		sectionValues += db.CastInterfaceString(leId.LogoPath)
		sectionValues += db.CastInterfaceString(listCountryUUID[*leId.Country.Country.Name])
		queryInsert := db.SectionInsert["matchcenter_league"] + strings.Trim(sectionValues, ", ") + ")" + db.SectionConflict["matchcenter_league"]

		//Выполняем запрос
		rows, err := db.Db.Query(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertLeagues", err)
			continue
		}

		var luid string
		for rows.Next() {
			// uid лиги
			err = rows.Scan(&luid)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("UpsertLeagues", err)
				continue
			}
		}
		//Обновляем данные по сезону
		UpsertSeasons(&leId.Seasons, luid)
	}
}
