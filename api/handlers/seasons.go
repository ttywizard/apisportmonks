package handler

import (
	"github.com/lib/pq"
	"github.com/valyala/fastjson"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strconv"
	"strings"
)

//UpsertSeasons добавляет или обновляет данные по сезону
func UpsertSeasons(leaguesData *types.Seasons, leagueUID string) {
	sectionValues := ""
	for _, season := range leaguesData.Data {

		sectionValues += "("
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(leagueUID)
		sectionValues += db.CastInterfaceString(season.Id)

		// Сезон, год старта сезона
		seasIdArray := strings.Split(season.Name, "/")
		seasId, _ := strconv.Atoi(seasIdArray[0])
		sectionValues += db.CastInterfaceString(seasId)

		sectionValues += db.CastInterfaceString(season.Name)
		sectionValues += db.CastInterfaceString(season.IsCurrentSeason)
		sectionValues += db.CastInterfaceString(season.CurrentRoundId)
		sectionValues += strings.Trim(db.CastInterfaceString(season.CurrentStageId), ", ") + "), "
	}

	queryInsert := db.SectionInsert["matchcenter_season"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_season"]
	//Выполняем запрос
	_, err := db.Db.Exec(queryInsert)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("UpsertSeasons", err)
	}
}

func HandlersUpsertCurrentSeason(leaguesData *fastjson.Value, leagueUID map[int]string) {

	//Обходим все лиги и обновляем данные по текущим сезонам
	for _, leId := range leaguesData.GetArray("data") {

		sectionValues := "(" + db.CastInterfaceString(db.Prov.Id)
		//league_uid
		sectionValues += db.CastInterfaceString(leagueUID[leId.GetInt("id")])
		//id
		sectionValues += db.CastInterfaceString(leId.GetInt("season", "data", "id"))

		// Сезон, год старта сезона
		seasonName := string(leId.GetStringBytes("season", "data", "name"))
		seasIdArray := strings.Split(seasonName, "/")
		seasId, _ := strconv.Atoi(seasIdArray[0])
		sectionValues += db.CastInterfaceString(seasId)

		//name
		sectionValues += db.CastInterfaceString(seasonName)
		//current
		sectionValues += db.CastInterfaceString(leId.GetBool("season", "data", "is_current_season"))

		//current_round_id
		if leId.Get("current_round_id") == nil {
			sectionValues += "NULL, "
		} else {
			sectionValues += db.CastInterfaceString(leId.GetInt("current_round_id"))
		}

		//current_stage_id
		if leId.Get("current_stage_id") == nil {
			sectionValues += "NULL, "
		} else {
			sectionValues += db.CastInterfaceString(leId.GetInt("current_stage_id"))
		}

		queryInsert := db.SectionInsert["matchcenter_season"] + strings.Trim(sectionValues, ", ") + ")" + db.SectionConflict["matchcenter_season"]

		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertCurrentSeason", err)
			continue
		}
	}
}
