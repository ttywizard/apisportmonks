package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"reflect"
	"strings"
)

func HandlersUpsertRefereesSeasons(nameEndpoint string, season *types.ResponseSeason) {
	arrValues := make(map[int]string)
	for _, fixture := range season.Data.Fixtures.Data {
		sectionValues := ""
		// Рефери
		if !reflect.ValueOf(fixture.RefereeId).IsNil() {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.Referee.Data.Fullname), ", ") + "), "
			arrValues[fixture.Referee.Data.Id] = sectionValues

		}

		//Первый помощник
		if !reflect.ValueOf(fixture.Assistants.FirstAssistantId).IsNil() && *fixture.Assistants.FirstAssistantId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.FirstAssistant.Data.Fullname), ", ") + "), "
			arrValues[fixture.FirstAssistant.Data.Id] = sectionValues
		}

		//Второй помощник
		if !reflect.ValueOf(fixture.Assistants.SecondAssistantId).IsNil() && *fixture.Assistants.SecondAssistantId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.SecondAssistant.Data.Fullname), ", ") + "), "
			arrValues[fixture.SecondAssistant.Data.Id] = sectionValues
		}

		//Официальный представитель
		if !reflect.ValueOf(fixture.Assistants.FourthOfficialId).IsNil() && *fixture.Assistants.FourthOfficialId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.FourthOfficial.Data.Fullname), ", ") + "), "
			arrValues[fixture.FourthOfficial.Data.Id] = sectionValues
		}
	}

	sectionValues := ""
	if len(arrValues) > 0 {
		for _, v := range arrValues {
			sectionValues += v
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_referee"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_referee"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertRefereesSeasons", err)
		}
	}
}

func HandlersUpsertReferees(nameEndpoint string, fixtures *types.ResponseFixtures) {
	arrValues := make(map[int]string)
	for _, fixture := range fixtures.Data {
		sectionValues := ""
		// Рефери
		if !reflect.ValueOf(fixture.RefereeId).IsNil() {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.Referee.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.Referee.Data.Fullname), ", ") + "), "
			arrValues[fixture.Referee.Data.Id] = sectionValues

		}

		//Первый помощник
		if !reflect.ValueOf(fixture.Assistants.FirstAssistantId).IsNil() && *fixture.Assistants.FirstAssistantId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.FirstAssistant.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.FirstAssistant.Data.Fullname), ", ") + "), "
			arrValues[fixture.FirstAssistant.Data.Id] = sectionValues
		}

		//Второй помощник
		if !reflect.ValueOf(fixture.Assistants.SecondAssistantId).IsNil() && *fixture.Assistants.SecondAssistantId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.SecondAssistant.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.SecondAssistant.Data.Fullname), ", ") + "), "
			arrValues[fixture.SecondAssistant.Data.Id] = sectionValues
		}

		//Официальный представитель
		if !reflect.ValueOf(fixture.Assistants.FourthOfficialId).IsNil() && *fixture.Assistants.FourthOfficialId != 0 {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Id)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.CommonName)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Firstname)
			sectionValues += db.CastInterfaceString(fixture.FourthOfficial.Data.Lastname)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.FourthOfficial.Data.Fullname), ", ") + "), "
			arrValues[fixture.FourthOfficial.Data.Id] = sectionValues
		}
	}

	sectionValues := ""
	if len(arrValues) > 0 {
		for _, v := range arrValues {
			sectionValues += v
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_referee"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_referee"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertReferees", err)
		}
	}
}
