package handler

import (
	"github.com/OneOfOne/xxhash"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"reflect"
	"strconv"
	"strings"
)

//HandlersOdds -task upsertOdds -fromDate 2021-07-21 -toDate 2021-07-22
func HandlersOdds(nameEndpoint string, fixtures *types.ResponseFixtures) {

	// Обходим события
	for _, fixture := range fixtures.Data {
		//Обходим типы ставок
		for _, market := range fixture.OddsD.Data {
			//Только нужные типы ставок
			if _, ok := queries.MarketsData[market.Id]; ok {
				//Обходим бк
				for _, bk := range market.Bookmaker.Data {
					//Только нужные бк
					if _, ok := queries.BookmakersData[bk.Id]; ok {
						sectionValues := ""
						arrValues := make(map[uint64]string)
						//Обходим данные по ставкам
						for _, odds := range bk.Odds.Data {

							var StrOdds strings.Builder
							StrOdds.WriteString(strconv.Itoa(market.Id))
							StrOdds.WriteString(strconv.Itoa(bk.Id))
							StrOdds.WriteString(strconv.Itoa(fixture.Id))
							if reflect.ValueOf(odds.Total).IsNil() == false {
								StrOdds.WriteString(*odds.Total)
							} else {
								StrOdds.WriteString("")
							}
							StrOdds.WriteString(*odds.Label)

							hashLineUps := xxhash.New64()
							hashLineUps.WriteString(StrOdds.String())
							xxh := hashLineUps.Sum64()

							//market_id
							sectionValues = "("
							sectionValues += db.CastInterfaceString(market.Id)
							//bookmaker_id
							sectionValues += db.CastInterfaceString(bk.Id)
							//match_id
							sectionValues += db.CastInterfaceString(fixture.Id)
							//Value
							if odds.Value.String == "null" {
								sectionValues += "NULL, "
							} else {
								vF, _ := strconv.ParseFloat(odds.Value.String, 64)
								sectionValues += db.CastInterfaceString(vF)
							}

							//Handicap
							if odds.Value.String == "null" {
								sectionValues += "NULL, "
							} else {
								sectionValues += db.CastInterfaceString(odds.Handicap.String)
							}
							//total
							sectionValues += db.CastInterfaceString(odds.Total)
							//label
							sectionValues += db.CastInterfaceString(odds.Label)
							//probability
							sectionValues += db.CastInterfaceString(odds.Probability)
							//dp3
							if reflect.ValueOf(odds.Dp3).IsNil() || *odds.Dp3 == "null" {
								sectionValues += "NULL, "
							} else {
								vF, _ := strconv.ParseFloat(*odds.Dp3, 64)
								sectionValues += db.CastInterfaceString(vF)
							}

							//american
							sectionValues += db.CastInterfaceString(odds.American)
							//factional, winning, stop, bookmaker_event_id, last_update, timezone_type, timezone)
							sectionValues += db.CastInterfaceString(odds.Factional)
							if reflect.ValueOf(odds.Winning).IsNil() {
								sectionValues += "NULL, "
							} else {
								if *odds.Winning {
									sectionValues += "'t', "
								} else {
									sectionValues += "'f', "
								}
							}

							sectionValues += db.CastInterfaceString(odds.Stop)
							sectionValues += db.CastInterfaceString(odds.BookmakerEventId.Int)
							sectionValues += db.CastInterfaceString(odds.LastUpdate.Date)
							sectionValues += db.CastInterfaceString(odds.LastUpdate.TimezoneType)
							sectionValues += db.CastInterfaceString(odds.LastUpdate.Timezone)
							sectionValues += strings.Trim(db.CastInterfaceString(xxh), ", ") + "), "
							arrValues[xxh] = sectionValues
						}
						sectionValuesItog := ""
						if len(arrValues) > 0 {
							for _, v := range arrValues {
								sectionValuesItog += v
							}
						}

						if sectionValuesItog != "" {
							queryInsert := db.SectionInsert["matchcenter_odds"] + strings.Trim(sectionValuesItog, ", ") + db.SectionConflict["matchcenter_odds"]
							//Выполняем запрос
							_, err := db.Db.Exec(queryInsert)
							if err, ok := err.(*pq.Error); ok {
								db.PqErrorError("HandlersOdds", err)
							}
						}

					}
				}
			}
		}
	}
}

func HandlersProbability(nameEndpoint string, fixtures *types.ResponseFixtures) {

	for _, fixture := range fixtures.Data {

		//1X2
		sectionValues := "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'3Way Result', NULL, "
		sectionValues += "'Draw', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Draw), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'3Way Result', NULL, "
		sectionValues += "'Home', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Home), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'3Way Result', NULL, "
		sectionValues += "'Away', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Away), ", ") + "), "

		// Over/Under 2.5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'Over/Under 2.5', "
		sectionValues += "'2.5', 'Over',"
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Over25), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'Over/Under 2.5', "
		sectionValues += "'2.5', 'Under', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Under25), ", ") + "), "

		// Over/Under 3.5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'Over/Under 3.5', "
		sectionValues += "'3.5', 'Over', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Over35), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'Over/Under 3.5', "
		sectionValues += "'3.5', 'Under',  "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Under35), ", ") + "), "

		//btts
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'BTTS', "
		sectionValues += "NULL, 'btts', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.Btts), ", ") + "), "

		// HT_over/under_0_5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'HT Over/Under 0.5', "
		sectionValues += "'0.5', 'Under', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.HTUnder05), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'HT Over/Under 0.5', "
		sectionValues += "'0.5', 'Over', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.HTOver05), ", ") + "), "

		// HT_over/under_1_5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'HT Over/Under 1.5', "
		sectionValues += "'1.5', 'Under', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.HTUnder15), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'HT Over/Under 1.5', "
		sectionValues += "'1.5', 'Over', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.HTOver15), ", ") + "), "

		// AT_over/under_0_5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'AT Over/Under 0.5', "
		sectionValues += "'0.5', 'Under', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.ATUnder05), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'AT Over/Under 0.5', "
		sectionValues += "'0.5', 'Over', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.ATOver05), ", ") + "), "

		// AT_over/under_1_5
		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'AT Over/Under 1.5', "
		sectionValues += "'1.5', 'Under', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.ATUnder15), ", ") + "), "

		sectionValues += "(" + db.CastInterfaceString(fixture.Id)
		sectionValues += "'AT Over/Under 1.5', "
		sectionValues += "'1.5', 'Over', "
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Probabilities.Data.Predictions.ATOver15), ", ") + "), "

		//correct_score
		for key, val := range fixture.Probabilities.Data.Predictions.CorrectScore {

			sectionValues += "(" + db.CastInterfaceString(fixture.Id)
			sectionValues += "'Correct Score', "
			sectionValues += "NULL, '" + key + "', "
			sectionValues += strings.Trim(db.CastInterfaceString(val), ", ") + "), "
		}

		if sectionValues != "" {
			queryInsert := db.SectionInsert["matchcenter_probability"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_probability"]

			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersProbability", err)
			}
		}
	}
}
