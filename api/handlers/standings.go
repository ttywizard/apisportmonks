package handler

import (
	"github.com/lib/pq"
	"github.com/valyala/fastjson"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/utility"
	"strings"
)

func UpsertStandingsSeasonId(nameEndpoint string, standings *fastjson.Value) {

	//Вехний уровень массива данных
	dataArray := standings.GetArray("data")

	for _, dataSeason := range dataArray {

		standingLevel := dataSeason.GetArray("standings", "data")

		// Если таблица существует и данные находятся на первом уровне
		if len(standingLevel) > 0 && standingLevel[0].Exists("position") {
			ParseStandings(dataSeason, nameEndpoint)
		}

		// Данные находятся на втором уровне
		if len(standingLevel) > 0 && standingLevel[0].Exists("id") {
			for _, standing := range standingLevel {
				ParseStandings(standing, nameEndpoint)
			}
		}

		//Обновление формы
		utility.UpdateStandingsLeagueForm(dataSeason.GetInt("season_id"), dataSeason.GetInt("league_id"), "all", true)
		utility.UpdateStandingsLeagueForm(dataSeason.GetInt("season_id"), dataSeason.GetInt("league_id"), "home", true)
		utility.UpdateStandingsLeagueForm(dataSeason.GetInt("season_id"), dataSeason.GetInt("league_id"), "away", true)
	}
}

func ParseStandings(standing *fastjson.Value, nameEndpoint string) {

	if len(standing.GetArray("standings", "data")) > 0 {
		sectionValues := ""
		for _, standingData := range standing.GetArray("standings", "data") {
			seasonId := standing.GetInt("season_id")
			sectionValues += "("

			//provider_id
			sectionValues += db.CastInterfaceString(db.Prov.Id)
			//team_id
			sectionValues += db.CastInterfaceString(standingData.GetInt("team_id"))
			//league_id
			sectionValues += db.CastInterfaceString(standing.GetInt("league_id"))
			//season_id
			sectionValues += db.CastInterfaceString(standing.GetInt("season_id"))
			//stage_id
			sectionValues += db.CastInterfaceString(standing.GetInt("stage_id"))
			//round_id
			sectionValues += db.CastInterfaceString(standing.GetInt("round_id"))
			//round_name
			sectionValues += db.CastInterfaceString(standing.GetInt("round_name"))
			//position
			sectionValues += db.CastInterfaceString(standingData.GetInt("position"))

			//Секция итоговой информации
			//overall_played
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "games_played"))
			//overall_win
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "won"))
			//overall_draw
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "draw"))
			//overall_loss
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "lost"))
			//overall_points
			if standingData.Exists("total", "points") {
				sectionValues += db.CastInterfaceString(standingData.GetInt("total", "points"))
			} else {
				OverallPoints := (standingData.GetInt("overall", "won") * queries.SeasonPointRules[seasonId].Win) + (standingData.GetInt("overall", "draw") * queries.SeasonPointRules[seasonId].Draw)
				sectionValues += db.CastInterfaceString(OverallPoints)
			}

			//overall_goal_for
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "goals_scored"))
			//overall_goal_against
			sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "goals_against"))
			//overall_goal_difference
			if standingData.Get("overall", "goals_scored") == nil {
				sectionValues += "0, "
			} else {
				sectionValues += db.CastInterfaceString(standingData.GetInt("overall", "goals_scored") - standingData.GetInt("overall", "goals_against"))
			}

			//Секция информации по домашним играм
			//home_played
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "games_played"))
			//home_win
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "won"))
			//home_draw
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "draw"))
			//home_loss
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "lost"))
			//home_points
			if standingData.Exists("home", "points") {
				sectionValues += db.CastInterfaceString(standingData.GetInt("home", "points"))
			} else {
				OverallPoints := (standingData.GetInt("home", "won") * queries.SeasonPointRules[seasonId].Win) + (standingData.GetInt("home", "draw") * queries.SeasonPointRules[seasonId].Draw)
				sectionValues += db.CastInterfaceString(OverallPoints)
			}

			//home_goal_for
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "goals_scored"))
			//home_goal_against
			sectionValues += db.CastInterfaceString(standingData.GetInt("home", "goals_against"))
			//home_goal_difference
			if standingData.Get("home", "goals_scored") == nil {
				sectionValues += "0, "
			} else {
				sectionValues += db.CastInterfaceString(standingData.GetInt("home", "goals_scored") - standingData.GetInt("home", "goals_against"))
			}

			//Секция информации по играм на выезде
			//away_played
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "games_played"))
			//away_win
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "won"))
			//away_draw
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "draw"))
			//away_loss
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "lost"))
			//away_points
			if standingData.Exists("away", "points") {
				sectionValues += db.CastInterfaceString(standingData.GetInt("away", "points"))
			} else {
				OverallPoints := (standingData.GetInt("away", "won") * queries.SeasonPointRules[seasonId].Win) + (standingData.GetInt("away", "draw") * queries.SeasonPointRules[seasonId].Draw)
				sectionValues += db.CastInterfaceString(OverallPoints)
			}

			//away_goal_for
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "goals_scored"))
			//away_goal_against
			sectionValues += db.CastInterfaceString(standingData.GetInt("away", "goals_against"))
			//away_goal_difference
			if standingData.Get("away", "goals_scored") == nil {
				sectionValues += "0, "
			} else {
				sectionValues += db.CastInterfaceString(standingData.GetInt("away", "goals_scored") - standingData.GetInt("away", "goals_against"))
			}

			//Название группы -> UUID group_name
			//group_name
			if standingData.Get("group_name") == nil {
				sectionValues += "NULL, "
			} else {
				if val, ok := queries.ListGroupsName[string(standingData.GetStringBytes("group_name"))]; ok {
					sectionValues += db.CastInterfaceString(val)
				} else {
					sectionValues += "NULL, "
				}
			}

			//group_id
			sectionValues += db.CastInterfaceString(standingData.GetInt("group_id"))

			// Правила таблиц(правило - результат) куда команда по итогам попадает Лига Чемпионов, Лига Европы или выбывает
			//result
			if standingData.Get("result") == nil {
				sectionValues += "NULL), "
			} else {
				if val, ok := queries.ListStandingRules[string(standingData.GetStringBytes("result"))]; ok {
					sectionValues += strings.Trim(db.CastInterfaceString(val), ", ") + "),"
				} else {
					sectionValues += "NULL), "
				}
			}
		}

		if sectionValues != "" {
			queryInsert := db.SectionInsert["matchcenter_standings_stage"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_standings_stage"]

			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("UpsertStandingsSeasonId", err)
			}
		}
	}
}
