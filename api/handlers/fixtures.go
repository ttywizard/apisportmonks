package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"reflect"
	"strconv"
	"strings"
)

func HandlersFixturesSeason(nameEndpoint string, season *types.ResponseSeason, leagueUUID string, seasonUUID string) {

	sectionValues := ""

	for _, fixture := range season.Data.Fixtures.Data {

		sectionValues += "("
		// ID fixture
		sectionValues += db.CastInterfaceString(fixture.Id)
		//league_uid
		sectionValues += db.CastInterfaceString(leagueUUID)
		//league_id
		sectionValues += db.CastInterfaceString(fixture.LeagueId)
		//season_uid
		sectionValues += db.CastInterfaceString(seasonUUID)
		//season_id
		sectionValues += db.CastInterfaceString(fixture.SeasonId)
		//round_id
		sectionValues += db.CastInterfaceString(fixture.RoundId)
		//stage_id
		sectionValues += db.CastInterfaceString(fixture.StageId)
		//group_id
		sectionValues += db.CastInterfaceString(fixture.GroupId)
		//match_date
		sectionValues += db.CastInterfaceString(fixture.TimeData.StartingAt.DateTime)
		//venue_id
		sectionValues += db.CastInterfaceString(fixture.VenueId)
		//referee_id
		sectionValues += db.CastInterfaceString(fixture.RefereeId)
		//first_assistant_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.FirstAssistantId)
		//second_assistant_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.SecondAssistantId)
		//fourth_official_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.FourthOfficialId)
		//home_team_id
		sectionValues += db.CastInterfaceString(fixture.LocalteamId)
		//away_team_id
		sectionValues += db.CastInterfaceString(fixture.VisitorteamId)
		//commentaries
		sectionValues += db.CastInterfaceString(fixture.Commentaries)
		//attendance
		sectionValues += db.CastInterfaceString(fixture.Attendance)

		// pitch, neutral_venue, home_formation, away_formation, home_goals,
		// Состояние поля pitch
		if reflect.ValueOf(fixture.Pitch).IsNil() {
			sectionValues += "NULL, "
		} else {
			sectionValues += db.CastInterfaceString(queries.MapPitchNameUuid[*fixture.Pitch])
		}
		//neutral_venue
		sectionValues += db.CastInterfaceString(fixture.NeutralVenue)
		//home_formation
		sectionValues += db.CastInterfaceString(fixture.Formations.LocalteamFormation)
		//away_formation
		sectionValues += db.CastInterfaceString(fixture.Formations.VisitorteamFormation)

		if *fixture.TimeData.Status == "NS" ||
			*fixture.TimeData.Status == "CANCL" ||
			*fixture.TimeData.Status == "POSTP" ||
			*fixture.TimeData.Status == "DELAYED" ||
			*fixture.TimeData.Status == "Deleted" {

			sectionValues += "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "

		} else {
			//home_goals
			sectionValues += db.CastInterfaceString(fixture.Scores.LocalteamScore)
			//away_goals
			sectionValues += db.CastInterfaceString(fixture.Scores.VisitorteamScore)

			//ftr Определяем как завершился матч  Full Time Result (H=Home Win, D=Draw, A=Away Win)
			if reflect.ValueOf(fixture.Scores.FtScore).IsNil() {
				sectionValues += "NULL, NULL, NULL, "
			} else {
				sectionValues += GetWinner(*fixture.Scores.FtScore)
				// Full time teams goals
				fts := strings.Split(*fixture.Scores.FtScore, "-")
				//fthg
				sectionValues += db.CastInterfaceString(fts[0])
				//ftag
				sectionValues += db.CastInterfaceString(fts[1])
			}

			//htr Если есть данные по таймам(1 тайм) Half Time Result (H=Home Win, D=Draw, A=Away Win)
			if reflect.ValueOf(fixture.Scores.HtScore).IsNil() {
				sectionValues += "NULL, NULL, NULL, "
			} else {
				sectionValues += GetWinner(*fixture.Scores.HtScore)
				// Half time teams goals
				hts := strings.Split(*fixture.Scores.HtScore, "-")
				//hthg
				sectionValues += db.CastInterfaceString(hts[0])
				//htag
				sectionValues += db.CastInterfaceString(hts[1])
			}

			// Если есть данные по дополнительным таймам), кто сколько забил
			if reflect.ValueOf(fixture.Scores.EtScore).IsNil() {
				sectionValues += "NULL, NULL, "
			} else {
				// Extra time teams goals
				ets := strings.Split(*fixture.Scores.EtScore, "-")
				//ethg
				sectionValues += db.CastInterfaceString(ets[0])
				//etag
				sectionValues += db.CastInterfaceString(ets[1])
			}

			// Если есть данные по пенальти, кто сколько забил
			if reflect.ValueOf(fixture.Scores.PsScore).IsNil() {
				sectionValues += "NULL, NULL, "
			} else {
				// Extra time teams goals
				pts := strings.Split(*fixture.Scores.PsScore, "-")
				//phg
				sectionValues += db.CastInterfaceString(pts[0])
				//pag
				sectionValues += db.CastInterfaceString(pts[1])
			}
		}
		//status
		sectionValues += db.CastInterfaceString(fixture.TimeData.Status)
		//elapsed
		sectionValues += db.CastInterfaceString(fixture.TimeData.Minute.Int)
		//added_time
		sectionValues += db.CastInterfaceString(fixture.TimeData.AddedTime.Int)
		//extra_minute
		sectionValues += db.CastInterfaceString(fixture.TimeData.ExtraMinute.Int)
		//home_coache_id
		sectionValues += db.CastInterfaceString(fixture.Coach.LocalteamCoachId)
		//away_coache_id
		sectionValues += db.CastInterfaceString(fixture.Coach.VisitorteamCoachId)
		//home_position
		sectionValues += db.CastInterfaceString(fixture.Standings.LocalteamPosition)
		//away_position
		sectionValues += db.CastInterfaceString(fixture.Standings.VisitorteamPosition)

		//home_color Цвета формы команд
		if reflect.ValueOf(fixture.Colors.Localteam.Color).IsNil() {
			sectionValues += "'{}', "
		} else {
			sectionValues += "'{\"" + *fixture.Colors.Localteam.Color + "\", \"" + *fixture.Colors.Localteam.KitColors + "\"}', "
		}

		//away_color
		if reflect.ValueOf(fixture.Colors.Visitorteam.Color).IsNil() {
			sectionValues += "'{}', "
		} else {
			sectionValues += "'{\"" + *fixture.Colors.Visitorteam.Color + "\", \"" + *fixture.Colors.Visitorteam.KitColors + "\"}', "
		}

		//В раундах на вылет, какая команда вышла в следующий раунд + (общий счет в двух матчевых играх)
		// Аггрегация отсутсвует
		if reflect.ValueOf(fixture.Aggregate).IsNil() {
			sectionValues += "NULL, NULL, "
		} else {
			// Раунд из одного матча
			if *fixture.Leg == "1/1" {
				sectionValues += db.CastInterfaceString(fixture.Aggregate.Data.Winner)
				sectionValues += "NULL, "
				// Раунд из двух матчей
			} else if *fixture.Leg == "2/2" {
				sectionValues += db.CastInterfaceString(fixture.Aggregate.Data.Winner)
				// Если общий результат отсутсвует
				if reflect.ValueOf(fixture.Aggregate.Data.Result).IsNil() {
					sectionValues += "NULL, "
				} else {
					sectionValues += db.CastInterfaceString(strings.ReplaceAll(*fixture.Aggregate.Data.Result, " ", ""))
				}
			} else {
				sectionValues += "NULL, NULL, "
			}
		}

		//s_events События присутсвуют
		if len(fixture.Events.Data) != 0 {
			sectionValues += "'t', "
		} else {
			sectionValues += "'f', "
		}
		//s_lineups Составы присутсвуют
		if len(fixture.Lineup.Data) != 0 {
			sectionValues += "'t', "
		} else {
			sectionValues += "'f', "
		}

		//leg
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Leg), ", ") + "), "
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_match"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_match"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersFixturesSeason", err)
		}
	}
}

//HandlersUpsertFixtures обновляет данные fixture событий (Livescore, Between Date)
func HandlersUpsertFixtures(nameEndpoint string, fixtures *types.ResponseFixtures) {

	sectionValues := ""

	var stats db.Statistics
	seasData := queries.GetSeasonsIdData(stats)

	for _, fixture := range fixtures.Data {

		sectionValues += "("
		// ID fixture
		sectionValues += db.CastInterfaceString(fixture.Id)
		//league_uid
		sectionValues += db.CastInterfaceString(seasData[fixture.SeasonId].LeagueUid)
		//league_id
		sectionValues += db.CastInterfaceString(fixture.LeagueId)
		//season_uid
		sectionValues += db.CastInterfaceString(seasData[fixture.SeasonId].SeasonUid)
		//season_id
		sectionValues += db.CastInterfaceString(fixture.SeasonId)
		//round_id
		sectionValues += db.CastInterfaceString(fixture.RoundId)
		//stage_id
		sectionValues += db.CastInterfaceString(fixture.StageId)
		//group_id
		sectionValues += db.CastInterfaceString(fixture.GroupId)
		//match_date
		sectionValues += db.CastInterfaceString(fixture.TimeData.StartingAt.DateTime)
		//venue_id
		sectionValues += db.CastInterfaceString(fixture.VenueId)
		//referee_id
		sectionValues += db.CastInterfaceString(fixture.RefereeId)
		//first_assistant_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.FirstAssistantId)
		//second_assistant_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.SecondAssistantId)
		//fourth_official_id
		sectionValues += db.CastInterfaceString(fixture.Assistants.FourthOfficialId)
		//home_team_id
		sectionValues += db.CastInterfaceString(fixture.LocalteamId)
		//away_team_id
		sectionValues += db.CastInterfaceString(fixture.VisitorteamId)
		//commentaries
		sectionValues += db.CastInterfaceString(fixture.Commentaries)
		//attendance
		sectionValues += db.CastInterfaceString(fixture.Attendance)

		// pitch, neutral_venue, home_formation, away_formation, home_goals,
		// Состояние поля pitch
		if reflect.ValueOf(fixture.Pitch).IsNil() {
			sectionValues += "NULL, "
		} else {
			sectionValues += db.CastInterfaceString(queries.MapPitchNameUuid[*fixture.Pitch])
		}
		//neutral_venue
		sectionValues += db.CastInterfaceString(fixture.NeutralVenue)
		//home_formation
		sectionValues += db.CastInterfaceString(fixture.Formations.LocalteamFormation)
		//away_formation
		sectionValues += db.CastInterfaceString(fixture.Formations.VisitorteamFormation)

		if *fixture.TimeData.Status == "NS" ||
			*fixture.TimeData.Status == "CANCL" ||
			*fixture.TimeData.Status == "POSTP" ||
			*fixture.TimeData.Status == "DELAYED" ||
			*fixture.TimeData.Status == "Deleted" {

			sectionValues += "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "

		} else {
			//home_goals
			sectionValues += db.CastInterfaceString(fixture.Scores.LocalteamScore)
			//away_goals
			sectionValues += db.CastInterfaceString(fixture.Scores.VisitorteamScore)

			//ftr Определяем как завершился матч  Full Time Result (H=Home Win, D=Draw, A=Away Win)
			if reflect.ValueOf(fixture.Scores.FtScore).IsNil() {
				sectionValues += "NULL, NULL, NULL, "
			} else {
				sectionValues += GetWinner(*fixture.Scores.FtScore)
				// Full time teams goals
				fts := strings.Split(*fixture.Scores.FtScore, "-")
				//fthg
				sectionValues += db.CastInterfaceString(fts[0])
				//ftag
				sectionValues += db.CastInterfaceString(fts[1])
			}

			//htr Если есть данные по таймам(1 тайм) Half Time Result (H=Home Win, D=Draw, A=Away Win)
			if reflect.ValueOf(fixture.Scores.HtScore).IsNil() {
				sectionValues += "NULL, NULL, NULL, "
			} else {
				sectionValues += GetWinner(*fixture.Scores.HtScore)
				// Half time teams goals
				hts := strings.Split(*fixture.Scores.HtScore, "-")
				//hthg
				sectionValues += db.CastInterfaceString(hts[0])
				//htag
				sectionValues += db.CastInterfaceString(hts[1])
			}

			// Если есть данные по дополнительным таймам), кто сколько забил
			if reflect.ValueOf(fixture.Scores.EtScore).IsNil() {
				sectionValues += "NULL, NULL, "
			} else {
				// Extra time teams goals
				ets := strings.Split(*fixture.Scores.EtScore, "-")
				//ethg
				sectionValues += db.CastInterfaceString(ets[0])
				//etag
				sectionValues += db.CastInterfaceString(ets[1])
			}

			// Если есть данные по пенальти, кто сколько забил
			if reflect.ValueOf(fixture.Scores.PsScore).IsNil() {
				sectionValues += "NULL, NULL, "
			} else {
				// Extra time teams goals
				pts := strings.Split(*fixture.Scores.PsScore, "-")
				//phg
				sectionValues += db.CastInterfaceString(pts[0])
				//pag
				sectionValues += db.CastInterfaceString(pts[1])
			}
		}
		//status
		sectionValues += db.CastInterfaceString(fixture.TimeData.Status)
		//elapsed
		sectionValues += db.CastInterfaceString(fixture.TimeData.Minute.Int)
		//added_time
		sectionValues += db.CastInterfaceString(fixture.TimeData.AddedTime.Int)
		//extra_minute
		sectionValues += db.CastInterfaceString(fixture.TimeData.ExtraMinute.Int)
		//home_coache_id
		sectionValues += db.CastInterfaceString(fixture.Coach.LocalteamCoachId)
		//away_coache_id
		sectionValues += db.CastInterfaceString(fixture.Coach.VisitorteamCoachId)
		//home_position
		sectionValues += db.CastInterfaceString(fixture.Standings.LocalteamPosition)
		//away_position
		sectionValues += db.CastInterfaceString(fixture.Standings.VisitorteamPosition)

		//home_color Цвета формы команд
		if reflect.ValueOf(fixture.Colors.Localteam.Color).IsNil() {
			sectionValues += "'{}', "
		} else {
			sectionValues += "'{\"" + *fixture.Colors.Localteam.Color + "\", \"" + *fixture.Colors.Localteam.KitColors + "\"}', "
		}

		//away_color
		if reflect.ValueOf(fixture.Colors.Visitorteam.Color).IsNil() {
			sectionValues += "'{}', "
		} else {
			sectionValues += "'{\"" + *fixture.Colors.Visitorteam.Color + "\", \"" + *fixture.Colors.Visitorteam.KitColors + "\"}', "
		}

		//В раундах на вылет, какая команда вышла в следующий раунд + (общий счет в двух матчевых играх)
		if reflect.ValueOf(fixture.Aggregate).IsNil() {
			sectionValues += "NULL, NULL, "
		} else {
			if *fixture.Leg == "1/1" {
				sectionValues += db.CastInterfaceString(fixture.Aggregate.Data.Winner)
				sectionValues += "NULL, "
			} else if *fixture.Leg == "2/2" {
				sectionValues += db.CastInterfaceString(fixture.Aggregate.Data.Winner)
				sectionValues += db.CastInterfaceString(strings.ReplaceAll(*fixture.Aggregate.Data.Result, " ", ""))
			} else {
				sectionValues += "NULL, NULL, "
			}
		}

		//s_events События присутсвуют
		if len(fixture.Events.Data) != 0 {
			sectionValues += "'t', "
		} else {
			sectionValues += "'f', "
		}
		//s_lineups Составы присутсвуют
		if len(fixture.Lineup.Data) != 0 {
			sectionValues += "'t', "
		} else {
			sectionValues += "'f', "
		}

		//leg
		sectionValues += strings.Trim(db.CastInterfaceString(fixture.Leg), ", ") + "), "
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_match"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_match"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertFixtures", err)
		}
	}
}

func HandlersUpsertFixturesStatsSeasons(nameEndpoint string, season *types.ResponseSeason) {

	//(team_id,match_id,shots_total,shots_ongoal,shots_blocked,shots_offgoal,shots_insidebox,shots_outsidebox,passes_total,
	//passes_accurate,passes_percentage,attacks,dangerous_attacks,fouls,corners,offsides,possessiontime,yellowcards,redcards,
	//yellowredcards,saves,substitutions,goal_kick,goal_attempts,free_kick,throw_in,ball_safe,goals,penalties,injuries,tackles)
	for _, fixture := range season.Data.Fixtures.Data {
		sectionValues := ""
		for _, stats := range fixture.Stats.Data {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(stats.TeamId)
			sectionValues += db.CastInterfaceString(stats.FixtureId)
			sectionValues += db.CastInterfaceString(stats.Shots.Total)
			sectionValues += db.CastInterfaceString(stats.Shots.Ongoal)
			sectionValues += db.CastInterfaceString(stats.Shots.Blocked)
			sectionValues += db.CastInterfaceString(stats.Shots.Offgoal)
			sectionValues += db.CastInterfaceString(stats.Shots.Insidebox)
			sectionValues += db.CastInterfaceString(stats.Shots.Outsidebox)
			sectionValues += db.CastInterfaceString(stats.Passes.Total)
			sectionValues += db.CastInterfaceString(stats.Passes.Accurate)
			sectionValues += db.CastInterfaceString(stats.Passes.Percentage)
			sectionValues += db.CastInterfaceString(stats.Attacks.Attacks)
			sectionValues += db.CastInterfaceString(stats.Attacks.DangerousAttacks)
			sectionValues += db.CastInterfaceString(stats.Fouls)
			sectionValues += db.CastInterfaceString(stats.Corners)
			sectionValues += db.CastInterfaceString(stats.Offsides)
			sectionValues += db.CastInterfaceString(stats.Possessiontime)
			sectionValues += db.CastInterfaceString(stats.Yellowcards)
			sectionValues += db.CastInterfaceString(stats.Redcards)
			sectionValues += db.CastInterfaceString(stats.Yellowredcards)
			sectionValues += db.CastInterfaceString(stats.Saves)
			sectionValues += db.CastInterfaceString(stats.Substitutions)
			sectionValues += db.CastInterfaceString(stats.GoalKick)
			sectionValues += db.CastInterfaceString(stats.GoalAttempts)
			sectionValues += db.CastInterfaceString(stats.FreeKick)
			sectionValues += db.CastInterfaceString(stats.ThrowIn)
			sectionValues += db.CastInterfaceString(stats.BallSafe)
			sectionValues += db.CastInterfaceString(stats.Goals)
			sectionValues += db.CastInterfaceString(stats.Penalties)
			sectionValues += db.CastInterfaceString(stats.Injuries)
			sectionValues += strings.Trim(db.CastInterfaceString(stats.Tackles), ", ") + "), "
		}

		if sectionValues != "" {
			queryInsert := db.SectionInsert["matchcenter_matchstatistic"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_matchstatistic"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersFixturesSeason", err)
			}
		}
	}
}

func HandlersUpsertFixturesStats(nameEndpoint string, fixtures *types.ResponseFixtures) {

	//(team_id,match_id,shots_total,shots_ongoal,shots_blocked,shots_offgoal,shots_insidebox,shots_outsidebox,passes_total,
	//passes_accurate,passes_percentage,attacks,dangerous_attacks,fouls,corners,offsides,possessiontime,yellowcards,redcards,
	//yellowredcards,saves,substitutions,goal_kick,goal_attempts,free_kick,throw_in,ball_safe,goals,penalties,injuries,tackles)
	for _, fixture := range fixtures.Data {
		sectionValues := ""
		for _, stats := range fixture.Stats.Data {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(stats.TeamId)
			sectionValues += db.CastInterfaceString(stats.FixtureId)
			sectionValues += db.CastInterfaceString(stats.Shots.Total)
			sectionValues += db.CastInterfaceString(stats.Shots.Ongoal)
			sectionValues += db.CastInterfaceString(stats.Shots.Blocked)
			sectionValues += db.CastInterfaceString(stats.Shots.Offgoal)
			sectionValues += db.CastInterfaceString(stats.Shots.Insidebox)
			sectionValues += db.CastInterfaceString(stats.Shots.Outsidebox)
			sectionValues += db.CastInterfaceString(stats.Passes.Total)
			sectionValues += db.CastInterfaceString(stats.Passes.Accurate)
			sectionValues += db.CastInterfaceString(stats.Passes.Percentage)
			sectionValues += db.CastInterfaceString(stats.Attacks.Attacks)
			sectionValues += db.CastInterfaceString(stats.Attacks.DangerousAttacks)
			sectionValues += db.CastInterfaceString(stats.Fouls)
			sectionValues += db.CastInterfaceString(stats.Corners)
			sectionValues += db.CastInterfaceString(stats.Offsides)
			sectionValues += db.CastInterfaceString(stats.Possessiontime)
			sectionValues += db.CastInterfaceString(stats.Yellowcards)
			sectionValues += db.CastInterfaceString(stats.Redcards)
			sectionValues += db.CastInterfaceString(stats.Yellowredcards)
			sectionValues += db.CastInterfaceString(stats.Saves)
			sectionValues += db.CastInterfaceString(stats.Substitutions)
			sectionValues += db.CastInterfaceString(stats.GoalKick)
			sectionValues += db.CastInterfaceString(stats.GoalAttempts)
			sectionValues += db.CastInterfaceString(stats.FreeKick)
			sectionValues += db.CastInterfaceString(stats.ThrowIn)
			sectionValues += db.CastInterfaceString(stats.BallSafe)
			sectionValues += db.CastInterfaceString(stats.Goals)
			sectionValues += db.CastInterfaceString(stats.Penalties)
			sectionValues += db.CastInterfaceString(stats.Injuries)
			sectionValues += strings.Trim(db.CastInterfaceString(stats.Tackles), ", ") + "), "
		}

		if sectionValues != "" {
			queryInsert := db.SectionInsert["matchcenter_matchstatistic"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_matchstatistic"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertFixturesStats", err)
			}
		}
	}
}

func GetWinner(tScore string) string {
	fts := strings.Split(tScore, "-")
	hS, _ := strconv.Atoi(fts[0])
	aS, _ := strconv.Atoi(fts[1])

	// Определяем как завершился матч  Full Time Result (H=Home Win, D=Draw, A=Away Win)
	if hS > aS {
		return " 'H', "
	} else if aS > hS {
		return " 'A', "
	} else {
		return " 'D', "
	}
}
