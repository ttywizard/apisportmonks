package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"reflect"
	"strconv"
	"strings"
)

//HandlersUpsertWeatherSeasons - обновляет поголдные данные на матчи сезона
func HandlersUpsertWeatherSeasons(nameEndpoint string, season *types.ResponseSeason) {
	//match_id, code, types, icon, temp_celcius, temp_fahrenheit, clouds, humidity, pressure, wind_speed, wind_degree, lat, lon
	sectionValues := ""
	for _, fixture := range season.Data.Fixtures.Data {
		if !reflect.ValueOf(fixture.WeatherReport.Code).IsNil() {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.Id)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Code)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Type)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Icon)

			if !reflect.ValueOf(fixture.WeatherReport.TemperatureCelcius.Temp).IsNil() {
				sectionValues += db.CastInterfaceString(fixture.WeatherReport.TemperatureCelcius.Temp)
			} else {
				if !reflect.ValueOf(fixture.WeatherReport.Temperature.Temp).IsNil() {
					sectionValues += strconv.FormatFloat((*fixture.WeatherReport.Temperature.Temp-32.0)/1.8, 'f', 2, 64) + ", "
				} else {
					sectionValues += "NULL, "
				}
			}

			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Temperature.Temp)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Clouds)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Humidity)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Pressure)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Wind.Speed)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Wind.Degree)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Coordinates.Lat)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.WeatherReport.Coordinates.Lon), ", ") + "), "
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_weather"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_weather"]
		//fmt.Println(queryInsert)
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertWeatherSeasons", err)
		}
	}
}

//HandlersUpsertWeather - обновляет поголдные данные на список событий(матчей)
func HandlersUpsertWeather(nameEndpoint string, fixtures *types.ResponseFixtures) {
	//match_id, code, types, icon, temp_celcius, temp_fahrenheit, clouds, humidity, pressure, wind_speed, wind_degree, lat, lon
	sectionValues := ""
	for _, fixture := range fixtures.Data {
		if !reflect.ValueOf(fixture.WeatherReport.Code).IsNil() {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(fixture.Id)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Code)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Type)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Icon)

			if !reflect.ValueOf(fixture.WeatherReport.TemperatureCelcius.Temp).IsNil() {
				sectionValues += db.CastInterfaceString(fixture.WeatherReport.TemperatureCelcius.Temp)
			} else {
				if !reflect.ValueOf(fixture.WeatherReport.Temperature.Temp).IsNil() {
					sectionValues += strconv.FormatFloat((*fixture.WeatherReport.Temperature.Temp-32.0)/1.8, 'f', 2, 64) + ", "
				} else {
					sectionValues += "NULL, "
				}
			}

			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Temperature.Temp)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Clouds)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Humidity)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Pressure)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Wind.Speed)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Wind.Degree)
			sectionValues += db.CastInterfaceString(fixture.WeatherReport.Coordinates.Lat)
			sectionValues += strings.Trim(db.CastInterfaceString(fixture.WeatherReport.Coordinates.Lon), ", ") + "), "
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_weather"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_weather"]
		//fmt.Println(queryInsert)
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertWeather", err)
		}
	}
}
