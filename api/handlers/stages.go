package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

//UpsertStagesOfSeasons добавляем или обновляем раунды сезона
func UpsertStagesOfSeasons(nameEndpoint string, stages *types.ResponseStages) {

	sectionValues := ""
	for _, stage := range stages.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(stage.Id)
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(stage.Name)
		sectionValues += db.CastInterfaceString(stage.Type)
		sectionValues += db.CastInterfaceString(stage.LeagueId)
		sectionValues += db.CastInterfaceString(stage.SeasonId)
		sectionValues += db.CastInterfaceString(stage.SortOrder)
		sectionValues += strings.Trim(db.CastInterfaceString(stage.HasStandings), ", ") + "), "
	}

	queryInsert := db.SectionInsert["matchcenter_stages"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_stages"]
	//Выполняем запрос
	_, err := db.Db.Exec(queryInsert)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("UpsertStagesOfSeasons", err)
	}
}
