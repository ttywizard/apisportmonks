package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strconv"
	"strings"
)

func HandlersUpsertEventsSeason(nameEndpoint string, season *types.ResponseSeason) {

	sectionValues := ""
	for _, fixture := range season.Data.Fixtures.Data {
		for _, events := range fixture.Events.Data {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(events.Id)
			sectionValues += db.CastInterfaceString(db.Prov.Id)
			sectionValues += db.CastInterfaceString(events.FixtureId)
			sectionValues += db.CastInterfaceString(events.PlayerId)
			TeamId, _ := strconv.Atoi(*events.TeamId)
			sectionValues += db.CastInterfaceString(TeamId)
			sectionValues += db.CastInterfaceString(queries.ListEventsType[*events.Type].UUID)
			sectionValues += db.CastInterfaceString(queries.ListEventsType[*events.Type].Name)
			sectionValues += db.CastInterfaceString(events.Reason)
			sectionValues += db.CastInterfaceString(events.Injuried)
			sectionValues += db.CastInterfaceString(events.RelatedPlayerId)
			sectionValues += db.CastInterfaceString(events.Minute)
			sectionValues += db.CastInterfaceString(events.ExtraMinute)
			sectionValues += db.CastInterfaceString(events.VarResult)
			sectionValues += db.CastInterfaceString(events.Result)
			sectionValues += strings.Trim(db.CastInterfaceString(events.OnPitch), ", ") + "), "
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_event"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_event"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertEventsSeason", err)
		}
	}
}

//HandlersUpsertEvents обновляет данные fixture событий (Livescore, Between Date)
func HandlersUpsertEvents(nameEndpoint string, fixtures *types.ResponseFixtures) {

	sectionValues := ""
	for _, fixture := range fixtures.Data {
		for _, events := range fixture.Events.Data {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(events.Id)
			sectionValues += db.CastInterfaceString(db.Prov.Id)
			sectionValues += db.CastInterfaceString(events.FixtureId)
			sectionValues += db.CastInterfaceString(events.PlayerId)
			TeamId, _ := strconv.Atoi(*events.TeamId)
			sectionValues += db.CastInterfaceString(TeamId)
			sectionValues += db.CastInterfaceString(queries.ListEventsType[*events.Type].UUID)
			sectionValues += db.CastInterfaceString(queries.ListEventsType[*events.Type].Name)
			sectionValues += db.CastInterfaceString(events.Reason)
			sectionValues += db.CastInterfaceString(events.Injuried)
			sectionValues += db.CastInterfaceString(events.RelatedPlayerId)
			sectionValues += db.CastInterfaceString(events.Minute)
			sectionValues += db.CastInterfaceString(events.ExtraMinute)
			sectionValues += db.CastInterfaceString(events.VarResult)
			sectionValues += db.CastInterfaceString(events.Result)
			sectionValues += strings.Trim(db.CastInterfaceString(events.OnPitch), ", ") + "), "
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_event"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_event"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertEvents", err)
		}
	}
}
