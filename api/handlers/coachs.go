package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"reflect"
	"strings"
	"time"
)

func (nameEndpoint string, teams *types.ResponseTeams) {

	arrValues := make(map[int]string)
	sectionValues := ""
	for _, team := range teams.Data {
		sectionValues = "("
		sectionValues += db.CastInterfaceString(team.Coach.Data.CoachId)
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(team.Coach.Data.CommonName)
		sectionValues += db.CastInterfaceString(team.Coach.Data.Fullname)
		sectionValues += db.CastInterfaceString(team.Coach.Data.Firstname)
		sectionValues += db.CastInterfaceString(team.Coach.Data.Lastname)
		sectionValues += db.CastInterfaceString(team.Coach.Data.Nationality)

		var t time.Time
		if reflect.ValueOf(team.Coach.Data.Birthdate).IsNil() {
			sectionValues += "NULL, "
		} else {
			t, _ = time.Parse("02/01/2006", *team.Coach.Data.Birthdate)
			sectionValues += db.CastInterfaceString(t.Format("2006-01-02"))
		}

		sectionValues += db.CastInterfaceString(queries.MapCountryProviderIdToUuid[team.Coach.Data.CountryId])
		sectionValues += db.CastInterfaceString(team.Coach.Data.Birthplace)
		sectionValues += strings.Trim(db.CastInterfaceString(team.Coach.Data.ImagePath), ", ") + "), "

		arrValues[team.Coach.Data.CoachId] = sectionValues
	}

	if len(arrValues) > 0 {
		sectionValues = ""
		for _, v := range arrValues {
			sectionValues += v
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_coach"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_coach"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("", err)
		}
	}
}
