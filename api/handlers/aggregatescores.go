package handler

import (
	"github.com/lib/pq"
	"github.com/valyala/fastjson"
	"gitlab.com/ttywizard/apisportmonks/db"
	"strings"
)

func HandlersAggregateScore(nameEndpoint string, topScoresData *fastjson.Value) {

	leagueId := topScoresData.GetInt("league_id")

	// Бомбандиры
	var sectionValues string
	for _, goalScores := range topScoresData.GetArray("data", "aggregatedGoalscorers", "data") {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(leagueId)
		sectionValues += db.CastInterfaceString(goalScores.GetInt("season_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("team_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("player_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("goals"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("penalty_goals"))
		sectionValues += "'AG'),"
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_aggregate_score_ag"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_standings_stage_ag"]

		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersAggregateScore", err)
		}
	}

	sectionValues = ""
	for _, goalScores := range topScoresData.GetArray("data", "aggregatedAssistscorers", "data") {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(leagueId)
		sectionValues += db.CastInterfaceString(goalScores.GetInt("season_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("team_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("player_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("assists"))
		sectionValues += "'AA'),"
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_aggregate_score_aa"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_standings_stage_aa"]

		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersAggregateScore", err)
		}
	}

	sectionValues = ""
	for _, goalScores := range topScoresData.GetArray("data", "aggregatedAssistscorers", "data") {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(leagueId)
		sectionValues += db.CastInterfaceString(goalScores.GetInt("season_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("team_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("player_id"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("yellowcards"))
		sectionValues += db.CastInterfaceString(goalScores.GetInt("redcards"))
		sectionValues += "'AC'),"
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_aggregate_score_ac"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_standings_stage_ac"]

		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersAggregateScore", err)
		}
	}
}

// HandlersUpsertTopGoals - обработка данных о бомбандирах
func HandlersUpsertTopGoals(nameEndpoint string) {

	query := `INSERT INTO matchcenter_aggregate_score(league_id,season_id, team_id,player_id,goals,penalty_goals,games_played,minutes_played,type_score)
SELECT mc_l.id AS league_id,
       mc_s.id AS season_id,
       mc_ev.team_id,
       mc_ev.player_id,
       COUNT(type_id) AS goals,
       MAX(COALESCE(pen_scored,0)) AS penalty_goals,
       MAX(COALESCE(games_played, 0)) AS games_played,
       MAX(COALESCE(minutes_played, 0)) AS minutes_played,
       'AG' AS type_score
FROM  matchcenter_league AS mc_l LEFT JOIN matchcenter_season mc_s ON (mc_l.uid = mc_s.league_uid AND mc_s.current)
      LEFT JOIN matchcenter_match AS mc_m ON (mc_s.id = mc_m.season_id AND mc_l.id = mc_m.league_id)
      LEFT JOIN matchcenter_event AS mc_ev ON (mc_ev.match_id = mc_m.id)
      LEFT JOIN LATERAL (
          SELECT SUM(COALESCE(minutes_played, 0)) AS minutes_played,
                 COUNT(COALESCE(t.match_id, 0)) AS games_played,
                 SUM(COALESCE(pen_scored, 0)) AS pen_scored,
                 t.player_id,
                 t.season_id
          FROM  matchcenter_player_statistics_match AS t
          WHERE  t.player_id = mc_ev.player_id AND t.season_id = mc_s.id
          GROUP BY t.player_id, t.season_id
    ) AS mc_pl_st_match ON (mc_ev.player_id = mc_pl_st_match.player_id)
WHERE mc_s.current
  AND mc_ev.type_id IN ('b2516c44-23e4-4ed2-9301-b7a71a417500', '49762df2-1db1-4999-84c3-343cfd9603b8')
  AND mc_ev.player_id NOTNULL
GROUP BY mc_l.id, mc_s.id, mc_ev.player_id, mc_ev.team_id
HAVING COUNT(type_id) > 0
ON CONFLICT (league_id, season_id, team_id, player_id, type_score)  DO UPDATE SET updated_at = NOW(),
                                                                                  games_played = EXCLUDED.games_played,
                                                                                  minutes_played = EXCLUDED.minutes_played,
                                                                                  goals         = EXCLUDED.goals,
                                                                                  penalty_goals = EXCLUDED.penalty_goals`
	//Выполняем запрос
	_, err := db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlersUpsertTopGoals", err)
	}
}

// HandlersUpsertTopAssist - обработка данных о ассистентах
func HandlersUpsertTopAssist(nameEndpoint string) {
	query := `INSERT INTO matchcenter_aggregate_score(league_id,season_id, team_id,player_id,assists,minutes_played,games_played,type_score)
SELECT t.league_id,
       t.season_id,
       t.team_id,
       t.player_id,
       SUM(goals_assists)  AS assists,
       SUM(COALESCE(minutes_played, 0)) AS minutes_played,
       COUNT(COALESCE(t.match_id, 0)) AS games_played,
       'AA' AS type_score
FROM  matchcenter_player_statistics_match AS t
GROUP BY t.player_id, t.season_id, t.league_id, t.team_id
HAVING SUM(goals_assists) > 0
ON CONFLICT (league_id, season_id, team_id, player_id, type_score)  DO UPDATE SET updated_at = NOW(),
                                                                                  games_played = EXCLUDED.games_played, 
                                                                                  minutes_played = EXCLUDED.minutes_played,
                                                                                  assists       = EXCLUDED.assists`
	//Выполняем запрос
	_, err := db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlersUpsertTopAssist", err)
	}
}

// HandlersUpsertTopCards - обработка данных о нарушителях(предупреждения, удаления)
func HandlersUpsertTopCards(nameEndpoint string) {
	query := `INSERT INTO matchcenter_aggregate_score(league_id,season_id, team_id,player_id,yellowcards, redcards,yellowredcards, minutes_played,games_played, type_score, points)
SELECT league_id,
       season_id,
       team_id,
       player_id,
       COALESCE(yellowcards, 0),
       COALESCE(redcards, 0),
       COALESCE(yellowredcards, 0),
       COALESCE(minutes_played, 0),
       COALESCE(games_played, 0),
       type_score,
       COALESCE((yellowcards * 1) + (yellowredcards * 2) + (redcards * 3), 0) AS points
FROM (
         SELECT t.league_id,
                t.season_id,
                t.team_id,
                t.player_id,
                SUM(yellowcards)                 AS yellowcards,
                SUM(redcards)                    AS redcards,
                SUM(yellowredcards)              AS yellowredcards,
                SUM(COALESCE(minutes_played, 0)) AS minutes_played,
                COUNT(COALESCE(t.match_id, 0))   AS games_played,
                'AC'                             AS type_score
         FROM matchcenter_player_statistics_match AS t
         GROUP BY t.player_id, t.season_id, t.league_id, t.team_id
     ) AS itog
WHERE yellowcards > 0 OR yellowredcards > 0 OR redcards > 0
ON CONFLICT (league_id, season_id, team_id, player_id, type_score)  DO UPDATE SET updated_at = NOW(),
                                                                                  games_played = EXCLUDED.games_played, 
                                                                                  minutes_played = EXCLUDED.minutes_played,
                                                                                  yellowcards	  = EXCLUDED.yellowcards,
                                                                                  redcards	  = EXCLUDED.redcards`
	//Выполняем запрос
	_, err := db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlersUpsertTopCards", err)
	}
}

// HandlersUpsertTopPenalty - обработка данных о пенальтистах
func HandlersUpsertTopPenalty(nameEndpoint string) {
	query := `INSERT INTO matchcenter_aggregate_score(league_id,season_id, team_id,player_id, penalty, pen_missed , minutes_played,games_played, type_score)
SELECT t.league_id,
       t.season_id,
       t.team_id,
       t.player_id,
       SUM(COALESCE(pen_scored, 0)) + SUM(COALESCE(pen_missed, 0)) AS penalty,
       SUM(COALESCE(pen_missed, 0))     AS pen_missed,
       SUM(COALESCE(minutes_played, 0)) AS minutes_played,
       COUNT(COALESCE(t.match_id, 0))   AS games_played,
       'AP'                             AS type_score
FROM matchcenter_player_statistics_match AS t
GROUP BY t.player_id, t.season_id, t.league_id, t.team_id
HAVING  SUM(pen_scored) > 0 OR SUM(pen_missed) > 0
ON CONFLICT (league_id, season_id, team_id, player_id, type_score)  DO UPDATE SET updated_at = NOW(),
                                                                                  games_played = EXCLUDED.games_played, 
                                                                                  minutes_played = EXCLUDED.minutes_played,
                                                                                  penalty = EXCLUDED.penalty,
                                                                                  pen_missed = EXCLUDED.pen_missed`
	//Выполняем запрос
	_, err := db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlersUpsertTopPenalty", err)
	}
}

// HandlersUpsertTopGoalkeeper - обработка данных о голкиперах
func HandlersUpsertTopGoalkeeper(nameEndpoint string) {
	query := `INSERT INTO matchcenter_aggregate_score(league_id,season_id, team_id,player_id, cleansheets, goals, minutes_played,games_played, type_score)
SELECT t.league_id,
       t.season_id,
       t.team_id,
       t.player_id,
       COUNT(COALESCE(goals_conceded, 0)) FILTER ( WHERE COALESCE(goals_conceded,0) = 0) AS cleansheets,
       SUM(COALESCE(goals_conceded, 0)) AS goals_conceded,
       SUM(COALESCE(minutes_played, 0)) AS minutes_played,
       COUNT(COALESCE(t.match_id, 0))   AS games_played,
       'AGK'                            AS type_score
FROM matchcenter_player_statistics_match AS t LEFT JOIN matchcenter_lineups AS mc_li ON (t.match_id = mc_li.match_id AND t.player_id = mc_li.player_id)
WHERE mc_li.position = 'G'
GROUP BY t.player_id, t.season_id, t.league_id, t.team_id
HAVING  SUM(COALESCE(goals_conceded, 0)) >= 0
ON CONFLICT (league_id, season_id, team_id, player_id, type_score)  DO UPDATE SET updated_at = NOW(),
                                                                                  games_played = EXCLUDED.games_played, 
                                                                                  minutes_played = EXCLUDED.minutes_played,
                                                                                  cleansheets = EXCLUDED.cleansheets,
                                                                                  goals = EXCLUDED.goals`
	//Выполняем запрос
	_, err := db.Db.Exec(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlersUpsertTopGoalkeeper", err)
	}
}
