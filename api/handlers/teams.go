package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func UpsertTeams(nameEndpoint string, teams *types.ResponseTeams) {

	sectionValues := ""
	for _, team := range teams.Data {
		sectionValues += "("

		sectionValues += db.CastInterfaceString(team.Id)
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(team.Name)
		sectionValues += db.CastInterfaceString(team.ShortCode)
		sectionValues += db.CastInterfaceString(team.Founded)
		sectionValues += db.CastInterfaceString(team.VenueId)
		sectionValues += db.CastInterfaceString(team.LogoPath)
		sectionValues += db.CastInterfaceString(team.NationalTeam)
		sectionValues += db.CastInterfaceString(queries.MapCountryProviderIdToUuid[team.CountryId])
		sectionValues += db.CastInterfaceString(team.CurrentSeasonId)
		sectionValues += strings.Trim(db.CastInterfaceString(team.IsPlaceholder), ", ") + "), "
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_team"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_team"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertTeams", err)
		}
	}
}

func UpsertSquadTeamSeason(nameEndpoint string, teams *types.ResponseTeams, legaueId int, seasonId int) {

	sectionValues := ""
	for _, team := range teams.Data {
		for _, squad := range team.Squad.Data {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(squad.PlayerId)
			sectionValues += db.CastInterfaceString(db.Prov.Id)
			sectionValues += db.CastInterfaceString(team.Id)
			sectionValues += db.CastInterfaceString(legaueId)
			sectionValues += db.CastInterfaceString(seasonId)
			sectionValues += db.CastInterfaceString(squad.PositionId)
			sectionValues += db.CastInterfaceString(squad.Number)
			sectionValues += db.CastInterfaceString(squad.Captain)
			sectionValues += db.CastInterfaceString(squad.Injured)
			sectionValues += db.CastInterfaceString(squad.Minutes)
			sectionValues += db.CastInterfaceString(squad.Appearences)
			sectionValues += db.CastInterfaceString(squad.Lineups)
			sectionValues += db.CastInterfaceString(squad.SubstituteIn)
			sectionValues += db.CastInterfaceString(squad.SubstituteOut)
			sectionValues += db.CastInterfaceString(squad.SubstitutesOnBench)
			sectionValues += db.CastInterfaceString(squad.Goals)
			sectionValues += db.CastInterfaceString(squad.Owngoals)
			sectionValues += db.CastInterfaceString(squad.Assists)
			sectionValues += db.CastInterfaceString(squad.Saves)
			sectionValues += db.CastInterfaceString(squad.InsideBoxSaves)
			sectionValues += db.CastInterfaceString(squad.Dispossesed)
			sectionValues += db.CastInterfaceString(squad.Interceptions)
			sectionValues += db.CastInterfaceString(squad.Yellowcards)
			sectionValues += db.CastInterfaceString(squad.Yellowred)
			sectionValues += db.CastInterfaceString(squad.Redcards)
			sectionValues += db.CastInterfaceString(squad.Tackles)
			sectionValues += db.CastInterfaceString(squad.Blocks)
			sectionValues += db.CastInterfaceString(squad.HitPost)
			sectionValues += db.CastInterfaceString(squad.Cleansheets)
			sectionValues += db.CastInterfaceString(squad.Rating)
			sectionValues += db.CastInterfaceString(squad.Fouls.Committed)
			sectionValues += db.CastInterfaceString(squad.Fouls.Drawn)
			sectionValues += db.CastInterfaceString(squad.Crosses.Total)
			sectionValues += db.CastInterfaceString(squad.Crosses.Accurate)
			sectionValues += db.CastInterfaceString(squad.Dribbles.Attempts)
			sectionValues += db.CastInterfaceString(squad.Dribbles.Success)
			sectionValues += db.CastInterfaceString(squad.Dribbles.DribbledPast)
			sectionValues += db.CastInterfaceString(squad.Duels.Total)
			sectionValues += db.CastInterfaceString(squad.Duels.Won)
			sectionValues += db.CastInterfaceString(squad.Passes.Total)
			sectionValues += db.CastInterfaceString(squad.Passes.Accuracy)
			sectionValues += db.CastInterfaceString(squad.Passes.KeyPasses)
			sectionValues += db.CastInterfaceString(squad.Penalties.Won)
			sectionValues += db.CastInterfaceString(squad.Penalties.Scores)
			sectionValues += db.CastInterfaceString(squad.Penalties.Missed)
			sectionValues += db.CastInterfaceString(squad.Penalties.Committed)
			sectionValues += db.CastInterfaceString(squad.Penalties.Saves)
			sectionValues += db.CastInterfaceString(squad.Shots.ShotsTotal)
			sectionValues += db.CastInterfaceString(squad.Shots.ShotsOnTarget)
			sectionValues += strings.Trim(db.CastInterfaceString(squad.Shots.ShotsOffTarget), ", ") + "), "
		}
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_squad"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_squad"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertSquadTeamSeason", err)
		}
	}
}

func UpsertTeamsSeason(nameEndpoint string, teams *types.ResponseTeams, legaueId int, seasonId int, seasons *map[int]db.SeasonData) {

	sectionValues := ""
	for _, team := range teams.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString((*seasons)[seasonId].LeagueUid)
		sectionValues += db.CastInterfaceString((*seasons)[seasonId].SeasonUid)
		sectionValues += db.CastInterfaceString(team.ShortCode)
		sectionValues += strings.Trim(db.CastInterfaceString(team.Id), ", ") + "), "
	}

	if sectionValues != "" {
		queryInsert := db.SectionInsert["matchcenter_teams_season"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_teams_season"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertTeamsSeason", err)
		}
	}
}
