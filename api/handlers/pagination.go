package handler

import "gitlab.com/ttywizard/apisportmonks/types"

type PaginationData struct {
	Total   *int
	Next    *int
	Current *int
}

func (pd *PaginationData) Init() {
	pd.Total = new(int)
	pd.Next = new(int)
	pd.Current = new(int)
}

// PaginationHandler Функция обрабатывает ответ провайдера
func PaginationHandler(p *types.Pagination, pgData *PaginationData) {
	pgData.Init()
	*pgData.Total = *p.TotalPages
	*pgData.Current = *p.CurrentPage
	*pgData.Next = *pgData.Current + 1
}
