package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func UpsertProviderCountry(countries *types.ResponseCountries) {

	sectionValues := ""
	for _, country := range countries.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += db.CastInterfaceString(country.Id)
		sectionValues += db.CastInterfaceString(country.Name)
		sectionValues += db.CastInterfaceString(country.ImagePath)
		sectionValues += db.CastInterfaceString(country.Extra.SubRegion)
		sectionValues += db.CastInterfaceString(country.Extra.WorldRegion)
		sectionValues += db.CastInterfaceString(country.Extra.Fifa.String)
		sectionValues += db.CastInterfaceString(country.Extra.Iso)
		sectionValues += db.CastInterfaceString(country.Extra.Iso2)
		sectionValues += db.CastInterfaceString(country.Extra.Longitude)
		sectionValues += db.CastInterfaceString(country.Extra.Latitude)
		sectionValues += strings.Trim(db.CastInterfaceString(country.Extra.Flag), ", ") + "), "
	}

	queryInsert := db.SectionInsert["matchcenter_providers_country"] + strings.Trim(sectionValues, ", ")

	//Выполняем запрос
	_, err := db.Db.Exec(queryInsert)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("UpsertProviderCountry", err)
	}
}
