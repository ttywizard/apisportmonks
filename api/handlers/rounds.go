package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

// HandlersUpsertRound добавляем или обновляем раунды сезона
func HandlersUpsertRound(nameEndpoint string, round []types.Round) {
	var stats db.Statistics
	seasons := queries.GetSeasons(stats)
	var seasonUid string
	var leagueUid string

	if len(round) > 0 {

		for _, v := range seasons[round[0].LeagueId] {
			if v.SeasonId == round[0].SeasonId {
				seasonUid = v.SeasonUid
				leagueUid = v.LeagueUid
				break
			}
		}

		sectionValues := ""
		for _, roundId := range round {
			sectionValues += "("
			sectionValues += db.CastInterfaceString(roundId.Id)
			sectionValues += db.CastInterfaceString(roundId.Name)
			sectionValues += db.CastInterfaceString(roundId.LeagueId)
			sectionValues += db.CastInterfaceString(leagueUid)
			sectionValues += db.CastInterfaceString(roundId.SeasonId)
			sectionValues += db.CastInterfaceString(seasonUid)
			sectionValues += db.CastInterfaceString(roundId.StageId)
			sectionValues += db.CastInterfaceString(roundId.Start)
			sectionValues += strings.Trim(db.CastInterfaceString(roundId.End), ", ") + "), "
		}

		queryInsert := db.SectionInsert["matchcenter_rounds"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_rounds"]
		//Выполняем запрос
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("HandlersUpsertRound", err)
		}
	}
}
