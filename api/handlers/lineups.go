package handler

import (
	"github.com/OneOfOne/xxhash"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strconv"
	"strings"
)

func HandlersUpsertLineupsSeason(nameEndpoint string, season *types.ResponseSeason) {

	for _, fixture := range season.Data.Fixtures.Data {

		arrValues := make(map[uint64]string)
		sectionValues := ""
		//Запас
		for _, bench := range fixture.Bench.Data {

			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(bench.FixtureId))
			StrLineups.WriteString(strconv.Itoa(bench.TeamId))
			StrLineups.WriteString(strconv.Itoa(bench.PlayerId))

			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(bench.FixtureId)
			sectionValues += db.CastInterfaceString(bench.PlayerId)
			sectionValues += db.CastInterfaceString(bench.PlayerName)
			sectionValues += db.CastInterfaceString(bench.TeamId)
			sectionValues += db.CastInterfaceString(bench.Number)
			sectionValues += db.CastInterfaceString(bench.Position)
			sectionValues += db.CastInterfaceString(bench.AdditionalPosition)
			sectionValues += db.CastInterfaceString(bench.FormationPosition)
			sectionValues += db.CastInterfaceString(bench.Captain)
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += strings.Trim(db.CastInterfaceString(1), ", ") + "), "

			arrValues[xxh] = sectionValues
		}

		//Основной состав
		for _, lineups := range fixture.Lineup.Data {
			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(lineups.FixtureId))
			StrLineups.WriteString(strconv.Itoa(lineups.TeamId))
			StrLineups.WriteString(strconv.Itoa(lineups.PlayerId))

			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(lineups.FixtureId)
			sectionValues += db.CastInterfaceString(lineups.PlayerId)
			sectionValues += db.CastInterfaceString(lineups.PlayerName)
			sectionValues += db.CastInterfaceString(lineups.TeamId)
			sectionValues += db.CastInterfaceString(lineups.Number)
			sectionValues += db.CastInterfaceString(lineups.Position)
			sectionValues += db.CastInterfaceString(lineups.AdditionalPosition)
			sectionValues += db.CastInterfaceString(lineups.FormationPosition)
			sectionValues += db.CastInterfaceString(lineups.Captain)
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += strings.Trim(db.CastInterfaceString(0), ", ") + "), "

			arrValues[xxh] = sectionValues
		}

		sectionValuesItog := ""

		if len(arrValues) > 0 {
			for _, v := range arrValues {
				sectionValuesItog += v
			}
		}

		if sectionValuesItog != "" {
			queryInsert := db.SectionInsert["matchcenter_lineups"] + strings.Trim(sectionValuesItog, ", ") + db.SectionConflict["matchcenter_lineups"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertLineupsSeason", err)
			}
		}
	}
}

//HandlersUpsertLineups обновляет данные fixture событий (Livescore, Between Date)
func HandlersUpsertLineups(nameEndpoint string, fixtures *types.ResponseFixtures) {

	for _, fixture := range fixtures.Data {

		arrValues := make(map[uint64]string)
		sectionValues := ""
		//Запас
		for _, bench := range fixture.Bench.Data {

			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(bench.FixtureId))
			StrLineups.WriteString(strconv.Itoa(bench.TeamId))
			StrLineups.WriteString(strconv.Itoa(bench.PlayerId))

			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(bench.FixtureId)
			sectionValues += db.CastInterfaceString(bench.PlayerId)
			sectionValues += db.CastInterfaceString(bench.PlayerName)
			sectionValues += db.CastInterfaceString(bench.TeamId)
			sectionValues += db.CastInterfaceString(bench.Number)
			sectionValues += db.CastInterfaceString(bench.Position)
			sectionValues += db.CastInterfaceString(bench.AdditionalPosition)
			sectionValues += db.CastInterfaceString(bench.FormationPosition)
			sectionValues += db.CastInterfaceString(bench.Captain)
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += strings.Trim(db.CastInterfaceString(1), ", ") + "), "

			arrValues[xxh] = sectionValues
		}

		//Основной состав
		for _, lineups := range fixture.Lineup.Data {
			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(lineups.FixtureId))
			StrLineups.WriteString(strconv.Itoa(lineups.TeamId))
			StrLineups.WriteString(strconv.Itoa(lineups.PlayerId))

			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(lineups.FixtureId)
			sectionValues += db.CastInterfaceString(lineups.PlayerId)
			sectionValues += db.CastInterfaceString(lineups.PlayerName)
			sectionValues += db.CastInterfaceString(lineups.TeamId)
			sectionValues += db.CastInterfaceString(lineups.Number)
			sectionValues += db.CastInterfaceString(lineups.Position)
			sectionValues += db.CastInterfaceString(lineups.AdditionalPosition)
			sectionValues += db.CastInterfaceString(lineups.FormationPosition)
			sectionValues += db.CastInterfaceString(lineups.Captain)
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += strings.Trim(db.CastInterfaceString(0), ", ") + "), "

			arrValues[xxh] = sectionValues
		}

		sectionValuesItog := ""

		if len(arrValues) > 0 {
			for _, v := range arrValues {
				sectionValuesItog += v
			}
		}

		if sectionValuesItog != "" {
			queryInsert := db.SectionInsert["matchcenter_lineups"] + strings.Trim(sectionValuesItog, ", ") + db.SectionConflict["matchcenter_lineups"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertLineups", err)
			}
		}
	}
}
