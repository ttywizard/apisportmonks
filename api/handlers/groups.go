package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func UpsertGroupsBySeason(nameEndpoint string, seasons *types.ResponseSeason) {

	sectionValues := ""
	for _, group := range seasons.Data.Groups.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(db.Prov.Id)
		sectionValues += strings.Trim(db.CastInterfaceString(group.Name), ", ") + "), "
	}
	queryInsert := db.SectionInsert["matchcenter_groups_name"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_groups_name"]
	//Выполняем запрос
	if sectionValues != "" {
		_, err := db.Db.Exec(queryInsert)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("UpsertGroupsBySeason", err)
		}
	}
}
