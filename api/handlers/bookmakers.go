package handler

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"strings"
)

func HandlerBookmakers(bookmakers *types.ResponseBookmaker) {

	sectionValues := ""
	for _, bk := range bookmakers.Data {
		sectionValues += "("
		sectionValues += db.CastInterfaceString(bk.Id)
		sectionValues += db.CastInterfaceString(bk.Name)
		sectionValues += strings.Trim(db.CastInterfaceString(bk.Logo), ", ") + "), "
	}

	queryInsert := db.SectionInsert["matchcenter_bookmaker"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_bookmaker"]

	//Выполняем запрос
	_, err := db.Db.Exec(queryInsert)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("HandlerBookmakers", err)
	}
}
