package handler

import (
	"github.com/OneOfOne/xxhash"
	logger2 "github.com/Pastir/logger"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/types"
	"go.uber.org/zap"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func HandlersUpsertPlayerSquadTeams(nameEndpoint string, teams *types.ResponseTeams) {

	//sectionValues := ""
	for _, team := range teams.Data {
		for _, squad := range team.Squad.Data {
			sectionValues := "("
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.PlayerId)
			sectionValues += db.CastInterfaceString(db.Prov.Id)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Firstname)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Lastname)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.CommonName)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.DisplayName)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Fullname)

			var t time.Time
			if reflect.ValueOf(squad.PlayerData.Data.Birthdate).IsNil() {
				sectionValues += "NULL, "
			} else {
				t, _ = time.Parse("02/01/2006", *squad.PlayerData.Data.Birthdate)
				sectionValues += db.CastInterfaceString(t.Format("2006-01-02"))
			}

			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Nationality)
			if reflect.ValueOf(squad.PlayerData.Data.CountryId).IsNil() {
				sectionValues += "NULL ,"
			} else if _, ok := queries.MapCountryProviderIdToUuid[*squad.PlayerData.Data.CountryId]; ok {
				sectionValues += db.CastInterfaceString(queries.MapCountryProviderIdToUuid[*squad.PlayerData.Data.CountryId])
			} else {
				logger2.ErrorLog.Error("Error: ",
					zap.String("endpoint", nameEndpoint),
					zap.String("func", "HandlersUpsertPlayerSquadTeams"),
					zap.Int("playerId", squad.PlayerData.Data.PlayerId),
				)
			}
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Birthplace)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Height)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.Weight)
			sectionValues += db.CastInterfaceString(squad.PlayerData.Data.ImagePath)
			if reflect.ValueOf(squad.PlayerData.Data.CountryId).IsNil() {
				sectionValues += strings.Trim("NULL, ", ", ") + "), "
			} else if _, ok := queries.MapCountryProviderIdToUuid[*squad.PlayerData.Data.CountryId]; ok {
				sectionValues += strings.Trim(db.CastInterfaceString(queries.MapCountryProviderIdToUuid[*squad.PlayerData.Data.CountryId]), ", ") + "), "
			} else {
				logger2.ErrorLog.Error("Error: ",
					zap.String("endpoint", nameEndpoint),
					zap.String("func", "HandlersUpsertPlayerSquadTeams"),
					zap.Int("playerId", squad.PlayerData.Data.PlayerId),
				)
			}

			if sectionValues != "" {
				queryInsert := db.SectionInsert["matchcenter_player"] + strings.Trim(sectionValues, ", ") + db.SectionConflict["matchcenter_player"]
				//Выполняем запрос
				_, err := db.Db.Exec(queryInsert)
				if err, ok := err.(*pq.Error); ok {
					db.PqErrorError("HandlersUpsertPlayerSquadTeams", err)
				}
			}
		}
	}
}

//HandlersUpsertStatsPlayersFixturesSeasons - Обновляет данные игроков по каждому матчу в сезоне
func HandlersUpsertStatsPlayersFixturesSeasons(nameEndpoint string, season *types.ResponseSeason) {

	for _, fixture := range season.Data.Fixtures.Data {
		arrValues := make(map[uint64]string)
		sectionValues := ""
		//Запас
		for _, bench := range fixture.Bench.Data {
			// Если игрок выходил на замену
			if reflect.ValueOf(bench.Stats.Other.MinutesPlayed).IsNil() == false {
				var StrLineups strings.Builder
				StrLineups.WriteString(strconv.Itoa(bench.FixtureId))
				StrLineups.WriteString(strconv.Itoa(bench.TeamId))
				StrLineups.WriteString(strconv.Itoa(bench.PlayerId))

				hashLineUps := xxhash.New64()
				hashLineUps.WriteString(StrLineups.String())
				xxh := hashLineUps.Sum64()

				sectionValues = "("
				sectionValues += db.CastInterfaceString(xxh)
				sectionValues += db.CastInterfaceString(bench.Stats.Shots.ShotsTotal)
				sectionValues += db.CastInterfaceString(bench.Stats.Shots.ShotsOnGoal)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Scored)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Assists)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Conceded)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Owngoals)
				sectionValues += db.CastInterfaceString(bench.Stats.Fouls.Drawn)
				sectionValues += db.CastInterfaceString(bench.Stats.Fouls.Committed)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Yellowcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Redcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Yellowredcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.TotalCrosses)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.CrossesAccuracy)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.Passes)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.AccuratePasses)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.PassesAccuracy)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.KeyPasses)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.Attempts)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.Success)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.DribbledPast)
				sectionValues += db.CastInterfaceString(bench.Stats.Duels.Total)
				sectionValues += db.CastInterfaceString(bench.Stats.Duels.Won)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.AerialsWon)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Punches)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Offsides)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Saves)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.InsideBoxSaves)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenScored)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenMissed)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenSaved)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenCommitted)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenWon)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.HitWoodwork)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Tackles)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Blocks)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Interceptions)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Clearances)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Dispossesed)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.MinutesPlayed)
				sectionValues += db.CastInterfaceString(bench.Stats.Rating)
				sectionValues += db.CastInterfaceString(fixture.LeagueId)
				sectionValues += db.CastInterfaceString(fixture.SeasonId)
				sectionValues += db.CastInterfaceString(fixture.Id)
				sectionValues += db.CastInterfaceString(bench.TeamId)
				sectionValues += db.CastInterfaceString(bench.PlayerId)
				sectionValues += strings.Trim(db.CastInterfaceString(bench.PlayerName), ", ") + "), "
				arrValues[xxh] = sectionValues
			}
		}

		//Основной состав
		for _, lineups := range fixture.Lineup.Data {

			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(lineups.FixtureId))
			StrLineups.WriteString(strconv.Itoa(lineups.TeamId))
			StrLineups.WriteString(strconv.Itoa(lineups.PlayerId))
			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += db.CastInterfaceString(lineups.Stats.Shots.ShotsTotal)
			sectionValues += db.CastInterfaceString(lineups.Stats.Shots.ShotsOnGoal)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Scored)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Assists)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Conceded)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Owngoals)
			sectionValues += db.CastInterfaceString(lineups.Stats.Fouls.Drawn)
			sectionValues += db.CastInterfaceString(lineups.Stats.Fouls.Committed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Yellowcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Redcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Yellowredcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.TotalCrosses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.CrossesAccuracy)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.Passes)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.AccuratePasses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.PassesAccuracy)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.KeyPasses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.Attempts)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.Success)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.DribbledPast)
			sectionValues += db.CastInterfaceString(lineups.Stats.Duels.Total)
			sectionValues += db.CastInterfaceString(lineups.Stats.Duels.Won)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.AerialsWon)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Punches)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Offsides)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Saves)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.InsideBoxSaves)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenScored)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenMissed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenSaved)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenCommitted)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenWon)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.HitWoodwork)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Tackles)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Blocks)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Interceptions)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Clearances)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Dispossesed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.MinutesPlayed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Rating)
			sectionValues += db.CastInterfaceString(fixture.LeagueId)
			sectionValues += db.CastInterfaceString(fixture.SeasonId)
			sectionValues += db.CastInterfaceString(fixture.Id)
			sectionValues += db.CastInterfaceString(lineups.TeamId)
			sectionValues += db.CastInterfaceString(lineups.PlayerId)
			sectionValues += strings.Trim(db.CastInterfaceString(lineups.PlayerName), ", ") + "), "
			arrValues[xxh] = sectionValues
		}

		sectionValuesItog := ""

		if len(arrValues) > 0 {
			for _, v := range arrValues {
				sectionValuesItog += v
			}
		}

		if sectionValuesItog != "" {
			queryInsert := db.SectionInsert["matchcenter_player_statistics_match"] + strings.Trim(sectionValuesItog, ", ") + db.SectionConflict["matchcenter_player_statistics_match"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertStatsPlayersFixturesSeasons", err)
			}
		}
	}
}

//HandlersUpsertStatsPlayersFixtures - Обновляет статистику игроков в матче(в матчах)
func HandlersUpsertStatsPlayersFixtures(nameEndpoint string, fixtures *types.ResponseFixtures) {

	for _, fixture := range fixtures.Data {
		arrValues := make(map[uint64]string)
		sectionValues := ""
		//Запас
		for _, bench := range fixture.Bench.Data {
			// Если игрок выходил на замену
			if reflect.ValueOf(bench.Stats.Other.MinutesPlayed).IsNil() == false {
				var StrLineups strings.Builder
				StrLineups.WriteString(strconv.Itoa(bench.FixtureId))
				StrLineups.WriteString(strconv.Itoa(bench.TeamId))
				StrLineups.WriteString(strconv.Itoa(bench.PlayerId))

				hashLineUps := xxhash.New64()
				hashLineUps.WriteString(StrLineups.String())
				xxh := hashLineUps.Sum64()

				sectionValues = "("
				sectionValues += db.CastInterfaceString(xxh)
				sectionValues += db.CastInterfaceString(bench.Stats.Shots.ShotsTotal)
				sectionValues += db.CastInterfaceString(bench.Stats.Shots.ShotsOnGoal)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Scored)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Assists)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Conceded)
				sectionValues += db.CastInterfaceString(bench.Stats.Goals.Owngoals)
				sectionValues += db.CastInterfaceString(bench.Stats.Fouls.Drawn)
				sectionValues += db.CastInterfaceString(bench.Stats.Fouls.Committed)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Yellowcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Redcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Cards.Yellowredcards)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.TotalCrosses)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.CrossesAccuracy)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.Passes)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.AccuratePasses)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.PassesAccuracy)
				sectionValues += db.CastInterfaceString(bench.Stats.Passing.KeyPasses)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.Attempts)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.Success)
				sectionValues += db.CastInterfaceString(bench.Stats.Dribbles.DribbledPast)
				sectionValues += db.CastInterfaceString(bench.Stats.Duels.Total)
				sectionValues += db.CastInterfaceString(bench.Stats.Duels.Won)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.AerialsWon)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Punches)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Offsides)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Saves)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.InsideBoxSaves)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenScored)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenMissed)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenSaved)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenCommitted)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.PenWon)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.HitWoodwork)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Tackles)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Blocks)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Interceptions)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Clearances)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.Dispossesed)
				sectionValues += db.CastInterfaceString(bench.Stats.Other.MinutesPlayed)
				sectionValues += db.CastInterfaceString(bench.Stats.Rating)
				sectionValues += db.CastInterfaceString(fixture.LeagueId)
				sectionValues += db.CastInterfaceString(fixture.SeasonId)
				sectionValues += db.CastInterfaceString(fixture.Id)
				sectionValues += db.CastInterfaceString(bench.TeamId)
				sectionValues += db.CastInterfaceString(bench.PlayerId)
				sectionValues += strings.Trim(db.CastInterfaceString(bench.PlayerName), ", ") + "), "
				arrValues[xxh] = sectionValues
			}
		}

		//Основной состав
		for _, lineups := range fixture.Lineup.Data {

			var StrLineups strings.Builder
			StrLineups.WriteString(strconv.Itoa(lineups.FixtureId))
			StrLineups.WriteString(strconv.Itoa(lineups.TeamId))
			StrLineups.WriteString(strconv.Itoa(lineups.PlayerId))
			hashLineUps := xxhash.New64()
			hashLineUps.WriteString(StrLineups.String())
			xxh := hashLineUps.Sum64()

			sectionValues = "("
			sectionValues += db.CastInterfaceString(xxh)
			sectionValues += db.CastInterfaceString(lineups.Stats.Shots.ShotsTotal)
			sectionValues += db.CastInterfaceString(lineups.Stats.Shots.ShotsOnGoal)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Scored)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Assists)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Conceded)
			sectionValues += db.CastInterfaceString(lineups.Stats.Goals.Owngoals)
			sectionValues += db.CastInterfaceString(lineups.Stats.Fouls.Drawn)
			sectionValues += db.CastInterfaceString(lineups.Stats.Fouls.Committed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Yellowcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Redcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Cards.Yellowredcards)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.TotalCrosses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.CrossesAccuracy)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.Passes)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.AccuratePasses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.PassesAccuracy)
			sectionValues += db.CastInterfaceString(lineups.Stats.Passing.KeyPasses)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.Attempts)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.Success)
			sectionValues += db.CastInterfaceString(lineups.Stats.Dribbles.DribbledPast)
			sectionValues += db.CastInterfaceString(lineups.Stats.Duels.Total)
			sectionValues += db.CastInterfaceString(lineups.Stats.Duels.Won)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.AerialsWon)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Punches)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Offsides)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Saves)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.InsideBoxSaves)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenScored)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenMissed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenSaved)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenCommitted)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.PenWon)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.HitWoodwork)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Tackles)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Blocks)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Interceptions)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Clearances)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.Dispossesed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Other.MinutesPlayed)
			sectionValues += db.CastInterfaceString(lineups.Stats.Rating)
			sectionValues += db.CastInterfaceString(fixture.LeagueId)
			sectionValues += db.CastInterfaceString(fixture.SeasonId)
			sectionValues += db.CastInterfaceString(fixture.Id)
			sectionValues += db.CastInterfaceString(lineups.TeamId)
			sectionValues += db.CastInterfaceString(lineups.PlayerId)
			sectionValues += strings.Trim(db.CastInterfaceString(lineups.PlayerName), ", ") + "), "
			arrValues[xxh] = sectionValues
		}

		sectionValuesItog := ""

		if len(arrValues) > 0 {
			for _, v := range arrValues {
				sectionValuesItog += v
			}
		}

		if sectionValuesItog != "" {
			queryInsert := db.SectionInsert["matchcenter_player_statistics_match"] + strings.Trim(sectionValuesItog, ", ") + db.SectionConflict["matchcenter_player_statistics_match"]
			//Выполняем запрос
			_, err := db.Db.Exec(queryInsert)
			if err, ok := err.(*pq.Error); ok {
				db.PqErrorError("HandlersUpsertStatsPlayersFixtures", err)
			}
		}
	}
}
