package types

type (
	sidelined struct {
		PlayerId    int     `json:"player_id"`
		SeasonId    int     `json:"season_id"`
		TeamId      int     `json:"team_id"`
		Description *string `json:"description"`
		StartDate   *string `json:"start_date"`
		EndDate     *string `json:"end_date"`
	}

	ResponseSidelined struct {
		Data []Team `json:"data"`
	}
)
