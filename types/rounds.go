package types

type (
	Round struct {
		Id       int    `json:"id"`
		Name     int    `json:"name"`
		LeagueId int    `json:"league_id"`
		SeasonId int    `json:"season_id"`
		StageId  int    `json:"stage_id"`
		Start    string `json:"start"`
		End      string `json:"end"`
	}

	ResponseRound struct {
		Data []Round `json:"data"`
		Meta meta    `json:"meta"`
	}
)
