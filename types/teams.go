package types

type (

	// Team  структура содержит данные команды по сезону
	Team struct {
		Id              int    `json:"id"`
		LegacyId        int    `json:"legacy_id"`
		Name            string `json:"name"`
		ShortCode       string `json:"short_code"`
		Twitter         string `json:"twitter"`
		CountryId       int    `json:"country_id"`
		NationalTeam    bool   `json:"national_team"`
		Founded         *int   `json:"founded"`
		LogoPath        string `json:"logo_path"`
		VenueId         int    `json:"venue_id"`
		CurrentSeasonId int    `json:"current_season_id"`
		IsPlaceholder   bool   `json:"is_placeholder"`
		Squad           struct {
			Data []squad `json:"data"`
		} `json:"squad"`

		Transfers struct {
			Data []transfers `json:"data"`
		} `json:"transfers"`

		Sidelined struct {
			Data []sidelined `json:"data"`
		} `json:"sidelined"`

		// Стадион
		Venue struct {
			Data venue `json:"data"`
		} `json:"venue"`

		//Тренер
		Coach coach `json:"coach"`
		// ФИФА рейтинг
		FifaRanking ranking `json:"fifaranking"`
		//УЕФА рейтинг
		UefaRanking ranking `json:"uefaranking"`
	}

	// transfers структура содержит данные по трансферам игроков определенной команды.
	transfers struct {
		Id         int     `json:"id"`
		PlayerId   int     `json:"player_id"`
		FromTeamId int     `json:"from_team_id"`
		ToTeamId   int     `json:"to_team_id"`
		SeasonId   int     `json:"season_id"`
		Transfer   string  `json:"transfer"`
		Type       string  `json:"type"`
		Date       string  `json:"date"`
		Amount     *string `json:"amount"`
	}

	//squad Состав команды
	squad struct {
		PlayerId           *int    `json:"player_id"`
		PositionId         *int    `json:"position_id"`
		Number             *int    `json:"number"`
		Captain            *int    `json:"captain"`
		Injured            bool    `json:"injured"`
		Minutes            *int    `json:"minutes"`
		Appearences        *int    `json:"appearences"`
		Lineups            *int    `json:"lineups"`
		SubstituteIn       *int    `json:"substitute_in"`
		SubstituteOut      *int    `json:"substitute_out"`
		SubstitutesOnBench *int    `json:"substitutes_on_bench"`
		Goals              *int    `json:"goals"`
		Owngoals           *int    `json:"owngoals"`
		Assists            *int    `json:"assists"`
		Saves              *int    `json:"saves"`
		InsideBoxSaves     *int    `json:"inside_box_saves"`
		Dispossesed        *int    `json:"dispossesed"`
		Interceptions      *int    `json:"interceptions"`
		Yellowcards        *int    `json:"yellowcards"`
		Yellowred          *int    `json:"yellowred"`
		Redcards           *int    `json:"redcards"`
		Tackles            *int    `json:"tackles"`
		Blocks             *int    `json:"blocks"`
		HitPost            *int    `json:"hit_post"`
		Cleansheets        *int    `json:"cleansheets"`
		Rating             *string `json:"rating"`

		Fouls struct {
			Drawn     *int `json:"drawn"`
			Committed *int `json:"committed"`
		} `json:"fouls"`

		Crosses struct {
			Total    *int `json:"total"`
			Accurate *int `json:"accurate"`
		} `json:"crosses"`

		Dribbles struct {
			Attempts     *int `json:"attempts"`
			Success      *int `json:"success"`
			DribbledPast *int `json:"dribbled_past"`
		} `json:"dribbles"`

		Duels struct {
			Total *int `json:"total"`
			Won   *int `json:"won"`
		} `json:"duels"`

		Passes struct {
			Total     *int `json:"total"`
			Accuracy  *int `json:"accuracy"`
			KeyPasses *int `json:"key_passes"`
		} `json:"passes"`

		Penalties struct {
			Won       *int `json:"won"`
			Scores    *int `json:"scores"`
			Missed    *int `json:"missed"`
			Committed *int `json:"committed"`
			Saves     *int `json:"saves"`
		} `json:"penalties"`

		Shots struct {
			ShotsTotal     *int `json:"shots_total"`
			ShotsOnTarget  *int `json:"shots_on_target"`
			ShotsOffTarget *int `json:"shots_off_target"`
		} `json:"shots"`

		PlayerData struct {
			Data Player `json:"data"`
		} `json:"player"`
	}

	// venue структура содержит данные по стадиону
	venue struct {
		Id          int    `json:"id"`
		Name        string `json:"name"`
		Surface     string `json:"surface"`
		Address     string `json:"address"`
		City        string `json:"city"`
		Capacity    *int   `json:"capacity"`
		ImagePath   string `json:"image_path"`
		Coordinates string `json:"coordinates"`
	}

	// ranking рейтинг команды (ФИФА УЕФА)
	ranking struct {
		Data struct {
			TeamId            int     `json:"team_id"`
			Points            *int    `json:"points"`
			Position          *int    `json:"position"`
			PositionStatus    *string `json:"position_status"`
			PositionWonOrLost *string `json:"position_won_or_lost"`
			Coeffiecient      *int    `json:"coeffiecient"`
		} `json:"data"`
	}

	ResponseTeams struct {
		Data []Team `json:"data"`
		Meta meta   `json:"meta"`
	}
)
