package types

type (
	assistantsReferee struct {
		FirstAssistantId  *int `json:"first_assistant_id"`
		SecondAssistantId *int `json:"second_assistant_id"`
		FourthOfficialId  *int `json:"fourth_official_id"`
	}

	referee struct {
		Id         int     `json:"id"`
		CommonName *string `json:"common_name"`
		Fullname   *string `json:"fullname"`
		Firstname  *string `json:"firstname"`
		Lastname   *string `json:"lastname"`
	}
)
