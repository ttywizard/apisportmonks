package types

import (
	"encoding/json"
	"errors"
	"strings"
)

const QuotesByte = 34

type (
	Include map[string]bool

	CustomString struct {
		String string
	}

	CustomInt struct {
		Int *int
	}
)

func (c *CustomString) UnmarshalJSON(data []byte) error {
	if data[0] == QuotesByte {
		err := json.Unmarshal(data, &c.String)
		if err != nil {
			return errors.New("CustomString: UnmarshalJSON: " + err.Error())
		}
	} else {
		c.String = string(data)
	}

	return nil
}

func (c *CustomInt) UnmarshalJSON(data []byte) error {
	if data[0] != QuotesByte {
		err := json.Unmarshal(data, &c.Int)
		if err != nil {
			return errors.New("CustomString: UnmarshalJSON: " + err.Error())
		}
	}

	return nil
}

func (i Include) SetHandler(name []string) {

	for _, typeHandler := range name {
		switch typeHandler {
		case "season":
			i[typeHandler] = true
		case "fixtures.aggregate":
			i[typeHandler] = true
		case "aggregate":
			i[typeHandler] = true
		case "substitutions":
			i[typeHandler] = true
		case "events":
			i[typeHandler] = true
		case "referee":
			i[typeHandler] = true
		case "lineup":
			i[typeHandler] = true
		case "bench":
			i[typeHandler] = true
		case "sidelined":
			i[typeHandler] = true
		case "odds":
			i[typeHandler] = true
		case "stats":
			i[typeHandler] = true
		case "country":
			i[typeHandler] = true
		case "squad":
			i[typeHandler] = true
		case "coach":
			i[typeHandler] = true
		case "transfers":
			i[typeHandler] = true
		case "venue":
			i[typeHandler] = true
		case "fifaranking":
			i[typeHandler] = true
		case "uefaranking":
			i[typeHandler] = true
		case "visitorFixtures":
			i[typeHandler] = true
		case "localFixtures":
			i[typeHandler] = true
		case "visitorResults":
			i[typeHandler] = true
		case "latest":
			i[typeHandler] = true
		case "upcoming":
			i[typeHandler] = true
		case "goalscorers":
			i[typeHandler] = true
		case "cardscorers":
			i[typeHandler] = true
		case "assistscorers":
			i[typeHandler] = true
		case "aggregatedgoalscorers":
			i[typeHandler] = true
		case "aggregatedcardscorers":
			i[typeHandler] = true
		case "aggregatedassistscorers":
			i[typeHandler] = true
		case "trophies":
			i[typeHandler] = true
		case "probability":
			i[typeHandler] = true
		case "fixtures.stage":
			i[typeHandler] = true
		case "groups":
			i[typeHandler] = true
		case "fixtures":
			i[typeHandler] = true
		case "fixtures.sidelined":
			i[typeHandler] = true
		case "fixtures.stats":
			i[typeHandler] = true
		case "fixtures.lineup":
			i[typeHandler] = true
		case "fixtures.bench":
			i[typeHandler] = true
		case "fixtures.events":
			i[typeHandler] = true
		case "fixtures.referee":
			i[typeHandler] = true
		case "fixtures.firstAssistant":
			i[typeHandler] = true
		case "fixtures.secondAssistant":
			i[typeHandler] = true
		case "fixtures.fourthOfficial":
			i[typeHandler] = true
		case "squad.player":
			i["squad.player"] = true
		}
	}

}

func (i Include) SetSeasons() {
	i["stages"] = true
	i["rounds"] = true
	i["groups"] = true
	i["fixtures"] = true
	i["fixtures.Lineup"] = true
	i["fixtures.bench"] = true
	i["fixtures.events"] = true
	i["fixtures.stats"] = true
}

func (i Include) GetStringIncludes() string {
	incStr := ""
	for k, v := range i {
		if v {
			incStr += k + ","
		}
	}
	return strings.Trim(incStr, ",")
}
