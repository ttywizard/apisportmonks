package types

type (
	Player struct {
		PlayerId     int     `json:"player_id"`
		TeamId       *int    `json:"team_id"`
		CountryId    *int    `json:"country_id"`
		PositionId   *int    `json:"position_id"`
		CommonName   *string `json:"common_name"`
		DisplayName  *string `json:"display_name"`
		Fullname     *string `json:"fullname"`
		Firstname    *string `json:"firstname"`
		Lastname     *string `json:"lastname"`
		Nationality  *string `json:"nationality"`
		Birthdate    *string `json:"birthdate"`
		Birthcountry *string `json:"birthcountry"`
		Birthplace   *string `json:"birthplace"`
		Height       *string `json:"height"`
		Weight       *string `json:"weight"`
		ImagePath    *string `json:"image_path"`
	}

	// statsPlayer Статистика по игроку
	statsPlayer struct {
		Rating *string `json:"rating"`
		Shots  struct {
			ShotsTotal  *int `json:"shots_total"`
			ShotsOnGoal *int `json:"shots_on_goal"`
		} `json:"shots"`

		Goals struct {
			Scored   *int `json:"scored"`
			Assists  *int `json:"assists"`
			Conceded *int `json:"conceded"`
			Owngoals *int `json:"owngoals"`
		} `json:"goals"`

		Fouls struct {
			Drawn     *int `json:"drawn"`
			Committed *int `json:"committed"`
		} `json:"fouls"`

		Cards struct {
			Yellowcards    *int `json:"yellowcards"`
			Redcards       *int `json:"redcards"`
			Yellowredcards *int `json:"yellowredcards"`
		} `json:"cards"`

		Passing struct {
			TotalCrosses    *int `json:"total_crosses"`
			CrossesAccuracy *int `json:"crosses_accuracy"`
			Passes          *int `json:"passes"`
			AccuratePasses  *int `json:"accurate_passes"`
			PassesAccuracy  *int `json:"passes_accuracy"`
			KeyPasses       *int `json:"key_passes"`
		} `json:"passing"`

		Dribbles struct {
			Attempts     *int `json:"attempts"`
			Success      *int `json:"success"`
			DribbledPast *int `json:"dribbled_past"`
		} `json:"dribbles"`

		Duels struct {
			Total *int `json:"total"`
			Won   *int `json:"won"`
		} `json:"duels"`

		Other struct {
			AerialsWon     *int `json:"aerials_won"`
			Punches        *int `json:"punches"`
			Offsides       *int `json:"offsides"`
			Saves          *int `json:"saves"`
			InsideBoxSaves *int `json:"inside_box_saves"`
			PenScored      *int `json:"pen_scored"`
			PenMissed      *int `json:"pen_missed"`
			PenSaved       *int `json:"pen_saved"`
			PenCommitted   *int `json:"pen_committed"`
			PenWon         *int `json:"pen_won"`
			HitWoodwork    *int `json:"hit_woodwork"`
			Tackles        *int `json:"tackles"`
			Blocks         *int `json:"blocks"`
			Interceptions  *int `json:"interceptions"`
			Clearances     *int `json:"clearances"`
			Dispossesed    *int `json:"dispossesed"`
			MinutesPlayed  *int `json:"minutes_played"`
		}
	}
)
