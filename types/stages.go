package types

type (
	Stage struct {
		Id           int    `json:"id"`
		Name         string `json:"name"`
		Type         string `json:"type"`
		LeagueId     int    `json:"league_id"`
		SeasonId     int    `json:"season_id"`
		SortOrder    int    `json:"sort_order"`
		HasStandings bool   `json:"has_standings"`
	}

	ResponseStages struct {
		Data []Stage `json:"data"`
		Meta meta    `json:"meta"`
	}
)
