package types

type (

	// ResponseSeason структура сохраняет данные по сезону
	ResponseSeason struct {
		Data struct {
			Id              int    `json:"id"`
			Name            string `json:"name"`
			LeagueId        int    `json:"league_id"`
			IsCurrentSeason bool   `json:"is_current_season"`
			CurrentRoundId  int    `json:"current_round_id"`
			CurrentStageId  int64  `json:"current_stage_id"`

			Rounds struct {
				Data []Round `json:"data"`
			} `json:"rounds"`

			Fixtures struct {
				Data []Fixture `json:"data"`
			} `json:"fixtures"`

			Groups struct {
				Data []struct {
					Id        *int    `json:"id"`
					Name      *string `json:"name"`
					LeagueId  *int    `json:"league_id"`
					SeasonId  *int    `json:"season_id"`
					RoundId   *int    `json:"round_id"`
					RoundName *int    `json:"round_name"`
					StageId   *int    `json:"stage_id"`
					StageName *string `json:"stage_name"`
					Resource  *string `json:"resource"`
				}
			} `json:"groups"`
		} `json:"data"`

		Meta meta `json:"meta"`
	}
)
