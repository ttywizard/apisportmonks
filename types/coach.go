package types

type (

	// coach структура содержит данные по тренеру
	coach struct {
		Data struct {
			CoachId      int     `json:"coach_id"`
			TeamId       *int    `json:"team_id"`
			CountryId    int     `json:"country_id"`
			CommonName   *string `json:"common_name"`
			Fullname     *string `json:"fullname"`
			Firstname    *string `json:"firstname"`
			Lastname     *string `json:"lastname"`
			Nationality  *string `json:"nationality"`
			Birthdate    *string `json:"birthdate"`
			Birthcountry *string `json:"birthcountry"`
			Birthplace   *string `json:"birthplace"`
			ImagePath    *string `json:"image_path"`
		} `json:"data"`
	}

	FixtureCoach struct {
		LocalteamCoachId   *int `json:"localteam_coach_id"`
		VisitorteamCoachId *int `json:"visitorteam_coach_id"`
	}
)
