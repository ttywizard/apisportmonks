package types

type (

	//Odds головной элемент содержащий ставки и коффициенты
	Odds struct {
		//Массив маркетов(типов ставок)
		Data []MarketJson `json:"data"`
	}

	MarketJson struct {
		Id        int            `json:"id"`
		Name      string         `json:"name"`
		Suspended bool           `json:"suspended"`
		Bookmaker BookmakersData `json:"bookmaker"`
	}

	BookmakersData struct {
		Data []struct {
			Id   int      `json:"id"`
			Name string   `json:"name"`
			Odds OddsData `json:"odds"`
		} `json:"data"`
	}

	OddsData struct {
		Data []struct {
			Label            *string       `json:"label"`
			Value            *CustomString `json:"value"`
			Extra            *string       `json:"extra"`
			Probability      *string       `json:"probability"`
			Dp3              *string       `json:"dp3"`
			American         *int          `json:"american"`
			Factional        *string       `json:"factional"`
			Handicap         CustomString  `json:"handicap"`
			Total            *string       `json:"total"`
			Winning          *bool         `json:"winning"`
			Stop             bool          `json:"stop"`
			BookmakerEventId CustomInt     `json:"bookmaker_event_id"`
			LastUpdate       struct {
				Date         string `json:"date"`
				TimezoneType int    `json:"timezone_type"`
				Timezone     string `json:"timezone"`
			} `json:"last_update"`
		} `json:"data"`
	}

	Bookmaker struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
		Logo string `json:"logo"`
	}

	Market struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	}

	ResponseMarket struct {
		Data []Market `json:"data"`
		Meta meta     `json:"meta"`
	}

	ResponseBookmaker struct {
		Data []Bookmaker `json:"data"`
		Meta meta        `json:"meta"`
	}

	Probability struct {
		Data struct {
			Predictions struct {
				Btts         float64            `json:"btts"`
				Over25       float64            `json:"over_2_5"`
				Under25      float64            `json:"under_2_5"`
				Over35       float64            `json:"over_3_5"`
				Under35      float64            `json:"under_3_5"`
				HTOver05     float64            `json:"HT_over_0_5"`
				HTUnder05    float64            `json:"HT_under_0_5"`
				HTOver15     float64            `json:"HT_over_1_5"`
				HTUnder15    float64            `json:"HT_under_1_5"`
				ATOver05     float64            `json:"AT_over_0_5"`
				ATUnder05    float64            `json:"AT_under_0_5"`
				ATOver15     float64            `json:"AT_over_1_5"`
				ATUnder15    float64            `json:"AT_under_1_5"`
				Home         float64            `json:"home"`
				Away         float64            `json:"away"`
				Draw         float64            `json:"draw"`
				CorrectScore map[string]float64 `json:"correct_score"`
			} `json:"predictions"`
		} `json:"data"`
	}
)
