package types

type (
	Fixture struct {
		Id                    int               `json:"id"`
		LeagueId              int               `json:"league_id"`
		SeasonId              int               `json:"season_id"`
		StageId               *int              `json:"stage_id"`
		RoundId               *int              `json:"round_id"`
		GroupId               *int              `json:"group_id"`
		AggregateId           *int              `json:"aggregate_id"`
		VenueId               *int              `json:"venue_id"`
		RefereeId             *int              `json:"referee_id"`
		LocalteamId           *int              `json:"localteam_id"`
		VisitorteamId         *int              `json:"visitorteam_id"`
		WinnerTeamId          *int              `json:"winner_team_id"`
		Commentaries          bool              `json:"commentaries"`
		Attendance            *int              `json:"attendance"`
		Pitch                 *string           `json:"pitch"`
		Details               *string           `json:"details"`
		NeutralVenue          bool              `json:"neutral_venue"`
		WinningOddsCalculated bool              `json:"winning_odds_calculated"`
		Leg                   *string           `json:"leg"`
		Deleted               bool              `json:"deleted"`
		IsPlaceholder         bool              `json:"is_placeholder"`
		Formations            formations        `json:"formations"`
		WeatherReport         weatherReport     `json:"weather_report"`
		Scores                scores            `json:"scores"`
		TimeData              timeD             `json:"time"`
		Coach                 FixtureCoach      `json:"coaches"`
		Assistants            assistantsReferee `json:"assistants"`
		Colors                colors            `json:"colors"`

		//Данные по судьям
		Referee struct {
			Data referee `json:"data"`
		} `json:"referee"`

		FirstAssistant struct {
			Data referee `json:"data"`
		} `json:"firstAssistant"`

		SecondAssistant struct {
			Data referee `json:"data"`
		} `json:"secondAssistant"`

		FourthOfficial struct {
			Data referee `json:"data"`
		} `json:"fourthOfficial"`

		// Места на момент игры
		Standings struct {
			LocalteamPosition   *int `json:"localteam_position"`
			VisitorteamPosition *int `json:"visitorteam_position"`
		} `json:"standings"`

		// Основной состав
		Lineup struct {
			Data []Lineup `json:"data"`
		} `json:"Lineup"`

		// Запасной состав
		Bench struct {
			Data []Lineup `json:"data"`
		} `json:"bench"`

		SideLined struct {
			Data []sidelined `json:"data"`
		} `json:"sidelined"`

		// Статистика матча
		Stats struct {
			Data []statsMatch `json:"data"`
		} `json:"stats"`

		// События
		Events struct {
			Data []event `json:"data"`
		} `json:"events"`

		//Odds
		OddsD Odds `json:"odds"`

		//Probability
		Probabilities Probability `json:"probability"`

		//Aggregate
		Aggregate *aggregate `json:"aggregate"`
	}

	// Цвета формы
	color struct {
		Color     *string `json:"color"`
		KitColors *string `json:"kit_colors"`
	}

	colors struct {
		Localteam   color `json:"localteam"`
		Visitorteam color `json:"visitorteam"`
	}

	// timeD структура для хранения о времени(дата, добавочное время и так далее)
	timeD struct {
		Status      *string   `json:"status"`
		Minute      CustomInt `json:"minute"`
		Second      CustomInt `json:"second"`
		AddedTime   CustomInt `json:"added_time"`
		ExtraMinute CustomInt `json:"extra_minute"`
		InjuryTime  CustomInt `json:"injury_time"`
		StartingAt  struct {
			DateTime  *string `json:"date_time"`
			Date      *string `json:"date"`
			Time      *string `json:"time"`
			Timestamp *int    `json:"timestamp"`
			Timezone  *string `json:"timezone"`
		} `json:"starting_at"`
	}

	// formations Расстоновка команд
	formations struct {
		LocalteamFormation   string `json:"localteam_formation"`
		VisitorteamFormation string `json:"visitorteam_formation"`
	}

	temperature struct {
		Temp *float64 `json:"temp"`
		Unit *string  `json:"unit"`
	}

	// weatherReport погода
	weatherReport struct {
		Code               *string     `json:"code"`
		Type               *string     `json:"type"`
		Icon               *string     `json:"icon"`
		Clouds             *string     `json:"clouds"`
		Humidity           *string     `json:"humidity"`
		Pressure           *float64    `json:"pressure"`
		UpdatedAt          *string     `json:"updated_at"`
		Temperature        temperature `json:"temperature"`
		TemperatureCelcius temperature `json:"temperature_celcius"`
		Wind               struct {
			Speed  *string  `json:"speed"`
			Degree *float64 `json:"degree"`
		} `json:"wind"`
		Coordinates struct {
			Lat *float64 `json:"lat"`
			Lon *float64 `json:"lon"`
		} `json:"coordinates"`
	}

	// scores структура содержит результат матча(разбитый по таймам)
	scores struct {
		LocalteamScore      *int    `json:"localteam_score"`
		VisitorteamScore    *int    `json:"visitorteam_score"`
		LocalteamPenScore   *int    `json:"localteam_pen_score"`
		VisitorteamPenScore *int    `json:"visitorteam_pen_score"`
		HtScore             *string `json:"ht_score"`
		FtScore             *string `json:"ft_score"`
		EtScore             *string `json:"et_score"`
		PsScore             *string `json:"ps_score"`
	}

	//statsMatch Статистика в разрезе матча
	statsMatch struct {
		TeamId    int `json:"team_id"`
		FixtureId int `json:"fixture_id"`
		Shots     struct {
			Total      *int `json:"total"`
			Ongoal     *int `json:"ongoal"`
			Blocked    *int `json:"blocked"`
			Offgoal    *int `json:"offgoal"`
			Insidebox  *int `json:"insidebox"`
			Outsidebox *int `json:"outsidebox"`
		} `json:"shots"`

		Passes struct {
			Total      *int    `json:"total"`
			Accurate   *int    `json:"accurate"`
			Percentage float64 `json:"percentage"`
		} `json:"passes"`

		Attacks struct {
			Attacks          *int `json:"attacks"`
			DangerousAttacks *int `json:"dangerous_attacks"`
		} `json:"attacks"`

		Fouls          *int `json:"fouls"`
		Corners        *int `json:"corners"`
		Offsides       *int `json:"offsides"`
		Possessiontime *int `json:"possessiontime"`
		Yellowcards    *int `json:"yellowcards"`
		Redcards       *int `json:"redcards"`
		Yellowredcards *int `json:"yellowredcards"`
		Saves          *int `json:"saves"`
		Substitutions  *int `json:"substitutions"`
		GoalKick       *int `json:"goal_kick"`
		GoalAttempts   *int `json:"goal_attempts"`
		FreeKick       *int `json:"free_kick"`
		ThrowIn        *int `json:"throw_in"`
		BallSafe       *int `json:"ball_safe"`
		Goals          *int `json:"goals"`
		Penalties      *int `json:"penalties"`
		Injuries       *int `json:"injuries"`
		Tackles        *int `json:"tackles"`
	}

	// event События
	event struct {
		Id                *uint64 `json:"id"`
		TeamId            *string `json:"team_id"`
		Type              *string `json:"type"`
		VarResult         *string `json:"var_result"`
		FixtureId         *int    `json:"fixture_id"`
		PlayerId          *int    `json:"player_id"`
		PlayerName        *string `json:"player_name"`
		RelatedPlayerId   *int    `json:"related_player_id"`
		RelatedPlayerName *string `json:"related_player_name"`
		Minute            *int    `json:"minute"`
		ExtraMinute       *int    `json:"extra_minute"`
		Reason            *string `json:"reason"`
		Injuried          bool    `json:"injuried"`
		Result            *string `json:"result"`
		OnPitch           bool    `json:"on_pitch"`
	}

	aggregate struct {
		Data struct {
			Id            *int    `json:"id"`
			LeagueId      *int    `json:"league_id"`
			SeasonId      *int    `json:"season_id"`
			StageId       *int    `json:"stage_id"`
			Localteam     *string `json:"localteam"`
			LocalteamId   *int    `json:"localteam_id"`
			VisitorTeam   *string `json:"visitor_team"`
			VisitorteamId *int    `json:"visitorteam_id"`
			Result        *string `json:"result"`
			Winner        *int    `json:"winner"`
			Detail        *string `json:"detail"`
		} `json:"data"`
	}

	ResponseFixtures struct {
		Data []Fixture `json:"data"`
		Meta *meta     `json:"meta"`
	}
)
