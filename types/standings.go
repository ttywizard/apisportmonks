package types

type (
	StandingsData struct {
		Id        int     `json:"id"`
		Name      *string `json:"name"`
		LeagueId  int     `json:"league_id"`
		SeasonId  int     `json:"season_id"`
		RoundId   int     `json:"round_id"`
		RoundName int     `json:"round_name"`
		Type      *string `json:"type"`
		StageId   int     `json:"stage_id"`
		StageName *string `json:"stage_name"`
		Resource  *string `json:"resource"`
		Standings struct {
			Data []standings `json:"data"`
		} `json:"standings"`
	}

	standings struct {
		Position   int           `json:"position"`
		TeamId     int           `json:"team_id"`
		TeamName   string        `json:"team_name"`
		RoundId    int           `json:"round_id"`
		RoundName  int           `json:"round_name"`
		GroupId    *int          `json:"group_id"`
		GroupName  *string       `json:"group_name"`
		Overall    TeamStats     `json:"overall"`
		Home       TeamStats     `json:"home"`
		Away       TeamStats     `json:"away"`
		Result     *string       `json:"result"`
		Points     *int          `json:"points"`
		RecentForm *string       `json:"recent_form"`
		Status     *string       `json:"status"`
		SData      StandingsData `json:"standings"`
	}

	TeamStats struct {
		GamesPlayed  *int `json:"games_played"`
		Won          *int `json:"won"`
		Draw         *int `json:"draw"`
		Lost         *int `json:"lost"`
		GoalsScored  *int `json:"goals_scored"`
		GoalsAgainst *int `json:"goals_against"`
		Points       *int `json:"points"`
	}

	ResponseStandins struct {
		Data []StandingsData `json:"data"`
		Meta meta            `json:"meta"`
	}
)
