package types

type (
	//Lineup Статистика по игроку(стартовый состав или запасные, рейтинг, статистика)
	Lineup struct {
		TeamId             int         `json:"team_id"`
		FixtureId          int         `json:"fixture_id"`
		PlayerId           int         `json:"player_id"`
		PlayerName         string      `json:"player_name"`
		Number             int         `json:"number"`
		Position           string      `json:"position"`
		AdditionalPosition string      `json:"additional_position"`
		FormationPosition  int         `json:"formation_position"`
		Posx               *int        `json:"posx"`
		Posy               *int        `json:"posy"`
		Captain            bool        `json:"captain"`
		Type               string      `json:"type"`
		Stats              statsPlayer `json:"stats"`
	}
)
