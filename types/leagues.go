package types

type (
	//League Структура лиги
	League struct {
		Id              int     `json:"id"`
		Active          bool    `json:"active"`
		Type            string  `json:"type"`
		LegacyId        int     `json:"legacy_id"`
		CountryId       int     `json:"country_id"`
		LogoPath        *string `json:"logo_path"`
		Name            string  `json:"name"`
		IsCup           bool    `json:"is_cup"`
		CurrentSeasonId *int    `json:"current_season_id"`
		CurrentRoundId  *int    `json:"current_round_id"`
		CurrentStageId  *int    `json:"current_stage_id"`
		LiveStandings   bool    `json:"live_standings"`
		Coverage        struct {
			Predictions      bool `json:"predictions"`
			TopscorerGoals   bool `json:"topscorer_goals"`
			TopscorerAssists bool `json:"topscorer_assists"`
			TopscorerCards   bool `json:"topscorer_cards"`
		} `json:"coverage"`

		// Сезоны лиги
		Seasons Seasons `json:"seasons"`

		// Страна лиги
		Country struct {
			Country Country `json:"data"`
		} `json:"country"`
	}

	Seasons struct {
		Data []Season `json:"data"`
	}

	Season struct {
		Id              int    `json:"id"`
		Name            string `json:"name"`
		LeagueId        int    `json:"league_id"`
		IsCurrentSeason bool   `json:"is_current_season"`
		CurrentRoundId  int    `json:"current_round_id"`
		CurrentStageId  int    `json:"current_stage_id"`
	}

	ResponseLeague struct {
		Data []League `json:"data"`
		Meta meta     `json:"meta"`
	}
)
