package client

import (
	"compress/gzip"
	"errors"
	logger2 "github.com/Pastir/logger"
	"github.com/google/brotli/go/cbrotli"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"time"
)

func RequestToEndpoint(url string) (body []byte, err error) {

	client := http.Client{
		Timeout: 60 * time.Second,
	}

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("accept-encoding", "gzip, br")

	res, _ := client.Do(req)
	if res.StatusCode != 200 {
		logger2.ErrorLog.Error("response status error: ",
			zap.String("Request", url),
			zap.String("status", res.Status),
		)
		return []byte(""), errors.New("404")
	} else {
		logger2.InfoLog.Info("new request: ",
			zap.String("Request", url),
			zap.String("status", res.Status),
		)
	}

	defer res.Body.Close()

	switch res.Header.Get("Content-Encoding") {
	case "br":
		reader := cbrotli.NewReader(res.Body)
		defer reader.Close()
		return ioutil.ReadAll(reader)

	case "gzip":
		reader, _ := gzip.NewReader(res.Body)
		defer reader.Close()
		return ioutil.ReadAll(reader)

	default:
		return ioutil.ReadAll(res.Body)
	}
}

func CustomRequest(url string, headers map[string]string) (body []byte, err error) {

	client := http.Client{
		Timeout: 60 * time.Second,
	}

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("accept-encoding", "gzip, br")

	//Установка нужных хидеров
	for k, v := range headers {
		req.Header.Set(k, v)
	}

	res, _ := client.Do(req)
	if res.StatusCode != 200 {
		logger2.ErrorLog.Error("response status error: ",
			zap.String("Request", url),
			zap.String("status", res.Status),
		)
		return []byte(""), errors.New("404")
	} else {
		logger2.InfoLog.Info("new request: ",
			zap.String("Request", url),
			zap.String("status", res.Status),
		)
	}

	defer res.Body.Close()

	switch res.Header.Get("Content-Encoding") {
	case "br":
		reader := cbrotli.NewReader(res.Body)
		defer reader.Close()
		return ioutil.ReadAll(reader)

	case "gzip":
		reader, _ := gzip.NewReader(res.Body)
		defer reader.Close()
		return ioutil.ReadAll(reader)

	default:
		return ioutil.ReadAll(res.Body)
	}
}
