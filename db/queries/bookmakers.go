package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/gbet/bettings-lines/common"
	"os"
)

var BookmakersData map[int]db.BookmakerData
var MarketsData map[int]db.BookmakerData

// GetIdBookmakers возвращает данные об активных букмекерах
func GetIdBookmakers() {

	BookmakersData = make(map[int]db.BookmakerData)

	query := `SELECT id, uid, name
              FROM matchcenter_bookmaker
              WHERE active`

	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetIdBookmakers", err)
		os.Exit(1)
	}

	for rows.Next() {
		var bk db.BookmakerData

		err = rows.Scan(&bk.Id, &bk.UUID, &bk.Name)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetIdBookmakers", err)
		}

		BookmakersData[bk.Id] = bk
	}
}

// GetIdMarkets возвращает данные об активных типах ставок
func GetIdMarkets() {

	MarketsData = make(map[int]db.BookmakerData)

	query := `SELECT id, uid, name
              FROM matchcenter_market
              WHERE active`

	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetIdMarkets", err)
		os.Exit(1)
	}

	for rows.Next() {
		var bk db.BookmakerData

		err = rows.Scan(&bk.Id, &bk.UUID, &bk.Name)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetIdMarkets", err)
		}

		MarketsData[bk.Id] = bk
	}
}

//GetListMarket -
func GetListMarket(BkId int) (map[string]common.MatchBkMarket, error) {
	listBkMarket := make(map[string]common.MatchBkMarket)
	query := `SELECT market_id, 
                     COALESCE(bk_market_id, 0) AS bk_market_id, 
                     bk_market_name, 
                     bk_id 
              FROM matchcenter_bk_market
              WHERE bk_id = $1`
	rows, err := db.Db.Query(query, BkId)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetListMarket", err)
		return listBkMarket, err
	}

	for rows.Next() {
		var market common.MatchBkMarket
		err = rows.Scan(&market.MarketId, &market.BkMarketId, &market.BkMarketName, &market.BkId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetListMarket", err)
			return listBkMarket, err
		}
		listBkMarket[market.BkMarketName] = market
	}
	return listBkMarket, nil
}
