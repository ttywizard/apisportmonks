package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/types"
	"os"
)

var (
	MapCountryProviderIdToUuid map[int]string
	MapCountryDataByIso        map[string]types.CountryData
)

func GetUuidListProviderCountryId() (map[int]string, error) {
	MapCountryProviderIdToUuid = make(map[int]string)

	query := `SELECT mcp_country.id,
                     mc_country.uid
              FROM matchcenter_providers_country AS mcp_country LEFT JOIN matchcenter_country AS mc_country ON(mcp_country.country_uid = mc_country.uid)`

	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetUuidListProviderCountryId", err)
		return MapCountryProviderIdToUuid, err
	}

	for rows.Next() {
		var countryUUID string
		var countryProvId int

		err = rows.Scan(&countryProvId, &countryUUID)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetUuidListProviderCountryId", err)
			return MapCountryProviderIdToUuid, err
		}

		MapCountryProviderIdToUuid[countryProvId] = countryUUID
	}
	return MapCountryProviderIdToUuid, nil
}

//GetProviderCountry
// Возвращает названия стран в формате провайдера услуг
func GetProviderCountry() (map[string]string, error) {
	ls := make(map[string]string)
	query := `SELECT mca.alternatename,
                     mca.uid
              FROM matchcenter_alternativename AS mca LEFT JOIN matchcenter_country mcc ON(mca.uid = mcc.uid)
              WHERE mca.isolanguage = 'api-football'`

	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetProviderCountry", err)
		return ls, err
	}

	for rows.Next() {
		var countryUUID string
		var countryName string

		err = rows.Scan(&countryName, &countryUUID)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetProviderCountry", err)
			return ls, err
		}

		ls[countryName] = countryUUID
	}
	return ls, nil
}

//GetCountryDataByISO функция возвращает данные по коду страны.
func GetCountryDataByISO() {
	MapCountryDataByIso = make(map[string]types.CountryData)

	query := `SELECT countrycode, countryname, uid FROM matchcenter_country ORDER BY 1`
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetCountryDataByISO", err)
		os.Exit(1)
	}

	for rows.Next() {
		var uid string
		var countryCode string
		var countryName string

		err = rows.Scan(&countryCode, &countryName, &uid)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetCountryDataByISO", err)
			os.Exit(1)
		}

		var cN types.CountryName
		cN.CountryCode = countryCode
		cN.CountryName = countryName
		cN.Uid = uid
		if _, found := MapCountryDataByIso[countryCode]; found {
			MapCountryDataByIso[countryCode].Countries[countryName] = append(MapCountryDataByIso[countryCode].Countries[countryName], cN)
		} else {
			var cD types.CountryData
			cD.Countries = make(map[string][]types.CountryName)
			cD.Countries[countryName] = append(cD.Countries[countryName], cN)
			MapCountryDataByIso[countryCode] = cD
		}
	}
}
