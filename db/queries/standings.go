package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var ListStandingRules map[string]string

func GetStandingRules() {
	ListStandingRules = make(map[string]string)

	query := `SELECT uid, name FROM matchcenter_standing_rules WHERE provider_id = $1`
	//Запрос
	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetStandingRules", err)
		return
	}

	for rows.Next() {
		var rule string
		var ruleUid string

		err = rows.Scan(&ruleUid, &rule)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetStandingRules", err)
			return
		}
		ListStandingRules[rule] = ruleUid
	}
}
