package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"strconv"
	"strings"
)

// GetSeasons возвращает мапу leagueId -> seasonsId
func GetSeasons(stats db.Statistics) map[int][]db.SeasonData {
	seasons := make(map[int][]db.SeasonData)
	whereSection := stats.GetStringWhere()

	query := `SELECT mcl.id   AS league_id
                     ,mcs.id  AS season_id
                     ,mcl.uid AS league_uid
                     ,mcs.uid AS season_uid
              FROM matchcenter_league AS mcl LEFT JOIN matchcenter_season AS mcs ON(mcl.uid = mcs.league_uid)
              WHERE mcl.provider_id = $1 AND active = 't'`

	if whereSection != "" {
		query += " AND " + whereSection
	}

	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetSeasons", err)
		return nil
	}

	for rows.Next() {
		var seasData db.SeasonData

		err = rows.Scan(&seasData.LeagueId, &seasData.SeasonId, &seasData.LeagueUid, &seasData.SeasonUid)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetSeasons", err)
			return nil
		}
		seasons[seasData.LeagueId] = append(seasons[seasData.LeagueId], seasData)
	}
	return seasons
}

// GetSeasonsIdData возвращает мапу  seasonsId -> struct Data
func GetSeasonsIdData(stats db.Statistics) map[int]db.SeasonData {
	MapSeasonsIdData := make(map[int]db.SeasonData)
	whereSection := stats.GetStringWhere()

	query := `SELECT mcl.id   AS league_id
                     ,mcs.id  AS season_id
                     ,mcl.uid AS league_uid
                     ,mcs.uid AS season_uid
                     ,CONCAT(date_part('year', mcs.season_start)::TEXT , '/', date_part('year', mcs.season_end)::TEXT) AS season
              FROM matchcenter_league AS mcl LEFT JOIN matchcenter_season AS mcs ON(mcl.uid = mcs.league_uid)
              WHERE active = 't'
                    AND mcs.current`

	if whereSection != "" {
		query += " AND " + whereSection
	}

	query += "  ORDER BY mcl.id, mcs.id"

	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetSeasonsIdData", err)
		return nil
	}

	for rows.Next() {
		var seasData db.SeasonData

		err = rows.Scan(&seasData.LeagueId, &seasData.SeasonId, &seasData.LeagueUid, &seasData.SeasonUid, &seasData.Season)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetSeasonsIdData", err)
			return nil
		}
		MapSeasonsIdData[seasData.SeasonId] = seasData
	}

	return MapSeasonsIdData
}

var SeasonPointRules map[int]db.PointRules

func GetSeasonPointRules() {
	SeasonPointRules = make(map[int]db.PointRules)

	query := `SELECT mc_s.id,
                     rules
	          FROM matchcenter_season AS mc_s LEFT JOIN matchcenter_league AS ml ON (mc_s.league_uid = ml.uid)
              WHERE active = 't'`

	//Запрос
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("SeasonPointRules", err)
		return
	}

	for rows.Next() {
		var seasId int
		var pRules db.PointRules

		err = rows.Scan(&seasId, &pRules)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("SeasonPointRules", err)
			return
		}
		SeasonPointRules[seasId] = pRules
	}
}

func GetStageSeasonsHasStandings(stats db.Statistics) []int {

	var seasonData []int
	var sectionWhere string
	var andWhere []string
	//Выбран сезон
	if stats.LeagueId != 0 {
		andWhere = append(andWhere, `league_id = `+strconv.Itoa(stats.LeagueId))
	}

	//Выбрана лига
	if stats.SeasonId != 0 {
		andWhere = append(andWhere, `season_id = `+strconv.Itoa(stats.SeasonId))
	}

	//Только текущие сезоны
	if stats.Current {
		andWhere = append(andWhere, "current")
	}

	if len(andWhere) > 0 {
		sectionWhere += " WHERE "
		for _, w := range andWhere {
			sectionWhere += w + " AND "
		}
	}

	//Собираем запрос
	subQuery := `SELECT id FROM matchcenter_season `
	query := `SELECT season_id 
           FROM matchcenter_stages 
           WHERE season_id IN (` + (subQuery + strings.Trim(sectionWhere, "AND ")) + `) AND has_standings`

	//Запрос
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetSeasonsHasStandings", err)
		return nil
	}

	for rows.Next() {
		var seasId int

		err = rows.Scan(&seasId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetSeasonsHasStandings", err)
			return nil
		}
		seasonData = append(seasonData, seasId)
	}

	return seasonData
}

func GetSeasonsHasStandings(stats db.Statistics) []int {

	var seasonData []int
	//Собираем запрос
	query := `SELECT season_id
              FROM matchcenter_stages
              WHERE has_standings AND provider_id = $1`

	if stats.LeagueId != 0 {
		query += ` AND league_id = ` + strconv.Itoa(stats.LeagueId)
	}
	if stats.SeasonId != 0 {
		query += ` AND season_id = ` + strconv.Itoa(stats.SeasonId)
	}

	query += ` GROUP BY season_id`

	//Запрос
	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetSeasonsHasStandings", err)
		return nil
	}

	for rows.Next() {
		var seasId int

		err = rows.Scan(&seasId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetSeasonsHasStandings", err)
			return nil
		}
		seasonData = append(seasonData, seasId)
	}

	return seasonData
}
