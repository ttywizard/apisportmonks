package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var ListGroupsName map[string]string

func GetGroupsName() {
	ListGroupsName = make(map[string]string)

	//Собираем запрос
	query := `SELECT uid, name FROM public.matchcenter_groups_name WHERE provider_id = $1`

	//Запрос
	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetGroupsName", err)
		return
	}

	for rows.Next() {
		var grN string
		var grUid string

		err = rows.Scan(&grUid, &grN)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetGroupsName", err)
		}
		ListGroupsName[grN] = grUid
	}
}
