package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var MapPitchNameUuid map[string]string

//GetListPitch возвращает мапу состояний поля name -> uuid
func GetListPitch() {
	MapPitchNameUuid := make(map[string]string)

	query := `SELECT uid, name FROM matchcenter_pitch`
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetListPitch", err)
		return
	}

	for rows.Next() {
		var pUuid string
		var name string

		err = rows.Scan(&pUuid, &name)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetListPitch", err)
			return
		}
		MapPitchNameUuid[name] = pUuid
	}
}
