package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var ListTrendsText map[string]string

//GetListTrends - Возвращает тексты для трендов
func GetListTrends() {

	ListTrendsText = make(map[string]string)

	query := `SELECT label, word FROM matchcenter_trends_text`
	db.Db.Query(query)
	//Выполняем запрос
	rows, err := db.Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetListTrends", err)
		return
	}

	for rows.Next() {
		var label string
		var word string

		err = rows.Scan(&label, &word)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetListTrends", err)
			return
		}
		ListTrendsText[label] = word
	}
}
