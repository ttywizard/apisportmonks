package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

func GetMatchesSeason(seasonId int, tableName string) ([]int, error) {
	var matches []int
	query := `SELECT id FROM ` + tableName + ` WHERE season_id = $1`
	rows, err := db.Db.Query(query, seasonId)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("GetMatchesSeason", err)
		return nil, err
	}

	for rows.Next() {
		var matchId int

		err = rows.Scan(&matchId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorPanic("GetMatchesSeason", err)
			return nil, err
		}

		matches = append(matches, matchId)
	}
	return matches, nil
}
