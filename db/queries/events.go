package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var ListEventsType map[string]db.MappingType

func GetEventsType() {
	ListEventsType = make(map[string]db.MappingType)

	//Собираем запрос
	query := `SELECT uid, original_name, name FROM matchcenter_events_type WHERE provider_id = $1`

	//Запрос
	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetEventsType", err)
		return
	}

	for rows.Next() {
		var evMap db.MappingType
		var evOrigN string

		err = rows.Scan(&evMap.UUID, &evOrigN, &evMap.Name)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetEventsType", err)
		}
		ListEventsType[evOrigN] = evMap
	}
}
