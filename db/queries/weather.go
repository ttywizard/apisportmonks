package queries

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
)

var ListWeatherUuid map[string]string

func GetWeatherUuid() {
	ListWeatherUuid = make(map[string]string)

	//Собираем запрос
	query := `SELECT uid, name FROM matchcenter_weather_name WHERE provider_id = $1`

	//Запрос
	rows, err := db.Db.Query(query, db.Prov.Id)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetWeatherUuid", err)
		return
	}

	for rows.Next() {
		var wN string
		var wUid string

		err = rows.Scan(&wUid, &wN)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetWeatherUuid", err)
		}
		ListWeatherUuid[wN] = wUid
	}
}
