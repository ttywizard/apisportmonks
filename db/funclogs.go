package db

import (
	logger2 "github.com/Pastir/logger"
	"github.com/jackc/pgx"
	"github.com/lib/pq"
	"go.uber.org/zap"
)

func PqErrorPanic(nameFunc string, err *pq.Error) {
	logger2.ErrorLog.Panic("Panic DB:",
		zap.String("func", nameFunc),
		zap.String("errorName", err.Code.Name()),
		zap.Error(err),
	)
}

func PqErrorError(nameFunc string, err *pq.Error) {
	logger2.ErrorLog.Error("Error DB:",
		zap.String("func", nameFunc),
		zap.String("errorName", err.Code.Name()),
		zap.Error(err),
	)
}

func PGxPanic(nameFunc string, err *pgx.PgError) {
	logger2.ErrorLog.Panic("panic db:",
		zap.String("func", nameFunc),
		zap.String("message", err.Message),
		zap.String("detail", err.Detail),
		zap.Error(err),
	)
}

func PGxError(nameFunc string, err *pgx.PgError) {
	logger2.ErrorLog.Panic("error db:",
		zap.String("func", nameFunc),
		zap.String("message", err.Message),
		zap.String("detail", err.Detail),
		zap.Error(err),
	)
}

func ErrLog(nameFunc string, err error) {
	logger2.ErrorLog.Error("error",
		zap.String("func", nameFunc),
		zap.Error(err),
	)
}
