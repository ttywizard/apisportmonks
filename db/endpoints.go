package db

import (
	"encoding/json"
	"errors"
	"github.com/lib/pq"
	"net/url"
)

type (
	ProviderSettings struct {
		ApiKey    string            `json:"apikey"`
		Url       string            `json:"url_endpoints"`
		GetParams map[string]string `json:"get_params"`
	}

	Provider struct {
		Id       int
		Name     string
		Settings ProviderSettings `json:"settings"`
	}

	Endpoint struct {
		Name            string
		Url             string
		UpdateFrequency int
	}
)

var Prov Provider
var GetParam url.Values
var Endpoints map[string]Endpoint

func (p *ProviderSettings) Scan(value interface{}) error {

	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &p)
}

//SetSettingsProvider
//Заполняем настройки для выбранного провайдера
func SetSettingsProvider() {
	GetParam = url.Values{}
	// Получаем данные по endpoints
	query := `SELECT id
                     ,name
                     ,settings
               FROM matchcenter_provider
	           WHERE status = 't'`

	rows, err := Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		PqErrorPanic("SetSettingsProvider", err)
		return
	}

	for rows.Next() {
		err = rows.Scan(&Prov.Id, &Prov.Name, &Prov.Settings)
		if err, ok := err.(*pq.Error); ok {
			PqErrorPanic("SetSettingsProvider", err)
			return
		}
	}

	// Гет параметры
	for key, val := range Prov.Settings.GetParams {
		GetParam.Add(key, val)
	}
}

//SetProviderEndpoints
//Получаем все эндпоинты провайдера
func SetProviderEndpoints() {

	Endpoints = make(map[string]Endpoint)
	// Получаем данные по endpoints
	query := `SELECT endpoint
                     ,url
                     ,update_frequency
              FROM matchcenter_providersetting AS mcps LEFT JOIN matchcenter_provider AS mcp ON (mcps.provider_id = mcp.id)
              WHERE mcp.status = 't'`

	rows, err := Db.Query(query)
	if err, ok := err.(*pq.Error); ok {
		PqErrorPanic("SetSettingsProvider", err)
		return
	}

	for rows.Next() {
		var s Endpoint
		err = rows.Scan(&s.Name, &s.Url, &s.UpdateFrequency)
		if err, ok := err.(*pq.Error); ok {
			PqErrorPanic("SetSettingsProvider", err)
			return
		}
		Endpoints[s.Name] = s
	}
}
