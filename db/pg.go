package db

import (
	"database/sql"
	"fmt"
	"github.com/jackc/pgx"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/config"
	"os"
	"strconv"
)

//Db active connection to db
var Db *sql.DB
var DBx *sqlx.DB

//InitDb
//инициализация работы с базой
func InitDb() {

	configDB := config.Config.DB
	var err error

	strConn := "user=" + configDB.DbUser + " host=" + configDB.DbHost + " dbname=" + configDB.DbName + " password=" + configDB.DbPass + " port=" + strconv.Itoa(int(configDB.DbPort)) + " connect_timeout=" + strconv.Itoa(int(configDB.ConnectionTimeout)) + " sslmode=" + configDB.SslMode

	Db, err = sql.Open("postgres", strConn)
	if err, ok := err.(*pq.Error); ok {
		PqErrorPanic("InitDb", err)
		return
	}

	//ping database
	err = Db.Ping()
	if err, ok := err.(*pq.Error); ok {
		PqErrorPanic("InitDb", err)
		return
	}

	Db.SetConnMaxLifetime(configDB.MaxLifeTimeConnection)
	Db.SetMaxIdleConns(configDB.MaxIdleConnections)
	Db.SetMaxOpenConns(configDB.MaxOpenConnections)
}

func ConnectDB() {

	var err error
	configDB := config.Config.DB
	strConn := fmt.Sprintf("user=%s host=%s dbname=%s password=%s port=%d connect_timeout=%d sslmode=%s",
		configDB.DbUser,
		configDB.DbHost,
		configDB.DbName,
		configDB.DbPass,
		configDB.DbPort,
		configDB.ConnectionTimeout,
		configDB.SslMode)

	DBx, err = sqlx.Connect("pgx", strConn)
	if err, ok := err.(*pgx.PgError); ok {
		PGxPanic("ConnectDB", err)
		os.Exit(0)
	}

	//ping database
	err = DBx.Ping()
	if err, ok := err.(*pgx.PgError); ok {
		PGxPanic("InitDb", err)
		os.Exit(0)
	}

	DBx.SetConnMaxLifetime(configDB.MaxLifeTimeConnection)
	DBx.SetMaxIdleConns(configDB.MaxIdleConnections)
	DBx.SetMaxOpenConns(configDB.MaxOpenConnections)
}
