package config

import (
	"flag"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"time"
)

var (
	defaultConfigFile = "./config.yaml"
	Config            = Conf{}
	Args              = lineArgs{}
)

type (
	lineArgs struct {
		conf string

		Endpoint string

		//Имя задачи
		Task string

		//Имя провайдера
		Provider string

		//ID Лиги
		LeagueId int

		//ID Сезона
		SeasonId int

		//Тип форм
		TypeForm string

		//Тип турнира
		TypeTournament string

		FromDate string

		ToDate string

		CurrentSeason bool

		TypeHandlers string

		TypeTrends string

		TypeFacts string

		NameBK string
	}

	db struct {
		DbName                string        `yaml:"DbName"`
		DbUser                string        `yaml:"DbUser"`
		DbPass                string        `yaml:"DbPass"`
		DbPort                uint16        `yaml:"DbPort"`
		ConnectionTimeout     uint32        `yaml:"ConnectionTimeout"`
		DbHost                string        `yaml:"DbHost"`
		SslMode               string        `yaml:"SslMode"`
		MaxIdleConnections    int           `yaml:"MaxIdleConnections"`
		MaxOpenConnections    int           `yaml:"MaxOpenConnections"`
		MaxLifeTimeConnection time.Duration `yaml:"MaxLifeTimeConnection"`
	}

	logs struct {
		LogDir        string `yaml:"LogDir"`
		Debug         bool   `yaml:"Debug"`
		Warning       bool   `yaml:"Warning"`
		Info          bool   `yaml:"Info"`
		Error         bool   `yaml:"Error"`
		Critical      bool   `yaml:"Critical"`
		OutConsole    bool   `yaml:"OutConsole"`
		OutFile       bool   `yaml:"OutFile"`
		MaxFileSize   int    `yaml:"MaxFileSize"`
		MaxFilesCount int    `yaml:"MaxFilesCount"`
		MaxAge        int    `yaml:"MaxAge"`
		Compress      bool   `yaml:"Compress"`
	}

	Conf struct {
		Logs *logs
		DB   *db
	}
)

func (c *Conf) ConfigFile() {

	Args.CommandLine()

	//читаем конфигурационный файл
	source, err := ioutil.ReadFile(Args.conf)
	if err != nil {
		log.Fatalf("%s", err)
	}

	// Парсим yaml
	err = yaml.Unmarshal(source, &c)
	if err != nil {
		log.Fatalf("%s", err)
	}
}

func (la *lineArgs) CommandLine() {
	flag.StringVar(&la.conf, "conf", defaultConfigFile, "Path to config file")
	flag.StringVar(&la.Task, "task", "", "Название задачи")
	flag.StringVar(&la.Provider, "provider", "api-football.com", "Имя провайдера предоставляющего услугу")
	flag.StringVar(&la.TypeForm, "typeForm", "all", "Тип формы для итоговой таблицы, варианты(home|away|all)")
	flag.StringVar(&la.TypeTrends, "typeTrend", "all", "Тип тренда, варианты(1x2|yellowcard|corners|handicap|all)")
	flag.StringVar(&la.TypeFacts, "typeFact", "all", "Тип тренда, варианты(1x2|corners|btts|handicap|all)")
	flag.IntVar(&la.LeagueId, "leagueId", 0, "ID Лиги")
	flag.IntVar(&la.SeasonId, "seasonId", 0, "ID Сезона")
	flag.BoolVar(&la.CurrentSeason, "seasonIsCurrent", false, "Выбрать текущий сезон (false|true)")
	flag.StringVar(&la.TypeTournament, "typeTournament", "", "Тип турнира (League|Cup)")
	flag.StringVar(&la.FromDate, "fromDate", "", "Начало периода YYYY-MM-DD")
	flag.StringVar(&la.ToDate, "toDate", "", "Конец периода YYYY-MM-DD")
	flag.StringVar(&la.TypeHandlers, "typeHandlers", "all", "Варианты(разделитель запятая) fixtures,events,lineups,venues,statsPlayersFixture,statsFixtures,sidelined,weather,referees")
	flag.StringVar(&la.NameBK, "nameBK", "", "winline|parimatch|etc...")

	flag.Parse()
}
