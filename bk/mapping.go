package bk

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/gbet/bettings-lines/common"
)

// GetCountryLeagues Возвращает список сопоставленных лиг по странам, БК база c локальной базой
func GetCountryLeagues(bkId int, typesHash string) (map[string]common.Country, error) {
	CountryLeagues := make(map[string]common.Country)

	query := `SELECT mcl.id                                                                        AS league_id
                     ,mcl.uid                                                                      AS league_uid
                     ,mcl.name                                                                     AS en_league_name
                     ,CASE WHEN mct_l.longname ISNULL THEN mct_l.shortname ELSE mct_l.longname END AS ru_league_name
                     ,mc_bkl.bk_league_name
                     ,mcc.countryname                                                              AS en_country_name
                     ,mcalt_c.alternatename                                                        AS ru_country_name
                     ,mc_bkl.bk_country_name
                     ,mc_bkl.bk_league_id
                     ,mcc.uid                                                                      AS country_uid
                     ,mc_bkl.bk_country_id
              FROM matchcenter_bk_league AS mc_bkl LEFT JOIN matchcenter_league AS mcl ON(mc_bkl.league_uid = mcl.uid)
                   LEFT JOIN matchcenter_country AS mcc ON (mcl.country_id = mcc.uid)
                   LEFT JOIN matchcenter_alternativename AS mcalt_c ON (mcalt_c.uid = mcc.uid AND mcalt_c.isolanguage = 'ru')
                   LEFT JOIN matchcenter_translation AS mct_l ON (mct_l.uid = mcl.uid)
              WHERE mc_bkl.bk_id = $1`

	rows, err := db.Db.Query(query, bkId)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetCountryLeagues", err)
		return CountryLeagues, err
	}

	for rows.Next() {
		var c common.Country
		c.League = make(map[string]common.League)
		var l common.League

		err = rows.Scan(&l.Id, &l.Uid, &l.EngName, &l.RuName, &l.BkRuName,
			&c.EngName, &c.RuName, &c.BkRuName, &l.BkId, &c.Uid, &c.BkId)

		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetCountryLeagues", err)
			return CountryLeagues, err
		}

		// Исходя от данных предоставляемых БК
		switch typesHash {
		//с базой бк сопоставления происходит по имени страны, лиги
		case "name":
			if _, ok := CountryLeagues[*c.BkRuName]; ok {
				CountryLeagues[*c.BkRuName].League[*l.BkRuName] = l
			} else {
				c.League[*l.BkRuName] = l
				CountryLeagues[*c.BkRuName] = c
			}

		//с базой бк сопоставления происходит по ID страны, лиги
		case "id":
			if _, ok := CountryLeagues[*c.BkId]; ok {
				CountryLeagues[*c.BkId].League[*l.BkId] = l
			} else {
				c.League[*l.BkId] = l
				CountryLeagues[*c.BkId] = c
			}
		}
	}
	return CountryLeagues, nil
}

//GetExistsMappingTeams - Возвращает смапленые команды страны (БК базы и локальной базы)
func GetExistsMappingTeams(bkCountryName string, leagueId int, bkId int) (map[string]common.MatchBkTeams, error) {
	listTeam := make(map[string]common.MatchBkTeams)

	query := `SELECT mc_t.id  AS team_id
                     ,mc_bk_t.bk_team_name_ru
                     ,mc_bk_t.bk_team_id
              FROM matchcenter_teams_season AS mc_ts LEFT JOIN matchcenter_season AS mc_s ON(mc_ts.season_uid = mc_s.uid AND mc_s.current)
                   LEFT JOIN matchcenter_team AS mc_t ON(mc_t.id = mc_ts.team_id)
                   LEFT JOIN matchcenter_league AS mc_l ON(mc_ts.league_uid = mc_l.uid)
                   LEFT JOIN LATERAL (SELECT DISTINCT ON(uid) uid
                                            ,isolanguage
                                            ,longname AS team_name
                                     FROM matchcenter_translation
                                     WHERE uid = mc_t.uid
                                       AND isolanguage = 'ru'
                  ) AS mc_tr ON(mc_tr.uid = mc_t.uid AND mc_tr.isolanguage = 'ru')
                  LEFT JOIN matchcenter_bk_team AS mc_bk_t ON (mc_t.id = mc_bk_t.team_id)
                  LEFT JOIN matchcenter_country AS mc_c ON (mc_c.uid = mc_l.country_id)
                  LEFT JOIN matchcenter_bk_league AS mc_bk_l ON (mc_bk_l.country_uid = mc_c.uid AND mc_bk_l.league_id = mc_l.id)
                  WHERE mc_bk_l.bk_country_name = $1
                    AND mc_s.current
                    AND mc_tr.isolanguage = 'ru'
                    AND mc_l.id = $2
                    AND mc_bk_l.bk_id = $3
                    AND mc_bk_t.bk_id = $3
                  ORDER BY mc_bk_t.bk_team_name_ru`

	rows, err := db.Db.Query(query, bkCountryName, leagueId, bkId)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetExistsMappingTeams", err)
		return listTeam, err
	}

	for rows.Next() {
		var lt common.MatchBkTeams
		err = rows.Scan(&lt.TeamId, &lt.BkTeamName, &lt.BkTeamId)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetExistsMappingTeams", err)
			return listTeam, err
		}
		listTeam[lt.BkTeamName] = lt
	}

	return listTeam, nil
}
