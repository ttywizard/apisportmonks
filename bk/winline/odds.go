package winline

import (
	"encoding/xml"
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/ttywizard/apisportmonks/utility"
	"gitlab.com/gbet/bettings-lines/common"
	"strconv"
)

//ParserTotal - функция парсит и вставляет коффициенты по маркету "Total"
func ParserTotal(data interface{}, evd common.EventData, marketId int) {
	var HashM uint64
	var total string

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, total, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,               
                                                       probability = EXCLUDED.probability,
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {

		switch attr.Name.Local {
		case "value":
			total = attr.Value
			*h64Data.Total = total

		case "name1":
			if attr.Value == "Больше" {
				*h64Data.Label = "Over"
			} else {
				*h64Data.Label = "Under"
			}
			strValues += values + "'" + total + "', '" + *h64Data.Label + "', " //

		case "odd1":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name2":
			if attr.Value == "Больше" {
				*h64Data.Label = "Over"
			} else {
				*h64Data.Label = "Under"
			}
			strValues += values + "'" + total + "', '" + *h64Data.Label + "', " //Меньше

		case "odd2":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%')"
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("ParserTotal", err)
	}
}

//ParserCorners1X2 - функция парсит и вставляет коффициенты по маркету "1X2"
func ParserCorners1X2(data interface{}, evd common.EventData, marketId int) {
	var HashM uint64

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,               
                                                       probability = EXCLUDED.probability,        
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {
		switch attr.Name.Local {

		case "name1":
			strValues += values + "'" + attr.Value + "', " //1
			*h64Data.Label = attr.Value
		case "odd1":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name2":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " //X
		case "odd2":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name3":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " // 2
		case "odd3":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%')"
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("ParserCorners1X2", err)
	}
}

//Parser1X2 - функция парсит и вставляет коффициенты по маркету "1X2"
func Parser1X2(data interface{}, evd common.EventData, marketId int) {
	var HashM uint64

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,               
                                                       probability = EXCLUDED.probability,        
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {

		switch attr.Name.Local {
		case "name1":
			strValues += values + "'" + attr.Value + "', " //1
			*h64Data.Label = attr.Value

		case "odd1":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)

			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name2":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " //X
		case "odd2":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name3":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " // 2
		case "odd3":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%')"
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("Parser1X2", err)
	}
}

//ParserDoubleChance - функция парсит и вставляет коффициенты по маркету "Double Chance"
func ParserDoubleChance(data interface{}, evd common.EventData, marketId int) {
	var HashM uint64

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,               
                                                       probability = EXCLUDED.probability,        
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {
		switch attr.Name.Local {

		case "name1":
			strValues += values + "'" + attr.Value + "', " //1
			*h64Data.Label = attr.Value
		case "odd1":
			*h64Data.Value = attr.Value
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name2":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " //X
		case "odd2":
			*h64Data.Value = attr.Value
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name3":
			*h64Data.Label = attr.Value
			strValues += values + "'" + attr.Value + "', " // 2
		case "odd3":
			*h64Data.Value = attr.Value
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%')"
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("ParserDoubleChance", err)
	}
}

//ParserHandicap - функция парсит и вставляет коффициенты по маркету "Фора"
func ParserHandicap(data interface{}, evd common.EventData, marketId int) {
	var HomeHash uint64
	var AwayHash uint64

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, handicap, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,              
                                                       handicap = EXCLUDED.handicap,          
                                                       probability = EXCLUDED.probability,        
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {
		tData := h64Data

		switch attr.Name.Local {
		case "value":
			*tData.Market = attr.Value
		case "name1":
			strValues += values + "'" + *tData.Market + "', '" + attr.Value + "', "
			*tData.Label = attr.Value
		case "odd1":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HomeHash = utility.GenerateHash64(tData)
			strValues += attr.Value + ", " + strconv.FormatUint(HomeHash, 10) + ", '" + probability + "%'),"
		case "name2":
			*tData.Label = attr.Value
			strValues += values + "'" + *tData.Market + "', '" + attr.Value + "', "
		case "odd2":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			AwayHash = utility.GenerateHash64(tData)
			strValues += attr.Value + ", " + strconv.FormatUint(AwayHash, 10) + ", '" + probability + "%')"
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("ParserHandicap", err)
	}
}

//ParserBtts - функция парсит и вставляет коффициенты по маркету "1X2"
func ParserBtts(data interface{}, evd common.EventData, marketId int) {
	var HashM uint64

	values := "(" + strconv.Itoa(marketId) + ", " + strconv.Itoa(evd.BkID) + ", " + strconv.Itoa(evd.MatchId) + ", "
	strValues := ""

	query := `INSERT INTO matchcenter_odds(market_id, bookmaker_id, match_id, label, value, xxhash64, probability) VALUES`
	conflict := ` ON CONFLICT (xxhash64) DO UPDATE SET updated_at = NOW(),
                                                       value = EXCLUDED.value,               
                                                       probability = EXCLUDED.probability,        
                                                       winning = EXCLUDED.winning,
                                                       last_update = NOW()`

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, attr := range data.([]xml.Attr) {
		switch attr.Name.Local {

		case "name1":
			if attr.Value == "Да" {
				*h64Data.Label = "Yes"
			} else {
				*h64Data.Label = "No"
			}
			strValues += values + "'" + *h64Data.Label + "', " //Yes
		case "odd1":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%'),"

		case "name2":
			if attr.Value == "Да" {
				*h64Data.Label = "Yes"
			} else {
				*h64Data.Label = "No"
			}
			strValues += values + "'" + *h64Data.Label + "', " //X
		case "odd2":
			v, _ := strconv.ParseFloat(attr.Value, 64)
			probability := strconv.FormatFloat(100/v, 'f', 2, 64)
			HashM = utility.GenerateHash64(h64Data)
			strValues += attr.Value + ", " + strconv.FormatUint(HashM, 10) + ", '" + probability + "%') "
		}
	}

	_, err := db.Db.Exec(query + strValues + conflict)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorPanic("ParserBtts", err)
	}
}
