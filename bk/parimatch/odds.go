package parimatch

import (
	"github.com/valyala/fastjson"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/ttywizard/apisportmonks/utility"
	"gitlab.com/gbet/bettings-lines/common"
	"strconv"
	"time"
)

//ParserTotal - функция парсит и вставляет коффициенты по маркету "Total"
func ParserTotal(data interface{}, evd common.EventData, marketId int) {
	var marketsData []queries.Market
	//Дата последнего обновления
	lastUpdate := time.Now().Format("2006-01-02 15:04:05")

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, v := range data.(*fastjson.Value).GetArray("Outcomes") {
		var marketD queries.Market
		switch string(v.GetStringBytes("OutcomeName", "en")) {
		case "Over":
			marketD.MarketId = marketId
			marketD.MatchId = evd.MatchId
			marketD.Total = string(data.(*fastjson.Value).GetStringBytes("MarketKey", "MarketValue"))
			*h64Data.Total = marketD.Total
			marketD.Label = "Over"
			*h64Data.Label = "Over"
			marketD.Value = v.GetFloat64("Price")
			marketD.BookmakerId = evd.BkID
			marketD.LastUpdate = lastUpdate
			marketD.Xxhash64 = utility.GenerateHash64(h64Data)
			marketD.Probability = strconv.FormatFloat(100/marketD.Value, 'f', 2, 64)
			marketsData = append(marketsData, marketD)

		case "Under":
			marketD.MarketId = marketId
			marketD.MatchId = evd.MatchId
			marketD.Total = string(data.(*fastjson.Value).GetStringBytes("MarketKey", "MarketValue"))
			*h64Data.Total = marketD.Total
			marketD.Label = "Under"
			*h64Data.Label = "Under"
			marketD.Value = v.GetFloat64("Price")
			marketD.BookmakerId = evd.BkID
			marketD.LastUpdate = lastUpdate
			marketD.Probability = strconv.FormatFloat(100/marketD.Value, 'f', 2, 64)
			marketD.Xxhash64 = utility.GenerateHash64(h64Data)
			marketsData = append(marketsData, marketD)
		}
	}

	queries.SaveMarketTotal(marketsData)
}

//ParserCorners1X2 - функция парсит и вставляет коффициенты по маркету "1X2"
func ParserCorners1X2(data interface{}, evd common.EventData, marketId int) {
	//fmt.Println("Corners1X2")
	//spew.Dump(data)
}

//Parser1X2 - функция парсит и вставляет коффициенты по маркету "1X2"
func Parser1X2(data interface{}, evd common.EventData, marketId int) {
	var marketsData []queries.Market
	//Дата последнего обновления
	lastUpdate := time.Now().Format("2006-01-02 15:04:05")

	for _, v := range data.(*fastjson.Value).GetArray("Outcomes") {
		var h64Data utility.HashData
		h64Data.Init()
		h64Data.BkId = evd.BkID
		h64Data.MatchId = evd.MatchId

		var marketD queries.Market
		marketD.MatchId = evd.MatchId

		//Определяем маркет ID для форы
		switch data.(*fastjson.Value).GetInt("MarketKey", "Period") {
		case 1:
			marketD.MarketId = 1 // 3Way Result
		case 2:
			marketD.MarketId = 37 // 3Way Result 1st Half
		case 3:
			marketD.MarketId = 80 // 3Way Result 2st Half
		}
		h64Data.MarkertId = marketId

		// 1 или 2 команды
		switch string(v.GetStringBytes("OutcomeName", "en")) {
		case "Win 1":
			marketD.Label = "1"
		case "Win 2":
			marketD.Label = "2"
		case "Draw":
			marketD.Label = "X"
		}
		*h64Data.Label = marketD.Label

		marketD.Value = v.GetFloat64("Price")
		marketD.BookmakerId = evd.BkID
		marketD.LastUpdate = lastUpdate
		marketD.Xxhash64 = utility.GenerateHash64(h64Data)
		marketD.Probability = strconv.FormatFloat(100/marketD.Value, 'f', 2, 64)
		marketsData = append(marketsData, marketD)
	}

	queries.SaveMarketTotal(marketsData)
}

//ParserDoubleChance - функция парсит и вставляет коффициенты по маркету "Double Chance"
func ParserDoubleChance(data interface{}, evd common.EventData, marketId int) {
	//fmt.Println("DoubleChance")
	//spew.Dump(data)
}

//ParserHandicap - функция парсит и вставляет коффициенты по маркету "Фора"
func ParserHandicap(data interface{}, evd common.EventData, marketId int) {
	var marketsData []queries.Market
	//Дата последнего обновления
	lastUpdate := time.Now().Format("2006-01-02 15:04:05")

	var h64Data utility.HashData
	h64Data.Init()
	h64Data.MarkertId = marketId
	h64Data.BkId = evd.BkID
	h64Data.MatchId = evd.MatchId

	for _, v := range data.(*fastjson.Value).GetArray("Outcomes") {
		var marketD queries.Market

		marketD.MatchId = evd.MatchId
		marketD.Handicap = string(data.(*fastjson.Value).GetStringBytes("MarketKey", "MarketValue"))

		//Определяем маркет ID для форы
		switch data.(*fastjson.Value).GetInt("MarketKey", "Period") {
		case 1:
			marketD.MarketId = 83 // Handicap Result
		case 2:
			marketD.MarketId = 2000000044 // 1st Half Handicap
		case 3:
			marketD.MarketId = 2000000045 // 2st Half Handicap
		}

		// 1 или 2 команды
		switch string(v.GetStringBytes("OutcomeName", "en")) {
		case "Win 1":
			marketD.Label = "1"
		case "Win 2":
			marketD.Label = "2"
		}

		*h64Data.Label = marketD.Label
		marketD.Value = v.GetFloat64("Price")
		marketD.BookmakerId = evd.BkID
		marketD.LastUpdate = lastUpdate
		marketD.Xxhash64 = utility.GenerateHash64(h64Data)
		marketD.Probability = strconv.FormatFloat(100/marketD.Value, 'f', 2, 64)
		marketsData = append(marketsData, marketD)
	}

	queries.SaveMarketTotal(marketsData)
}

//ParserBtts - функция парсит и вставляет коффициенты по маркету "1X2"
func ParserBtts(data interface{}, evd common.EventData, marketId int) {
	//	fmt.Println("BTTS")
	//	spew.Dump(data)
}
