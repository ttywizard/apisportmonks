package bk

import (
	"github.com/Pastir/logger"
	parimatch2 "gitlab.com/ttywizard/apisportmonks/bk/parimatch"
	winline2 "gitlab.com/ttywizard/apisportmonks/bk/winline"
	"gitlab.com/ttywizard/apisportmonks/client"
	"gitlab.com/ttywizard/apisportmonks/db/queries"
	"gitlab.com/gbet/bettings-lines/common"
	parimatch "gitlab.com/gbet/bettings-lines/parimatch"
	winline "gitlab.com/gbet/bettings-lines/winline"
	"go.uber.org/zap"
)

func RoutingBK(bkName string) {

	var mData common.MatchingData
	mData.GetMatchId = GetIdMatch
	mData.SaveFoundTeam = InsertFoundTeamsByMatch
	mData.SearchTeamsByMatch = SearchTeamsByMatch
	mData.GetMappingTeams = GetExistsMappingTeams
	mData.MatchListMarket, _ = queries.GetListMarket(BkID[bkName])

	switch bkName {
	case "winline":
		var s winline.DataBK
		mData.CountryLeagues, _ = GetCountryLeagues(BkID[bkName], "name")
		mData.BkId = BkID[bkName]
		byteValue, err := client.RequestToEndpoint(WinlinePrematch)
		if err != nil {
			logger.ErrorLog.Panic("RoutingBK",
				zap.String("BK", bkName),
				zap.Error(err))
		}

		//xmlFile, _ := os.Open("/home/user/Downloads/winline.xml")
		//byteValue, _ := ioutil.ReadAll(xmlFile)
		var parseMarket common.ParseMarket
		parseMarket.ParserTotal = winline2.ParserTotal
		parseMarket.ParserCorners1X2 = winline2.ParserCorners1X2
		parseMarket.ParserBtts = winline2.ParserBtts
		parseMarket.Parser1X2 = winline2.Parser1X2
		parseMarket.ParserDoubleChance = winline2.ParserDoubleChance
		parseMarket.ParserHandicap = winline2.ParserHandicap
		s.Parser(byteValue, mData, parseMarket)

	case "parimatch":
		var s parimatch.DataBk
		headers := map[string]string{
			"ApiKey": PariMatchApiKey,
		}

		mData.CountryLeagues, _ = GetCountryLeagues(BkID[bkName], "id")
		mData.BkId = BkID[bkName]
		var parseMarket common.ParseMarket
		parseMarket.ParserTotal = parimatch2.ParserTotal
		parseMarket.ParserCorners1X2 = parimatch2.ParserCorners1X2
		parseMarket.ParserBtts = parimatch2.ParserBtts
		parseMarket.Parser1X2 = parimatch2.Parser1X2
		parseMarket.ParserDoubleChance = parimatch2.ParserDoubleChance
		parseMarket.ParserHandicap = parimatch2.ParserHandicap

		//Обрабатываем полученную линию
		//byteValue, err := ioutil.ReadFile("/home/user/BK/Parimatch/parimatch.json")

		byteValue, err := client.CustomRequest(PariMatchFeed, headers)
		if err != nil {
			logger.ErrorLog.Panic("RoutingBK",
				zap.String("BK", bkName),
				zap.Error(err))
		}

		s.Parser(byteValue, mData, parseMarket)
	}
}
