package bk

var BkID = map[string]int{
	"winline":   106,
	"parimatch": 300000001,
}

const (
	WinlinePrematch = "https://bn.wlbann.com:1034/?live=4"
	WinlineLive     = "https://bn.wlbann.com:1034/?live=5"

	/*PariMatchFeed
	sports=8 Football
	stage=Prematch|Live
	*/
	PariMatchFeed   = "https://m.ttfeed.net/api/Feed/current?sports=8&stage=Prematch"
	PariMatchApiKey = "beca026278bd4ff8ab05dcd8c62e272c"
)
