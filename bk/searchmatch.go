package bk

import (
	"github.com/lib/pq"
	"gitlab.com/ttywizard/apisportmonks/db"
	"gitlab.com/gbet/bettings-lines/common"
	"reflect"
	"strconv"
)

func GetIdMatch(homeTeamId int, awayTeamId int, leagueId int) int {
	query := `SELECT id
              FROM matchcenter_match
              WHERE home_team_id = $1
                AND away_team_id = $2
                AND DATE(match_date) >= DATE(NOW())
                AND league_id = $3
              ORDER BY match_date
              LIMIT 1`

	rows, err := db.Db.Query(query, homeTeamId, awayTeamId, leagueId)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("GetIdMatch", err)
		return 0
	}

	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("GetIdMatch", err)
			return 0
		}
		rows.Close()
		return id
	}
	return 0
}

// SearchTeamsByMatch - Поиск(совпадение команд из базы БК с локальной БД) команд по данным определенного матча
//(Страна, Дата, Имя команд в определнном порядке хозяква - гости)
func SearchTeamsByMatch(evd common.EventData, bkId int) error {

	query := `SELECT mcs.id
                    ,mcm.id
                    ,mcs.league_uid
                    ,mcm.match_date
                    ,home_team_id
                    ,away_team_id
                    ,mc_tr_home.longname AS team_home_name
                    ,mc_tr_away.longname AS team_away_name
                    ,similarity(mc_tr_home.longname, $2)  AS s_home_long
                    ,similarity(mc_tr_home.shortname, $2) AS s_home_short
                    ,similarity(mc_tr_away.longname, $3)  AS s_away_long
                    ,similarity(mc_tr_away.shortname, $3) AS s_away_short
                    ,similarity(mct_home.name, $6)        AS s_home_eng
                    ,similarity(mct_away.name, $7)        AS s_away_eng
             FROM matchcenter_bk_league AS bk_l LEFT JOIN matchcenter_season AS mcs ON(bk_l.league_uid = mcs.league_uid)
                  LEFT JOIN matchcenter_match AS mcm ON(mcs.uid = mcm.season_uid)
                  LEFT JOIN matchcenter_team AS mct_home ON(mct_home.id = mcm.home_team_id)
                  LEFT JOIN matchcenter_team AS mct_away ON(mct_away.id = mcm.away_team_id)
                  LEFT JOIN matchcenter_translation AS mc_tr_home ON(mc_tr_home.uid = mct_home.uid)
                  LEFT JOIN matchcenter_translation AS mc_tr_away ON(mc_tr_away.uid = mct_away.uid)
             WHERE bk_country_name = $4
               AND bk_l.bk_id = $5
               AND mcs.current
               AND DATE(match_date) = DATE($1)
               AND ((similarity(mc_tr_home.longname, $2) > 0.4 OR similarity(mc_tr_home.shortname, $2) > 0.4)
               OR (similarity(mc_tr_away.longname, $3) > 0.4 OR similarity(mc_tr_away.shortname, $3) > 0.4)
               OR (similarity(mct_home.name, $6) > 0.4 OR similarity(mct_away.name, $7) > 0.4))`

	rows, err := db.Db.Query(query, evd.BkMatchDate, evd.BkHomeTeamNameRu, evd.BkAwayTeamNameRu, evd.BkCountryNameRu, bkId, evd.BkHomeTeamNameRu, evd.BkAwayTeamNameEng)
	if err, ok := err.(*pq.Error); ok {
		db.PqErrorError("SearchTeamsByMatch", err)
		return err
	}

	for rows.Next() {
		err = rows.Scan(&evd.SeasonId, &evd.MatchId, &evd.LeagueUid, &evd.MatchDate,
			&evd.HomeTeamId, &evd.AwayTeamId, &evd.BkHomeTeamNameRu, &evd.BkAwayTeamNameRu,
			&evd.SimilarityHomeLong, &evd.SimilarityHomeShort, &evd.SimilarityAwayLong,
			&evd.SimilarityAwayShort, &evd.SimilarityHomeEng, &evd.SimilarityAwayEng)

		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("SearchTeamsByMatch.SearchTeamsByMatch", err)
			return err
		}
	}
	return nil
}

//InsertFoundTeamsByMatch -  Предварательный поиск,
//сохранение данных найденых(сопоставленных с локальной базой) команд по определенному матчу
func InsertFoundTeamsByMatch(evd common.EventData, bkId int) {
	SearchTeamsByMatch(evd, bkId)

	if evd.MatchId != 0 {
		insertQuery := `INSERT INTO matchcenter_bk_team (team_id 
                                                         ,bk_team_name_ru 
                                                         ,bk_team_name_en
                                                         ,bk_team_id
                                                         ,bk_id 
                                                         ,similarity_longname
                                                         ,similarity_shortname
                                                         ,similarity_engname) VALUES`
		//Home team
		values := "(" + strconv.Itoa(evd.HomeTeamId) + ", "
		values += "'" + evd.BkHomeTeamNameRu + "', "
		values += "'" + evd.BkHomeTeamNameEng + "', "
		values += strconv.Itoa(evd.BkHomeTeamId) + ", "
		values += strconv.Itoa(bkId) + ", "

		t := reflect.ValueOf(evd.SimilarityHomeLong)
		if t.IsNil() {
			values += "0.00, "
		} else {
			values += strconv.FormatFloat(*evd.SimilarityHomeLong, 'f', 2, 64) + ", "
		}

		t = reflect.ValueOf(evd.SimilarityHomeShort)
		if t.IsNil() {
			values += "0.00, "
		} else {
			values += strconv.FormatFloat(*evd.SimilarityHomeShort, 'f', 2, 64) + ", "
		}

		t = reflect.ValueOf(evd.SimilarityHomeEng)
		if t.IsNil() {
			values += "0.00), "
		} else {
			values += strconv.FormatFloat(*evd.SimilarityHomeEng, 'f', 2, 64) + "),"
		}

		//Away team
		values += "(" + strconv.Itoa(evd.AwayTeamId) + ", "
		values += "'" + evd.BkAwayTeamNameRu + "', "
		values += "'" + evd.BkAwayTeamNameEng + "', "
		values += strconv.Itoa(evd.BkAwayTeamId) + ", "
		values += strconv.Itoa(bkId) + ", "

		t = reflect.ValueOf(evd.SimilarityAwayLong)
		if t.IsNil() {
			values += "0.00, "
		} else {
			values += strconv.FormatFloat(*evd.SimilarityAwayLong, 'f', 2, 64) + ", "
		}

		t = reflect.ValueOf(evd.SimilarityAwayShort)
		if t.IsNil() {
			values += "0.00,"
		} else {
			values += strconv.FormatFloat(*evd.SimilarityAwayShort, 'f', 2, 64) + ", "
		}

		t = reflect.ValueOf(evd.SimilarityAwayEng)
		if t.IsNil() {
			values += "0.00)"
		} else {
			values += strconv.FormatFloat(*evd.SimilarityAwayEng, 'f', 2, 64) + ")"
		}

		query := insertQuery + values + ` ON CONFLICT (team_id, bk_id) DO UPDATE SET updated_at = NOW()
                                                                                     ,bk_team_name_ru = EXCLUDED.bk_team_name_ru
                                                                                     ,bk_team_name_en = EXCLUDED.bk_team_name_en`

		_, err := db.Db.Exec(query)
		if err, ok := err.(*pq.Error); ok {
			db.PqErrorError("InsertFoundTeamsByMatch", err)
		}
	}
}
